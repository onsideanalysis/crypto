package main

import (
	"bitbucket.org/onsideanalysis/crypto/handlers"
	"bitbucket.org/onsideanalysis/crypto/providers"
	"bitbucket.org/onsideanalysis/data_api/config"
	"bitbucket.org/onsideanalysis/data_api/schemas/environment"
	"bitbucket.org/onsideanalysis/data_api/tablestream/protocol"
	"bitbucket.org/onsideanalysis/data_api/tablestream/subscriber"
	"flag"
	"github.com/phyber/negroni-gzip/gzip"
	"github.com/rs/cors"
	"github.com/urfave/negroni"
	"log"
	"net/http"
)

var (
	ALLOWED_ORIGINS = []string{"*"}
	port            string
	env             environment.Environment
	configPath      string
)

const DEFAULT_PORT = "5000"

func init() {

	flag.StringVar(&port, "port", DEFAULT_PORT, "The port on which the app will run. If left blank defaults to 5000")
	flag.Var(&env, "env", "The running environment, one of prod/staging/dev/test")
	flag.StringVar(&configPath, "config_path", "/opt/app/", "The path to load the configuration")
	flag.Parse()

	log.SetFlags(log.Ldate | log.Lmicroseconds | log.Lshortfile)
}

func main() {

	log.Println("Starting up in", env.String())

	envStr := env.String()
	// Use dev config for staging
	if env == environment.STAGING {
		envStr = environment.DEVELOPMENT.String()
	}
	settings := config.GetConfig(envStr, configPath)

	transport := initTableStream(envStr, settings)
	defer transport.Close()

	sub := subscriber.NewTableStreamSubscriber(transport, protocol.HEARTBEAT_INTERVAL, protocol.HEARBEAT_TIMEOUT_MULTIPLIER)
	ws := handlers.NewWebsocketHandler(providers.NewTableStreamProvider(sub), ALLOWED_ORIGINS)

	op := providers.NewOrdersProvider(settings.Rabbit.HostList, "execution_crypto")
	oh := handlers.NewOrdersHandler(op)

	// Create router
	mux := handlers.Router(oh, ws)
	n := negroni.Classic()

	options := cors.New(cors.Options{
		AllowedOrigins:   ALLOWED_ORIGINS,
		AllowedMethods:   []string{http.MethodGet, http.MethodPost, http.MethodPut, http.MethodDelete, http.MethodOptions},
		AllowedHeaders:   []string{"Accept", "Accept-Encoding", "Authorization", "Content-Type", "Content-Length", "X-CSRF-Token", "Origin", "Upgrade", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	})

	n.Use(options)
	n.Use(gzip.Gzip(gzip.DefaultCompression))
	n.UseHandler(mux)
	n.Run(":" + port)
}

func initTableStream(envStr string, settings config.Config) (transport protocol.Transport) {

	switch envStr {
	case environment.PRODUCTION.String(), environment.DEVELOPMENT.String():

		transport = protocol.NewRabbitMQTransport(settings.Rabbit.HostList)
		if err := transport.Connect(); err != nil {
			log.Fatalf("TableStream transport failed to Connect(): %s", err.Error())
		}

	default:
		log.Fatal("Unknown environment provided.")
	}
	return transport
}
