FROM golang:1.10-alpine as golang

RUN mkdir -p /go/src/bitbucket.org/onsideanalysis/crypto
WORKDIR /go/src/bitbucket.org/onsideanalysis/crypto
RUN apk add --no-cache gcc libc-dev git

COPY ./ ./

ARG BUILD_NUMBER

RUN go test  ./...
RUN go build -x -ldflags "-X  bitbucket.org/onsideanalysis/crypto/handlers.BuildNumber=${BUILD_NUMBER} -X  bitbucket.org/onsideanalysis/crypto/handlers.Branch=master  -X bitbucket.org/onsideanalysis/crypto/handlers.Commit=$(git log -1 --format="%H") -X bitbucket.org/onsideanalysis/crypto/handlers.TimeStamp=$(date -u +%Y-%m-%d_%H:%M:%S)"



FROM alpine

COPY --from=golang /go/src/bitbucket.org/onsideanalysis/crypto/crypto /usr/local/bin/
