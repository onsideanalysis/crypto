package queue

type Producer interface {
	Publish(Message) error
	PublishMany([]Message) error
	Connect() (error, chan error)
	Stop() error
}

type Consumer interface {
	Consume(ConsumerInput) (<-chan Message, error)
	Connect() (error, chan error)
	Stop() error
	UnSubscribe(ConsumerInput) error
}

type Message struct {
	Body        []byte
	Topic       string
	Key         string
	ReplyTo     string
	ContentType string
	Headers     map[string]interface{}
	Args        map[string]interface{}
}

type ConsumerInput struct {
	Key   string
	Queue string
	Tag   string
}
