package rabbitmq

import (
	"errors"
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	"bitbucket.org/onsideanalysis/trading-system-framework/queue"
	"github.com/streadway/amqp"
	"math/rand"
	"net"
)

var (
	E_NO_URLS             = errors.New("No URLs given")
	E_FAILED_TO_CONNECT   = errors.New("Failed to connect to any of the URLs provided")
	E_FAILED_TO_RECONNECT = errors.New("Failed to reconnect")
)

var defaultConnectionTimeout = 5 * time.Second

type RMQConfig struct {
	AmqpConfig         amqp.Config
	ExchangeDurable    bool
	QueueDurable       bool
	ExchangeAutoDelete bool
	QueueAutoDelete    bool
	Exclusive          bool
	NoWait             bool
	AutoAck            bool
	NoLocal            bool
	Internal           bool
	Mandatory          bool
	Immediate          bool
	Args               amqp.Table
}

type RMQProducer struct {
	urls             []string
	activeConnection int

	exchange     string
	exchangeType string

	config *RMQConfig

	mu         sync.RWMutex
	connection *amqp.Connection
	channel    *amqp.Channel

	errorChannel chan error
}

func DefaultRMQConfig() *RMQConfig {
	return &RMQConfig{
		// these are the defaults in amqp library when calling Dial(url)
		AmqpConfig: amqp.Config{
			Heartbeat: 10 * time.Second,
			Locale:    "en_US",
			Dial: func(network, addr string) (net.Conn, error) {
				conn, err := net.DialTimeout(network, addr, defaultConnectionTimeout)
				if err != nil {
					return nil, err
				}

				// Heartbeating hasn't started yet, don't stall forever on a dead server.
				// A deadline is set for TLS and AMQP handshaking. After AMQP is established,
				// the deadline is cleared in openComplete.
				if err := conn.SetDeadline(time.Now().Add(defaultConnectionTimeout)); err != nil {
					return nil, err
				}

				return conn, nil
			},
		},
		ExchangeDurable:    true,
		QueueDurable:       false,
		ExchangeAutoDelete: false,
		QueueAutoDelete:    true,
		Exclusive:          false,
		NoWait:             false,
		AutoAck:            true,
		NoLocal:            false,
		Internal:           false,
		Mandatory:          false,
		Immediate:          false,
		Args:               amqp.Table{},
	}
}

// pass a nil config to get defaults
func NewRMQProducer(urls []string, exchange string, exchangeType string, config *RMQConfig) (*RMQProducer, error) {
	if len(urls) == 0 {
		return nil, E_NO_URLS
	}

	if config == nil {
		config = DefaultRMQConfig()
	}

	shuffledUrls := append([]string(nil), urls...)

	rand.Seed(time.Now().Unix())
	rand.Shuffle(len(shuffledUrls), func(i, j int) {
		shuffledUrls[i], shuffledUrls[j] = shuffledUrls[j], shuffledUrls[i]
	})

	return &RMQProducer{
		urls:             shuffledUrls,
		activeConnection: -1,

		exchange:     exchange,
		exchangeType: exchangeType,

		config: config,

		errorChannel: make(chan error, 1),
	}, nil
}

func (r *RMQProducer) Publish(message queue.Message) (err error) {
	correlationId, _ := message.Args["CorrelationId"].(string)

	r.mu.RLock()
	err = r.channel.Publish(r.exchange, message.Key, r.config.Mandatory, r.config.Immediate, amqp.Publishing{
		ContentType:   message.ContentType,
		Body:          message.Body,
		ReplyTo:       message.ReplyTo,
		Headers:       message.Headers,
		CorrelationId: correlationId,
	})
	r.mu.RUnlock()

	return
}

func (r *RMQProducer) PublishMany(messages []queue.Message) (err error) {
	r.mu.RLock()
	defer r.mu.RUnlock()

	for _, message := range messages {
		correlationId, _ := message.Args["CorrelationId"].(string)

		err = r.channel.Publish(r.exchange, message.Key, r.config.Mandatory, r.config.Immediate, amqp.Publishing{
			ContentType:   message.ContentType,
			Body:          message.Body,
			ReplyTo:       message.ReplyTo,
			Headers:       message.Headers,
			CorrelationId: correlationId,
		})
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *RMQProducer) Connect() (error, chan error) {
	var err error

	r.mu.Lock()
	defer r.mu.Unlock()

	connected := false
	connectedUrl := ""

	for i := 0; i < len(r.urls); i++ {
		var url = r.urls[i]
		var urlIdx = i
		if r.activeConnection != -1 {
			urlIdx = (r.activeConnection + 1 + i) % len(r.urls)
			url = r.urls[urlIdx]
		}

		r.connection, err = amqp.DialConfig(url, r.config.AmqpConfig)
		if err != nil {
			log.Printf("RMQProducer - Failed to connect to %s: %s", url, err.Error())
			continue
		}

		r.channel, err = r.connection.Channel()
		if err != nil {
			log.Printf("RMQProducer - Failed to get channel to %s: %s", url, err.Error())
			continue
		}

		log.Printf("RMQProducer - Connected to %s", url)

		connected = true
		connectedUrl = url
		r.activeConnection = urlIdx
		break
	}

	if !connected {
		return E_FAILED_TO_CONNECT, nil
	}

	err = r.channel.ExchangeDeclare(r.exchange, r.exchangeType, r.config.ExchangeDurable, r.config.ExchangeAutoDelete,
		r.config.Internal, r.config.NoWait, r.config.Args)
	if err == nil {
		errorChannel := make(chan *amqp.Error)

		go func(errorChannel chan *amqp.Error, url string) {
			amqpError := <-errorChannel
			// not a proper shutdown
			if amqpError != nil {
				log.Printf("RMQProducer at %s is closing on error: %d - %s. Will attempt to reconnect.", url, amqpError.Code, amqpError.Reason)
				if err, _ := r.Connect(); err != nil {
					log.Printf("RMQProducer - Failed to reconnect: %s", err.Error())
					r.errorChannel <- E_FAILED_TO_RECONNECT
					if err = r.Stop(); err != nil {
						log.Printf("RMQProducer - Failed to stop: %s", err.Error())
					}
				}
			} else {
				log.Printf("RMQProducer at %s is shutting down.", url)
			}
		}(errorChannel, connectedUrl)

		r.connection.NotifyClose(errorChannel)
	} else {
		log.Printf("RMQProducer - Failed to declare exchange: %s", err.Error())
	}

	return err, r.errorChannel
}

func (r *RMQProducer) Stop() (err error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	if r.channel != nil {
		if err := r.channel.Close(); err != nil {
			return fmt.Errorf("Producer channel close error: %s", err)
		}
	}
	if r.connection != nil {
		if err := r.connection.Close(); err != nil {
			return fmt.Errorf("Producer connection close error: %s", err)
		}
	}
	return
}

type RMQConsumer struct {
	urls             []string
	activeConnection int

	exchange     string
	exchangeType string

	config *RMQConfig

	connection    *amqp.Connection
	channels      map[string]*amqp.Channel
	strChannels   map[string]chan queue.Message
	consumeInputs map[string]queue.ConsumerInput
	quitChannels  map[string]chan struct{}
	waitGroups    map[string]*sync.WaitGroup
	mu            sync.Mutex

	errorChannel chan error
}

func NewRMQConsumer(urls []string, exchange string, exchangeType string, config *RMQConfig) (*RMQConsumer, error) {
	if len(urls) == 0 {
		return nil, E_NO_URLS
	}

	if config == nil {
		config = DefaultRMQConfig()
	}

	shuffledUrls := append([]string(nil), urls...)

	rand.Seed(time.Now().Unix())
	rand.Shuffle(len(shuffledUrls), func(i, j int) {
		shuffledUrls[i], shuffledUrls[j] = shuffledUrls[j], shuffledUrls[i]
	})

	return &RMQConsumer{
		urls:             shuffledUrls,
		activeConnection: -1,

		exchange:     exchange,
		exchangeType: exchangeType,

		config: config,

		channels:      make(map[string]*amqp.Channel),
		strChannels:   make(map[string]chan queue.Message),
		consumeInputs: make(map[string]queue.ConsumerInput),
		quitChannels:  make(map[string]chan struct{}),
		waitGroups:    make(map[string]*sync.WaitGroup),

		errorChannel: make(chan error, 1),
	}, nil
}

func (r *RMQConsumer) Connect() (error, chan error) {
	var err error

	r.mu.Lock()
	defer r.mu.Unlock()

	r.channels = make(map[string]*amqp.Channel)

	var ch *amqp.Channel
	connected := false
	connectedUrl := ""

	for i := 0; i < len(r.urls); i++ {
		var url = r.urls[i]
		var urlIdx = i
		if r.activeConnection != -1 {
			urlIdx = (r.activeConnection + 1 + i) % len(r.urls)
			url = r.urls[urlIdx]
		}

		r.connection, err = amqp.DialConfig(url, r.config.AmqpConfig)
		if err != nil {
			log.Printf("RMQConsumer - Failed to connect to %s: %s", url, err.Error())
			continue
		}

		ch, err = r.connection.Channel()
		if err != nil {
			log.Printf("RMQConsumer - Failed to get channel to %s: %s", url, err.Error())
			continue
		}

		log.Printf("RMQConsumer - Connected to %s", url)

		r.channels["exchange"] = ch

		connected = true
		connectedUrl = url
		r.activeConnection = urlIdx
		break
	}

	if !connected {
		return E_FAILED_TO_CONNECT, nil
	}

	err = ch.ExchangeDeclare(r.exchange, r.exchangeType, r.config.ExchangeDurable, r.config.ExchangeAutoDelete,
		r.config.Internal, r.config.NoWait, r.config.Args)
	if err == nil {
		for _, consumeInput := range r.consumeInputs {
			_, err := r.consumeUnlocked(consumeInput)
			if err != nil {
				log.Printf("Error consuming: %s", err.Error())
			}
		}

		errorChannel := make(chan *amqp.Error)

		go func(errorChannel chan *amqp.Error, url string) {
			amqpError := <-errorChannel
			// not a proper shutdown
			if amqpError != nil {
				log.Printf("RMQConsumer at %s is closing on error: %d - %s. Will attempt to reconnect.", url, amqpError.Code, amqpError.Reason)
				if err, _ := r.Connect(); err != nil {
					log.Printf("RMQConsumer - Failed to reconnect: %s", err.Error())
					r.errorChannel <- E_FAILED_TO_RECONNECT
					if err = r.Stop(); err != nil {
						log.Printf("RMQConsumer - Failed to stop: %s", err.Error())
					}
				}
			} else {
				log.Printf("RMQConsumer at %s is shutting down.", url)
			}
		}(errorChannel, connectedUrl)

		r.connection.NotifyClose(errorChannel)
	} else {
		log.Printf("RMQConsumer - Failed to declare exchange: %s", err.Error())
	}

	return err, r.errorChannel
}

func (r *RMQConsumer) consumeUnlocked(input queue.ConsumerInput) (<-chan queue.Message, error) {
	var channel chan queue.Message
	if value, has := r.strChannels[input.Queue]; has {
		log.Printf("Channel already existed %+v", value)
		channel = value
	} else {
		channel = make(chan queue.Message)
		r.strChannels[input.Queue] = channel
	}
	ch, err := r.connection.Channel()
	if err != nil {
		log.Printf("Error on channel connection %s", err.Error())
		return nil, err
	}
	_, err = ch.QueueDeclare(input.Queue, r.config.QueueDurable, r.config.QueueAutoDelete, r.config.Exclusive, r.config.NoWait, r.config.Args)
	if err != nil {
		log.Printf("Error on queue declaration %s", err.Error())
		return nil, err
	}
	r.consumeInputs[input.Queue] = input
	messages, err := ch.Consume(input.Queue, input.Tag, r.config.AutoAck, r.config.Exclusive, r.config.NoLocal, r.config.NoWait, r.config.Args)
	if err != nil {
		log.Printf("Error on channel consume %s", err.Error())
		return nil, err
	}

	err = ch.QueueBind(input.Queue, input.Key, r.exchange, r.config.NoWait, r.config.Args)
	if err != nil {
		log.Printf("Error on queue  bind %s", err.Error())
		return nil, err
	}

	r.channels[input.Queue] = ch

	quitChannel := make(chan struct{})
	r.quitChannels[input.Queue] = quitChannel

	wg := &sync.WaitGroup{}
	wg.Add(1)
	r.waitGroups[input.Queue] = wg

	go func() {
		defer wg.Done()
		for {
			select {
			case message, ok := <-messages:
				if !ok {
					log.Printf("RMQConsumer - Channel for queue %s closed", input.Queue)
					return
				}
				queueMessage := queue.Message{
					ContentType: message.ContentType,
					Body:        message.Body,
					Key:         message.RoutingKey,
					ReplyTo:     message.ReplyTo,
					Args: map[string]interface{}{
						"CorrelationId": message.CorrelationId,
					},
				}
				select {
				case channel <- queueMessage:
				case <-quitChannel:
					return
				}
			case <-quitChannel:
				return
			}
		}
	}()
	return channel, err
}

func (r *RMQConsumer) Consume(input queue.ConsumerInput) (<-chan queue.Message, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	return r.consumeUnlocked(input)
}

func (r *RMQConsumer) UnSubscribe(message queue.ConsumerInput) (err error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	quitChannel := r.quitChannels[message.Queue]
	close(quitChannel)
	delete(r.quitChannels, message.Queue)

	wg := r.waitGroups[message.Queue]
	wg.Wait()
	delete(r.waitGroups, message.Queue)

	if msgChan, hasChannel := r.strChannels[message.Queue]; hasChannel {
		close(msgChan)
		delete(r.strChannels, message.Queue)
	}
	if channel, hasChannel := r.channels[message.Queue]; hasChannel {
		if err = channel.Close(); err != nil {
			return fmt.Errorf("Unsubscribe failed: %s", err)
		}
		delete(r.channels, message.Queue)
	}
	if _, hasInput := r.consumeInputs[message.Queue]; hasInput {
		delete(r.consumeInputs, message.Queue)
	}
	return
}

func (r *RMQConsumer) Stop() (err error) {
	var errStrings []string

	r.mu.Lock()
	defer r.mu.Unlock()

	for _, quitChan := range r.quitChannels {
		close(quitChan)
	}
	for _, wg := range r.waitGroups {
		wg.Wait()
	}
	for _, msgChan := range r.strChannels {
		close(msgChan)
	}
	for _, channel := range r.channels {
		err = channel.Close()
		if err != nil {
			errStrings = append(errStrings, err.Error())
		}
	}
	if r.connection != nil {
		err = r.connection.Close()
		if err != nil {
			errStrings = append(errStrings, err.Error())
		}
	}

	if len(errStrings) > 0 {
		return fmt.Errorf(strings.Join(errStrings, "\n"))
	}
	return nil
}
