package trading

import (
	"time"

	"bitbucket.org/onsideanalysis/data_api/schemas/bookmakers"
	"bitbucket.org/onsideanalysis/data_api/schemas/stickers"
	"gopkg.in/mgo.v2/bson"
)

type InstructionStatus int
type InstructionClosedReason string

const (
	INSTRUCTIONS_EXECUTION        string = "instructions_execution"
	INSTRUCTIONS_CANCELLATION     string = "instructions_cancellation"
	INSTRUCTIONS_REPLACE          string = "instructions_replace"
	START_SOR_CODE                string = "start_sor_code"
	INSTRUCTIONS_RESET            string = "instructions_reset"
	INSTRUCTIONS_STRATEGY_UPDATES string = "instructions_strategy_update"

	SOR_NET_ODDS    SorCode = "sorNetOdds"
	SOR_AGGRESSIVE  SorCode = "sorAggressive"
	SOR_LIMIT_SNIPE SorCode = "sorLimitSnipe"
	SOR_NEUTRAL     SorCode = "sorNeutral"
	SOR_SYNTHETIC   SorCode = "sorSynthetic"

	INSTRUCTION_FULLY_MATCHED InstructionClosedReason = "matched"
	INSTRUCTION_CANCELLED     InstructionClosedReason = "cancelled"
	INSTRUCTION_TIMEOUT       InstructionClosedReason = "timeout"
	INSTRUCTION_VIOLATION     InstructionClosedReason = "violation"

	INSTRUCTION_CLOSED_REASON = "closed_reason"
	INSTRUCTION_IDLE_REASON   = "idle_reason"
	INSTRUCTION_FAILED_REASON = "failed_reason"

	LAPSE         Persistence = "lapse"
	PERSIST       Persistence = "persist"
	NA            Persistence = "na"
	FILL_AND_KILL Persistence = "fillAndKill"

	PROCESSING InstructionStatus = 1
	WATCHING   InstructionStatus = 2
	IDLE       InstructionStatus = 3
	CLOSED     InstructionStatus = 4
	FAILED     InstructionStatus = 5
)

var (
	ALL_INSTRUCTION_STATUSES = map[InstructionStatus]struct{}{
		PROCESSING: {},
		WATCHING:   {},
		IDLE:       {},
		CLOSED:     {},
		FAILED:     {},
	}
	ACTIVE_INSTRUCTION_STATUSES = map[InstructionStatus]struct{}{
		PROCESSING: {},
		WATCHING:   {},
		IDLE:       {},
	}
	INACTIVE_INSTRUCTION_STATUSES = map[InstructionStatus]struct{}{
		CLOSED: {},
		FAILED: {},
	}
)

type Persistence string

type SorCode string

func (s SorCode) String() string {
	switch s {
	case SOR_NET_ODDS:
		return "sorNetOdds"
	case SOR_AGGRESSIVE:
		return "sorAggressive"
	case SOR_LIMIT_SNIPE:
		return "sorLimitSnipe"
	}
	return ""
}

func (i InstructionStatus) IsActive() bool {
	if _, ok := ACTIVE_INSTRUCTION_STATUSES[i]; ok {
		return true
	}
	return false
}

func (i InstructionStatus) String() string {
	switch i {
	case PROCESSING:
		return "PROCESSING"
	case WATCHING:
		return "WATCHING"
	case IDLE:
		return "IDLE"
	case CLOSED:
		return "CLOSED"

	case FAILED:
		return "FAILED"
	}
	return ""
}

type Instruction struct {
	RestrictTo          map[bookmakers.Bookmaker][]bson.ObjectId `bson:"restrict_to,omitempty" json:"restrict_to,omitempty"`
	BetSide             BetSide                                  `bson:"bet_side" json:"bet_side"`
	InstructionID       bson.ObjectId                            `bson:"id,omitempty" json:"id,omitempty"`
	Sticker             stickers.Sticker                         `bson:"sticker" json:"sticker"`
	PlacedTime          time.Time                                `bson:"placed_time,omitempty" json:"placed_time,omitempty"`
	Strategy            string                                   `bson:"strategy,omitempty" json:"strategy,omitempty"`
	StrategyDescr       string                                   `bson:"strategy_descr,omitempty" json:"strategy_descr,omitempty"`
	Details             map[string]interface{}                   `bson:"details,omitempty" json:"details,omitempty"`
	AlgoDetails         map[string]interface{}                   `bson:"algo_details,omitempty" json:"algo_details,omitempty"`
	Price               float64                                  `bson:"price" json:"price"`
	Size                float64                                  `bson:"size" json:"size"`
	TargetSize          float64                                  `bson:"target_size" json:"target_size"`
	Source              Source                                   `bson:"source,omitempty" json:"source,omitempty"`
	Currency            Currency                                 `bson:"currency" json:"currency"`
	TradeID             bson.ObjectId                            `bson:"trade_id,omitempty" json:"trade_id,omitempty"`
	TradingUserID       bson.ObjectId                            `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	Status              InstructionStatus                        `bson:"status" json:"status"`
	SorCodes            map[SorCode]interface{}                  `bson:"sor_codes,omitempty" json:"sor_codes,omitempty"`
	TTL                 time.Time                                `bson:"ttl,omitempty" json:"ttl,omitempty"`
	Persistence         Persistence                              `bson:"persistence" json:"persistence"`
	Ut                  time.Time                                `bson:"ut,omitempty" json:"ut,omitempty"`
	SizeMatched         float64                                  `bson:"size_matched" json:"size_matched"`
	SizeActive          float64                                  `bson:"size_active" json:"size_active"`
	AveragePriceMatched float64                                  `bson:"average_price_matched" json:"average_price_matched"`
}

func (i Instruction) ToHistory() InstructionHistory {
	return InstructionHistory{
		InstructionID:       i.InstructionID,
		Ut:                  i.Ut,
		Status:              i.Status,
		TTL:                 i.TTL,
		SizeMatched:         i.SizeMatched,
		Persistence:         i.Persistence,
		SorCodes:            i.SorCodes,
		RestrictTo:          i.RestrictTo,
		SizeActive:          i.SizeActive,
		Price:               i.Price,
		Size:                i.Size,
		AveragePriceMatched: i.AveragePriceMatched,
		TargetSize:          i.TargetSize,
	}
}

func (PlaceInstruction) omsCommandLevel() omsCommandLevel          { return instruction }
func (CancelInstruction) omsCommandLevel() omsCommandLevel         { return instruction }
func (ReplaceInstruction) omsCommandLevel() omsCommandLevel        { return instruction }
func (StartSorCode) omsCommandLevel() omsCommandLevel              { return instruction }
func (ResetInstruction) omsCommandLevel() omsCommandLevel          { return instruction }
func (UpdateStrategyInstruction) omsCommandLevel() omsCommandLevel { return instruction }

type PlaceInstruction struct {
	RestrictTo    map[bookmakers.Bookmaker][]bson.ObjectId `json:"restrict_to"`
	BetSide       BetSide                                  `json:"bet_side"`
	InstructionID bson.ObjectId                            `json:"id"`
	Sticker       stickers.Sticker                         `json:"sticker"`
	PlacedTime    time.Time                                `json:"placed_time"`
	Strategy      string                                   `json:"strategy"`
	StrategyDescr string                                   `json:"strategy_descr"`
	Details       map[string]interface{}                   `json:"details,omitempty"`
	AlgoDetails   map[string]interface{}                   `json:"algo_details,omitempty"`
	Price         float64                                  `json:"price"`
	Size          float64                                  `json:"size"`
	Source        Source                                   `json:"source"`
	Currency      Currency                                 `json:"currency"`
	TradeID       bson.ObjectId                            `json:"trade_id"`
	SorCodes      map[SorCode]interface{}                  `json:"sor_codes,omitempty"`
	TTL           time.Time                                `json:"ttl"`
	Persistence   Persistence                              `json:"persistence"`
	TargetSize    float64                                  `json:"target_size"`
}

type PlaceInstructionMsg struct {
	TradingUserID bson.ObjectId      `json:"trading_user_id"`
	Instructions  []PlaceInstruction `json:"instructions"`
}

type InstructionHistory struct {
	InstructionID       bson.ObjectId                            `bson:"id" json:"id"`
	Ut                  time.Time                                `bson:"ut" json:"ut"`
	Status              InstructionStatus                        `bson:"status" json:"status"`
	Persistence         Persistence                              `bson:"persistence" json:"persistence"`
	TargetSize          float64                                  `bson:"target_size" json:"target_size"`
	Price               float64                                  `bson:"price" json:"price"`
	Size                float64                                  `bson:"size" json:"size"`
	SorCodes            map[SorCode]interface{}                  `bson:"sor_codes,omitempty" json:"sor_codes,omitempty"`
	RestrictTo          map[bookmakers.Bookmaker][]bson.ObjectId `bson:"restrict_to,omitempty" json:"restrict_to,omitempty"`
	TTL                 time.Time                                `bson:"ttl" json:"ttl"`
	SizeMatched         float64                                  `bson:"size_matched" json:"size_matched"`
	SizeActive          float64                                  `bson:"size_active" json:"size_active"`
	AveragePriceMatched float64                                  `bson:"average_price_matched" json:"average_price_matched"`
}

type CancelInstructionMsg struct {
	TradingUserID bson.ObjectId       `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	Instructions  []CancelInstruction `json:"instructions"`
}

type CancelInstruction struct {
	ID            bson.ObjectId `bson:"id" json:"id"`
	Strategy      string        `json:"strategy"`
	StrategyDescr string        `json:"strategy_descr"`
}

type ReplaceInstructionMsg struct {
	TradingUserID bson.ObjectId        `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	Instructions  []ReplaceInstruction `json:"instructions"`
}

type ReplaceInstruction struct {
	Price         *float64                                  `json:"price,omitempty"`
	Size          *float64                                  `bson:"size,omitempty" json:"size"`
	TargetSize    *float64                                  `bson:"target_size" json:"target_size"`
	SorCodes      *map[SorCode]interface{}                  `bson:"sor_codes,omitempty" json:"sor_codes,omitempty"`
	TTL           *time.Time                                `bson:"ttl,omitempty" json:"ttl,omitempty"`
	ID            bson.ObjectId                             `bson:"id" json:"id"`
	RestrictTo    *map[bookmakers.Bookmaker][]bson.ObjectId `bson:"restrict_to,omitempty" json:"restrict_to,omitempty"`
	Persistence   *Persistence                              `bson:"persistence,omitempty" json:"persistence,omitempty"`
	Force         bool                                      `bson:"force" json:"force"`
	Strategy      string                                    `json:"strategy"`
	StrategyDescr string                                    `json:"strategy_descr"`
}

type StartSorCodeMsg struct {
	TradingUserID bson.ObjectId  `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	SorCodes      []StartSorCode `json:"sor_codes"`
}

type StartSorCode struct {
	ID            bson.ObjectId                             `bson:"id" json:"id"`
	SorCodes      *map[SorCode]interface{}                  `bson:"sor_codes,omitempty" json:"sor_codes,omitempty"`
	RestrictTo    *map[bookmakers.Bookmaker][]bson.ObjectId `bson:"restrict_to,omitempty" json:"restrict_to,omitempty"`
	Strategy      string                                    `json:"strategy"`
	StrategyDescr string                                    `json:"strategy_descr"`
}

type ResetInstructionMsg struct {
	TradingUserID bson.ObjectId      `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	Instructions  []ResetInstruction `json:"instructions"`
}

type ResetInstruction struct {
	ID            bson.ObjectId `bson:"id" json:"id"`
	Strategy      string        `json:"strategy"`
	StrategyDescr string        `json:"strategy_descr"`
}

type UpdateStrategyInstructionsMsg struct {
	TradingUserId bson.ObjectId   `bson:"trading_user_id" json:"trading_user_id"`
	IDs           []bson.ObjectId `bson:"ids" json:"ids"`
	Strategy      string          `json:"strategy"`
	StrategyDescr string          `json:"strategy_descr"`
}
type UpdateStrategyInstruction struct {
	ID            bson.ObjectId `bson:"id" json:"id"`
	Strategy      string        `json:"strategy"`
	StrategyDescr string        `json:"strategy_descr"`
}
