package trading

import "bitbucket.org/onsideanalysis/data_api/schemas/sport"

type Portfolio struct {
	Strategies []Strategy `bson:"strategies,omitempty" json:"strategies"`
}

func (p *Portfolio) Styles() []StrategyStyle {
	var styles []StrategyStyle
	for _, strategy := range p.Strategies {
		styles = append(styles, strategy.Style)
	}
	return styles
}

func (p *Portfolio) DistinctSports() []sport.Sport {
	var sports []sport.Sport
	seenSports := make(map[sport.Sport]bool)
	for _, strategy := range p.Strategies {
		if _, has := seenSports[strategy.Sport]; !has {
			seenSports[strategy.Sport] = true
			sports = append(sports, strategy.Sport)
		}
	}
	return sports
}

func (p *Portfolio) Sports() []sport.Sport {
	var sports []sport.Sport
	for _, strategy := range p.Strategies {
		sports = append(sports, strategy.Sport)
	}
	return sports
}

func (p *Portfolio) StrategiesNames() []string {
	var strategies []string
	for _, strategy := range p.Strategies {
		strategies = append(strategies, strategy.Name)
	}
	return strategies
}

func (p *Portfolio) Competitions() map[sport.Sport][]int {
	competitions := make(map[sport.Sport][]int)
	competitionsEmpty := make(map[sport.Sport]bool)
	for _, strategy := range p.Strategies {
		if sportCompetitions, has := competitions[strategy.Sport]; has {
			sCompetitions := sportCompetitions
			sCompetitions = append(sCompetitions, strategy.Competitions[strategy.Name]...)
			competitions[strategy.Sport] = sCompetitions
			if len(strategy.Competitions[strategy.Name]) == 0 {
				competitionsEmpty[strategy.Sport] = true
			}
		} else {
			competitions[strategy.Sport] = strategy.Competitions[strategy.Name]
			if len(strategy.Competitions[strategy.Name]) == 0 {
				competitionsEmpty[strategy.Sport] = true
			}
		}
	}
	for key, _ := range competitionsEmpty {
		competitions[key] = []int{}
	}
	return competitions
}
