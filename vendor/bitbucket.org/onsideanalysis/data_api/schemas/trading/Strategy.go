package trading

import "bitbucket.org/onsideanalysis/data_api/schemas/sport"

type StrategyStyle string
type StrategyType string

const (
	DEADBALL   StrategyStyle = "dead-ball"
	INVESTMENT StrategyStyle = "investment"
	INPLAY     StrategyStyle = "in-play"

	SYSTEMATIC StrategyType = "Systematic"
	RnD        StrategyType = "RnD"

	STRATEGY_UPDATES string = "strategy_updates"
)

func (s StrategyStyle) String() string {
	return string(s)
}

func (s StrategyType) String() string {
	return string(s)
}

type Strategy struct {
	Sport        sport.Sport      `bson:"sport,omitempty" json:"sport"`
	Style        StrategyStyle    `bson:"style,omitempty" json:"style"`
	Name         string           `bson:"name,omitempty" json:"name"`
	Descriptions []string         `bson:"descriptions,omitempty" json:"descriptions"`
	Competitions map[string][]int `bson:"competitions" json:"competitions"`
	HasSource    bool             `bson:"has_source" json:"has_source"`
}
