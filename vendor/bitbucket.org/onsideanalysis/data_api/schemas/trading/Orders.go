package trading

import (
	"time"

	"bytes"
	"encoding/json"
	"fmt"

	"bitbucket.org/onsideanalysis/data_api/schemas/bookmakers"
	"bitbucket.org/onsideanalysis/data_api/schemas/football"
	"bitbucket.org/onsideanalysis/data_api/schemas/stickers"
	"gopkg.in/mgo.v2/bson"
)

type OrderStatus int
type BetSide string
type Source string

const (
	UNMATCHED           OrderStatus = 1
	PARTIALLY_MATCHED   OrderStatus = 2
	MATCHED             OrderStatus = 3
	CANCELLED           OrderStatus = 4
	SETTLED             OrderStatus = 5
	LAPSED              OrderStatus = 6
	PARTIALLY_CANCELLED OrderStatus = 7
	VOIDED              OrderStatus = 8
	REJECTED            OrderStatus = 9
	PENDING             OrderStatus = 10
	ORDER_FAILED        OrderStatus = 11
	ORDER_DELETED       OrderStatus = 12
	UNKNOWN             OrderStatus = -1

	ORDERS_CANCELLATION     string = "orders_cancellation"
	ORDERS_REPLACE          string = "orders_replace"
	ORDERS_EXECUTION        string = "orders_execution"
	MANUAL_ORDERS_EXECUTION string = "manual_orders_execution"
	MANUAL_ORDERS_UPDATE    string = "manual_orders_update"
	MANUAL_ORDERS_DELETION  string = "manual_orders_deletion"

	BACK BetSide = "back"
	LAY  BetSide = "lay"
	BUY  BetSide = "buy"
	SELL BetSide = "sell"

	MANUAL         Source = "manual"
	STRATABET      Source = "stratabet"
	TRADING_SYSTEM Source = "trading_system"
	BOOKMAKER      Source = "bookmaker"
	UNKNOWN_SOURCE Source = "unknown"
)

var (
	ALL_STATUSES = map[OrderStatus]struct{}{
		UNMATCHED:           {},
		PARTIALLY_MATCHED:   {},
		MATCHED:             {},
		CANCELLED:           {},
		SETTLED:             {},
		LAPSED:              {},
		PARTIALLY_CANCELLED: {},
		VOIDED:              {},
		REJECTED:            {},
		PENDING:             {},
		UNKNOWN:             {},
		ORDER_FAILED:        {},
		ORDER_DELETED:       {},
	}
	ACTIVE_STATUSES = map[OrderStatus]struct{}{
		UNMATCHED:           {},
		PARTIALLY_MATCHED:   {},
		MATCHED:             {},
		PARTIALLY_CANCELLED: {},
		PENDING:             {},
		UNKNOWN:             {},
	}
	INACTIVE_STATUSES = map[OrderStatus]struct{}{
		CANCELLED:     {},
		SETTLED:       {},
		LAPSED:        {},
		VOIDED:        {},
		REJECTED:      {},
		ORDER_FAILED:  {},
		ORDER_DELETED: {},
	}
)

func (o OrderStatus) String() string {
	switch o {
	case UNMATCHED:
		return "UNMATCHED"
	case PARTIALLY_MATCHED:
		return "PARTIALLY_MATCHED"
	case MATCHED:
		return "MATCHED"
	case CANCELLED:
		return "CANCELLED"
	case SETTLED:
		return "SETTLED"
	case LAPSED:
		return "LAPSED"
	case PARTIALLY_CANCELLED:
		return "PARTIALLY_CANCELLED"
	case VOIDED:
		return "VOIDED"
	case REJECTED:
		return "REJECTED"
	case PENDING:
		return "PENDING"
	case UNKNOWN:
		return "UNKNOWN"
	case ORDER_FAILED:
		return "FAILED"
	default:
		return ""
	}
}

func (o OrderStatus) IsActive() bool {
	if _, ok := ACTIVE_STATUSES[o]; ok {
		return true
	}
	return false
}

func (b BetSide) IsBack() bool {
	return b == BACK
}

type OrderType string

const (
	LIMIT  OrderType = "limit"
	MARKET OrderType = "market"
	// MarketWithLeftOverAsLimit
	STOP              OrderType = "stop"              // A Stop Loss Market order.
	STOP_LIMIT        OrderType = "stop_limit"        // A Stop Loss Limit order
	MARKET_IF_TOUCHED           = "market_if_touched" // A Take Profit Market order
	LIMIT_IF_TOUCHED            = "limit_if_touched"  // A Take Profit Limit order
)

type OrderHistory struct {
	OrderID             bson.ObjectId `bson:"id,omitempty" json:"id,omitempty"`
	Ut                  time.Time     `bson:"ut" json:"ut"`
	SizeMatched         float64       `bson:"size_matched" json:"size_matched"`
	SizeLapsed          float64       `bson:"size_lapsed" json:"size_lapsed"`
	SizeRejected        float64       `bson:"size_rejected" json:"size_rejected"`
	SizeCancelled       float64       `bson:"size_cancelled" json:"size_cancelled"`
	SizeVoided          float64       `bson:"size_voided" json:"size_voided"`
	AveragePriceMatched float64       `bson:"average_price_matched" json:"average_price_matched"`
	SizeRemaining       float64       `bson:"size_remaining" json:"size_remaining"`
	Status              OrderStatus   `bson:"status" json:"status"`
	Persistence         Persistence   `bson:"persistence" json:"persistence"`
	Outcome             *Outcome      `bson:"outcome,omitempty" json:"outcome,omitempty"`
	Size                float64       `bson:"size,omitempty" json:"size,omitempty"`
	Price               float64       `bson:"price,omitempty" json:"price,omitempty"`
}

type Order struct {
	OrderID             bson.ObjectId          `bson:"id,omitempty" json:"id,omitempty"`
	PlacedTime          time.Time              `bson:"placed_time,omitempty" json:"placed_time,omitempty"`
	Ut                  time.Time              `bson:"ut,omitempty" json:"ut,omitempty"`
	TradingUserID       bson.ObjectId          `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	AccountID           bson.ObjectId          `bson:"account_id,omitempty" json:"account_id,omitempty"`
	TradeID             bson.ObjectId          `bson:"trade_id,omitempty" json:"trade_id,omitempty"`
	Strategy            string                 `bson:"strategy,omitempty" json:"strategy,omitempty"`
	StrategyDescr       string                 `bson:"strategy_descr,omitempty" json:"strategy_descr,omitempty"`
	InstructionID       bson.ObjectId          `bson:"instruction_id,omitempty" json:"instruction_id,omitempty"`
	ExecutionDetails    ExecutionDetails       `bson:"execution_details,omitempty" json:"execution_details,omitempty"`
	Size                float64                `bson:"size" json:"size"`
	Price               float64                `bson:"price" json:"price"`
	PriceRequested      float64                `bson:"price_requested" json:"price_requested"`
	SizeMatched         float64                `bson:"size_matched" json:"size_matched"`
	SizeVoided          float64                `bson:"size_voided" json:"size_voided"`
	SizeLapsed          float64                `bson:"size_lapsed" json:"size_lapsed"`
	SizeCancelled       float64                `bson:"size_cancelled" json:"size_cancelled"`
	SizeRejected        float64                `bson:"size_rejected" json:"size_rejected"`
	AveragePriceMatched float64                `bson:"average_price_matched" json:"average_price_matched"`
	SizeRemaining       float64                `bson:"size_remaining" json:"size_remaining"`
	OrderType           OrderType              `bson:"order_type,omitempty" json:"order_type,omitempty"`
	Status              OrderStatus            `bson:"status" json:"status"`
	BetSide             BetSide                `bson:"bet_side" json:"bet_side"`
	Sticker             stickers.Sticker       `bson:"sticker,omitempty" json:"sticker"`
	Source              Source                 `bson:"source,omitempty" json:"source,omitempty"`
	Currency            Currency               `bson:"currency" json:"currency"`
	ExchangeRate        float64                `bson:"exchange_rate" json:"exchange_rate"`
	RawProviderDetails  *RawProviderDetails    `bson:"raw_provider_details,omitempty" json:"raw_provider_details,omitempty"`
	Outcome             *Outcome               `bson:"outcome,omitempty" json:"outcome,omitempty"`
	Persistence         Persistence            `bson:"persistence" json:"persistence"`
	MatchedBets         []MatchedBet           `bson:"matched_bets,omitempty" json:"matched_bets,omitempty"`
	ErrorMsg            string                 `bson:"error_msg,omitempty" json:"error_msg,omitempty"`
	LinkedInstructionID bson.ObjectId          `bson:"linked_instruction_id,omitempty" json:"linked_instruction_id,omitempty"`
	Details             map[string]interface{} `bson:"details,omitempty" json:"details,omitempty"`
	CryptoRawDetails    *CryptoRawDetails      `bson:"crypto_raw_details,omitempty" json:"crypto_raw_details,omitempty"`
}

type ManualBetInfo struct {
	Sticker       stickers.Sticker     `json:"sticker,omitempty"`
	StartDate     time.Time            `json:"start_date,omitempty"`
	Market        string               `json:"market,omitempty"`
	Selection     string               `json:"selection,omitempty"`
	Event         string               `json:"event,omitempty"`
	Competition   string               `json:"competition,omitempty"`
	Sport         string               `json:"sport,omitempty"`
	Bookmaker     bookmakers.Bookmaker `json:"bookmaker,omitempty"`
	IsBack        bool                 `json:"is_back,omitempty"`
	Outcome       stickers.Outcome     `json:"outcome_id,omitempty"`
	Provider      bookmakers.Provider  `json:"provider,omitempty"`
	Strategy      string               `json:"strategy,omitempty"`
	StrategyDescr string               `json:"strategy_descr,omitempty"`
	OrderOdds     float64              `json:"order_odds,omitempty"`
	OrderSize     float64              `json:"order_size,omitempty"`
	Payoff        float64              `json:"payoff,omitempty"`
	Commission    float64              `json:"commission,omitempty"`
	BetId         string               `json:"bet_id,omitempty"`
	OrderId       bson.ObjectId        `json:"order_id,omitempty"`
	PlacedDate    time.Time            `json:"placed_date,omitempty"`
	SettledDate   time.Time            `json:"settled_date,omitempty"`
}

func (o Order) ToHistory() OrderHistory {
	return OrderHistory{
		OrderID:             o.OrderID,
		Ut:                  o.Ut,
		Status:              o.Status,
		SizeMatched:         o.SizeMatched,
		Persistence:         o.Persistence,
		SizeCancelled:       o.SizeCancelled,
		AveragePriceMatched: o.AveragePriceMatched,
		SizeRejected:        o.SizeRejected,
		SizeLapsed:          o.SizeLapsed,
		SizeVoided:          o.SizeVoided,
		SizeRemaining:       o.SizeRemaining,
		Outcome:             o.Outcome,
		Price:               o.Price,
		Size:                o.Size,
	}
}

func OrderListToMap(orderList []Order) map[string]Order {

	orderMap := make(map[string]Order, len(orderList))
	for _, order := range orderList {
		orderMap[order.OrderID.Hex()] = order
	}
	return orderMap
}

func (o Order) GetReplaceOrder(size, price float64, persistence Persistence) ReplaceOrderMsg {
	return ReplaceOrderMsg{
		TradingUserID: o.TradingUserID,
		Orders: []ReplaceOrder{
			{
				OrderID:          o.OrderID,
				InstructionID:    o.InstructionID,
				Price:            o.Price,
				OriginalSize:     o.Size,
				Size:             size,
				Persistence:      persistence,
				AccountID:        o.AccountID,
				ExecutionDetails: o.ExecutionDetails,
				Strategy:         o.Strategy,
				StrategyDescr:    o.StrategyDescr,
				Sticker:          o.Sticker,
				BetSide:          o.BetSide,
				TradeID:          o.TradeID,
				Currency:         o.Currency,
			},
		},
	}
}

func (o Order) GetCancelOrder(sizeReduction float64) CancelOrderMsg {
	return CancelOrderMsg{
		TradingUserID: o.TradingUserID,
		Orders: []CancelOrder{
			{
				OrderID:          o.OrderID,
				InstructionID:    o.InstructionID,
				AccountID:        o.AccountID,
				ExecutionDetails: o.ExecutionDetails,
				Strategy:         o.Strategy,
				StrategyDescr:    o.StrategyDescr,
				SizeReduction:    sizeReduction,
			},
		},
	}
}

func (o Order) GetDeleteOrder() DeleteManualOrderMsg {
	return DeleteManualOrderMsg{
		TradingUserID: o.TradingUserID,
		ManualOrders: []DeleteManualOrder{
			{
				OrderID:          o.OrderID,
				InstructionID:    o.LinkedInstructionID,
				AccountID:        o.AccountID,
				Strategy:         o.Strategy,
				StrategyDescr:    o.StrategyDescr,
				ExecutionDetails: o.ExecutionDetails,
			},
		},
	}
}

type MatchedBet struct {
	Size      float64              `json:"size"`
	Dt        time.Time            `json:"dt"`
	Price     float64              `json:"price"`
	BetID     string               `bson:"bet_id,omitempty" json:"bet_id,omitempty"`
	Bookmaker bookmakers.Bookmaker `bson:"bookmaker,omitempty" json:"bookmaker,omitempty"`
}

type Outcome struct {
	ID         stickers.Outcome `json:"id"`
	Gross      float64          `json:"gross"`
	Commission float64          `json:"commission"`
	Net        float64          `json:"net"`
}

type CryptoRawDetails struct {
	Symbol string
	Text   string
}

type RawProviderDetails struct {
	StartDate   time.Time `bson:"start_date,omitempty" json:"start_date,omitempty"`
	Competition string    `bson:"competition,omitempty" json:"competition,omitempty"`
	Selection   string    `bson:"selection,omitempty" json:"selection,omitempty"`
	Handicap    float64   `bson:"handicap,omitempty" json:"handicap,omitempty"`
	Market      string    `bson:"market,omitempty" json:"market,omitempty"`
	Event       string    `bson:"event,omitempty" json:"event,omitempty"`
	Sport       string    `bson:"sport,omitempty" json:"sport,omitempty"`
	Accumulator bool      `bson:"accumulator,omitempty" json:"accumulator,omitempty"`
}

type UpdateOrderStrategyMsg struct {
	TradingUserID bson.ObjectId              `bson:"trading_user_id" json:"trading_user_id"`
	Orders        map[string][]bson.ObjectId `bson:"orders" json:"orders"`
	Strategy      string                     `json:"strategy"`
	StrategyDescr string                     `json:"strategy_descr"`
}

type PriceSize struct {
	Price float64 `json:"price"`
	Size  float64 `json:"size"`
}

type commandResponseErrors map[bson.ObjectId]string

func (c commandResponseErrors) MarshalJSON() ([]byte, error) {
	buffer := bytes.NewBufferString("{")
	length := len(c)
	count := 0
	for key, value := range c {
		jsonValue, err := json.Marshal(value)
		if err != nil {
			return nil, err
		}
		buffer.WriteString(fmt.Sprintf("\"%s\":%s", key.Hex(), string(jsonValue)))
		count++
		if count < length {
			buffer.WriteString(",")
		}
	}
	buffer.WriteString("}")
	return buffer.Bytes(), nil
}

func (c commandResponseErrors) UnmarshalJSON(b []byte) error {
	var temp map[string]string

	err := json.Unmarshal(b, &temp)
	if err != nil {
		return err
	}

	for key, value := range temp {
		c[bson.ObjectIdHex(key)] = value
	}

	return nil
}

type CommandResponse struct {
	// we can't use bson.ObjectId as map key due to known mgo bug
	// https://groups.google.com/forum/#!topic/golang-nuts/J_07LzGwyn8
	Errors    commandResponseErrors `json:"errors"`
	Successes []bson.ObjectId       `json:"successes"`

	FatalError string `json:"fatal_error"`
}

func NewCommandResponse() CommandResponse {
	return CommandResponse{
		Errors:    make(commandResponseErrors),
		Successes: make([]bson.ObjectId, 0),
	}
}

func (PlaceOrder) omsCommandLevel() omsCommandLevel                { return order }
func (PlaceMultiOrder) omsCommandLevel() omsCommandLevel           { return order }
func (CancelOrder) omsCommandLevel() omsCommandLevel               { return order }
func (ReplaceOrder) omsCommandLevel() omsCommandLevel              { return order }
func (ManualOrderExecution) omsCommandLevel() omsCommandLevel      { return order }
func (UpdateManualOrder) omsCommandLevel() omsCommandLevel         { return order }
func (MultiManualOrderExecution) omsCommandLevel() omsCommandLevel { return order }
func (DeleteManualOrder) omsCommandLevel() omsCommandLevel         { return order }

type PlaceOrderMsg struct {
	TradingUserID bson.ObjectId `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	Orders        []PlaceOrder  `json:"orders"`
}

type PlaceMultiOrder []PlaceOrder
type MultiManualOrderExecution []ManualOrderExecution

type PlaceOrder struct {
	AccountID     bson.ObjectId          `json:"account_id,omitempty"`
	OrderID       bson.ObjectId          `json:"order_id"`
	Bookmaker     bookmakers.Bookmaker   `json:"bookmaker"`
	InstructionID bson.ObjectId          `json:"instruction_id"`
	TradeID       bson.ObjectId          `json:"trade_id"`
	Strategy      string                 `json:"strategy"`
	StrategyDescr string                 `json:"strategy_descr"`
	Details       map[string]interface{} `json:"details,omitempty"`
	Sticker       stickers.Sticker       `json:"sticker"`
	Persistence   Persistence            `json:"persistence"`
	OrderType     OrderType              `json:"order_type"`
	Price         float64                `json:"price"`
	Size          float64                `json:"size"`
	BetSide       BetSide                `json:"bet_side"`
	Currency      Currency               `json:"currency"`
	Source        Source                 `bson:"source" json:"source"`
}

type ManualOrderExecutionMsg struct {
	TradingUserID bson.ObjectId          `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	ManualOrders  []ManualOrderExecution `json:"manual_orders"`
}

type ManualOrderExecution struct {
	OrderID             bson.ObjectId       `bson:"id,omitempty" json:"id,omitempty"`
	PlacedTime          time.Time           `bson:"placed_time,omitempty" json:"placed_time,omitempty"`
	Ut                  time.Time           `bson:"ut,omitempty" json:"ut,omitempty"`
	AccountID           bson.ObjectId       `bson:"account_id,omitempty" json:"account_id,omitempty"`
	TradeID             bson.ObjectId       `bson:"trade_id,omitempty" json:"trade_id,omitempty"`
	Strategy            string              `bson:"strategy,omitempty" json:"strategy,omitempty"`
	StrategyDescr       string              `bson:"strategy_descr,omitempty" json:"strategy_descr,omitempty"`
	ExecutionDetails    ExecutionDetails    `bson:"execution_details,omitempty" json:"execution_details,omitempty"`
	Size                float64             `bson:"size" json:"size"`
	Price               float64             `bson:"price" json:"price"`
	PriceRequested      float64             `bson:"price_requested" json:"price_requested"`
	SizeMatched         float64             `bson:"size_matched" json:"size_matched"`
	AveragePriceMatched float64             `bson:"average_price_matched" json:"average_price_matched"`
	Status              OrderStatus         `bson:"status" json:"status"`
	BetSide             BetSide             `bson:"bet_side" json:"bet_side"`
	Sticker             stickers.Sticker    `bson:"sticker,omitempty" json:"sticker"`
	Source              Source              `bson:"source,omitempty" json:"source,omitempty"`
	Currency            Currency            `bson:"currency" json:"currency"`
	ExchangeRate        float64             `bson:"exchange_rate" json:"exchange_rate"`
	RawProviderDetails  *RawProviderDetails `bson:"raw_provider_details,omitempty" json:"raw_provider_details,omitempty"`
	Outcome             *Outcome            `bson:"outcome,omitempty" json:"outcome,omitempty"`
	Persistence         Persistence         `bson:"persistence" json:"persistence"`
	MatchedBets         []MatchedBet        `bson:"matched_bets,omitempty" json:"matched_bets,omitempty"`
	InstructionID       bson.ObjectId       `bson:"instruction_id,omitempty" json:"instruction_id,omitempty"`
	CryptoRawDetails    *CryptoRawDetails   `bson:"crypto_raw_details,omitempty" json:"crypto_raw_details,omitempty"`
}

func (p ManualOrderExecution) ToOrder() Order {
	return Order{
		OrderID:             p.OrderID,
		Ut:                  p.Ut,
		Status:              p.Status,
		SizeMatched:         p.SizeMatched,
		Persistence:         p.Persistence,
		AveragePriceMatched: p.AveragePriceMatched,
		Outcome:             p.Outcome,
		MatchedBets:         p.MatchedBets,
		Source:              p.Source,
		ExecutionDetails:    p.ExecutionDetails,
		BetSide:             p.BetSide,
		Currency:            p.Currency,
		ExchangeRate:        p.ExchangeRate,
		RawProviderDetails:  p.RawProviderDetails,
		InstructionID:       p.InstructionID,
		Sticker:             p.Sticker,
		PriceRequested:      p.PriceRequested,
		Price:               p.Price,
		AccountID:           p.AccountID,
		TradeID:             p.TradeID,
		PlacedTime:          p.PlacedTime,
		Strategy:            p.Strategy,
		StrategyDescr:       p.StrategyDescr,
		Size:                p.Size,
	}
}

type UpdateManualOrderMsg struct {
	TradingUserID bson.ObjectId       `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	ManualOrders  []UpdateManualOrder `json:"manual_orders"`
}

type UpdateManualOrder struct {
	OrderID             bson.ObjectId       `bson:"id,omitempty" json:"id,omitempty"`
	PlacedTime          *time.Time          `bson:"placed_time,omitempty" json:"placed_time,omitempty"`
	Ut                  *time.Time          `bson:"ut,omitempty" json:"ut,omitempty"`
	AccountID           bson.ObjectId       `bson:"account_id,omitempty" json:"account_id,omitempty"`
	TradeID             bson.ObjectId       `bson:"trade_id,omitempty" json:"trade_id,omitempty"`
	Strategy            string              `bson:"strategy,omitempty" json:"strategy,omitempty"`
	StrategyDescr       string              `bson:"strategy_descr,omitempty" json:"strategy_descr,omitempty"`
	ExecutionDetails    *ExecutionDetails   `bson:"execution_details,omitempty" json:"execution_details,omitempty"`
	Size                float64             `bson:"size" json:"size"`
	OriginalSize        float64             `bson:"original_size" json:"original_size"`
	Price               float64             `bson:"price" json:"price"`
	PriceRequested      float64             `bson:"price_requested" json:"price_requested"`
	SizeMatched         float64             `bson:"size_matched" json:"size_matched"`
	AveragePriceMatched float64             `bson:"average_price_matched" json:"average_price_matched"`
	Status              OrderStatus         `bson:"status" json:"status"`
	BetSide             BetSide             `bson:"bet_side" json:"bet_side"`
	Sticker             stickers.Sticker    `bson:"sticker,omitempty" json:"sticker"`
	Currency            Currency            `bson:"currency" json:"currency"`
	ExchangeRate        float64             `bson:"exchange_rate" json:"exchange_rate"`
	RawProviderDetails  *RawProviderDetails `bson:"raw_provider_details,omitempty" json:"raw_provider_details,omitempty"`
	Outcome             *Outcome            `bson:"outcome,omitempty" json:"outcome,omitempty"`
	Persistence         Persistence         `bson:"persistence" json:"persistence"`
	MatchedBets         *[]MatchedBet       `bson:"matched_bets,omitempty" json:"matched_bets,omitempty"`
	InstructionID       bson.ObjectId       `bson:"instruction_id,omitempty" json:"instruction_id,omitempty"`
	CryptoRawDetails    *CryptoRawDetails   `bson:"crypto_raw_details,omitempty" json:"crypto_raw_details,omitempty"`
}

type DeleteManualOrderMsg struct {
	TradingUserID bson.ObjectId       `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	ManualOrders  []DeleteManualOrder `json:"manual_orders"`
}

type DeleteManualOrder struct {
	OrderID          bson.ObjectId    `bson:"id" json:"id"`
	InstructionID    bson.ObjectId    `json:"instruction_id"`
	AccountID        bson.ObjectId    `bson:"account_id" json:"account_id"`
	Strategy         string           `json:"strategy"`
	StrategyDescr    string           `json:"strategy_descr"`
	ExecutionDetails ExecutionDetails `bson:"execution_details,omitempty" json:"execution_details,omitempty"`
}

func (p PlaceOrder) Liability() float64 {
	switch p.BetSide {
	case BACK:
		return p.Size
	case LAY:
		return p.Size * (p.Price - 1)
	default:
		return 0
	}
}

type CancelOrderMsg struct {
	TradingUserID bson.ObjectId `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	Orders        []CancelOrder `json:"orders"`
}

type CancelOrder struct {
	OrderID          bson.ObjectId    `bson:"id,omitempty" json:"id,omitempty"`
	InstructionID    bson.ObjectId    `json:"instruction_id"`
	AccountID        bson.ObjectId    `bson:"account_id,omitempty" json:"account_id,omitempty"`
	ExecutionDetails ExecutionDetails `bson:"execution_details,omitempty" json:"execution_details,omitempty"`
	SizeReduction    float64          `json:"size_reduction,omitempty"`
	Strategy         string           `json:"strategy"`
	StrategyDescr    string           `json:"strategy_descr"`
}

type ReplaceOrderMsg struct {
	TradingUserID bson.ObjectId  `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	Orders        []ReplaceOrder `json:"orders"`
}

type ReplaceOrder struct {
	OrderID          bson.ObjectId    `bson:"id,omitempty" json:"id,omitempty"`
	InstructionID    bson.ObjectId    `json:"instruction_id"`
	Price            float64          `json:"price,omitempty"`
	OriginalSize     float64          `json:"original_size,omitempty"`
	Size             float64          `bson:"size,omitempty" json:"size"`
	Persistence      Persistence      `bson:"persistence" json:"persistence"`
	AccountID        bson.ObjectId    `bson:"account_id,omitempty" json:"account_id,omitempty"`
	ExecutionDetails ExecutionDetails `bson:"execution_details,omitempty" json:"execution_details,omitempty"`
	Strategy         string           `json:"strategy"`
	StrategyDescr    string           `json:"strategy_descr"`
	Sticker          stickers.Sticker `bson:"sticker,omitempty" json:"sticker,omitempty"`
	BetSide          BetSide          `json:"bet_side"`
	TradeID          bson.ObjectId    `json:"trade_id"`
	Currency         Currency         `json:"currency"`
	Source           Source           `bson:"source" json:"source"`
}

type ExecutionDetails struct {
	Bookmaker bookmakers.Bookmaker `bson:"bookmaker" json:"bookmaker"`
	Provider  bookmakers.Provider  `bson:"provider" json:"provider"`
	MarketID  string               `bson:"market_id,omitempty" json:"market_id,omitempty"`
	BetID     string               `bson:"bet_id" json:"bet_id"`
}

type FixtureOrderTeam struct {
	Area struct {
		GsmAbbr string `bson:"gsm_abbr" json:"gsm_abbr"`
	} `bson:"area" json:"area"`
	GsmID int           `bson:"gsm_id" json:"gsm_id"`
	Name  football.Name `bson:"name" json:"name"`
}

type FixtureOrder struct {
	GsmID       int              `bson:"gsm_id" json:"gsm_id"`
	Team1       FixtureOrderTeam `bson:"team1" json:"team1"`
	Team2       FixtureOrderTeam `bson:"team2" json:"team2"`
	Competition struct {
		GsmID int           `bson:"gsm_id" json:"gsm_id"`
		Name  football.Name `bson:"name" json:"name"`
	} `bson:"competition" json:"competition"`
	KickOff struct {
		FixtureWeek int `bson:"fixture_week" json:"fixture_week"`
		Utc         struct {
			Fd struct {
				Date time.Time `bson:"date" json:"date"`
			} `bson:"fd" json:"fd"`
			Gsm struct {
				DateTime time.Time `bson:"date_time" json:"date_time"`
			} `bson:"gsm" json:"gsm"`
		} `bson:"utc" json:"utc"`
	} `bson:"kick_off" json:"kick_off"`
	Type struct {
		Format string `bson:"format" json:"format"`
		Status string `bson:"status" json:"status"`
	} `bson:"type" json:"type"`
	Area   football.SharedArea `bson:"area" json:"area"`
	Orders []Order             `bson:"orders" json:"orders"`
}
