package trading

type OddsFormat string

const (
	AMERICAN   OddsFormat = "american"
	DECIMAL    OddsFormat = "decimal"
	HONGKONG   OddsFormat = "hongkong"
	INDONESIAN OddsFormat = "indonesian"
	MALAY      OddsFormat = "malay"
)

func (o OddsFormat) String() string {
	switch o {
	case AMERICAN:
		return "american"
	case DECIMAL:
		return "decimal"
	case HONGKONG:
		return "hongkong"
	case INDONESIAN:
		return "indonesian"
	case MALAY:
		return "malay"
	}
	return ""
}
