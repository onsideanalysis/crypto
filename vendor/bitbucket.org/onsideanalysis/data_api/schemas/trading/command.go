package trading

type omsCommandLevel uint32

const (
	order omsCommandLevel = iota
	instruction
)

// marker interface for OMS commands
type OMSCommand interface {
	omsCommandLevel() omsCommandLevel
}
