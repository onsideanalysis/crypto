package trading

import (
	"time"

	"bitbucket.org/onsideanalysis/data_api/schemas/sport"

	"gopkg.in/mgo.v2/bson"
)

type Allocation struct {
	Dt              time.Time     `bson:"dt,omitempty" json:"dt"`
	Sport           sport.Sport   `bson:"sport,omitempty" json:"sport"`
	Style           StrategyStyle `bson:"style,omitempty" json:"style"`
	Strategy        string        `bson:"strategy,omitempty" json:"strategy"`
	StrategyDescr   string        `bson:"strategy_descr,omitempty" json:"strategy_descr"`
	AllocationLevel int           `bson:"allocation_level" json:"allocation_level"`
	Allocation      float64       `bson:"allocation" json:"allocation"`
	TradingUserID   bson.ObjectId `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	UpdatedBy       string        `bson:"updated_by,omitempty" json:"updated_by"`
}

type AUM struct {
	TradingUserID bson.ObjectId `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	Dt            time.Time     `bson:"dt,omitempty" json:"dt"`
	FirmAum       float64       `bson:"firm_aum,omitempty" json:"firm_aum"`
	UpdatedBy     string        `bson:"updated_by,omitempty" json:"updated_by"`
}

type AllocationExpanded struct {
	AllocationInfo Allocation `bson:"allocation_info,omitempty" json:"allocation_info"`
	//YtdPnl         float64    `bson:"ytd_pnl,omitempty" json:"ytd_pnl"`
	//MonthlyPnl     float64    `bson:"montly_pnl,omitempty" json:"monthly_pnl"`
	//WeeklyPnl      float64    `bson:"weekly_pnl,omitempty" json:"weekly_pnl"`
	//YtdStake       float64    `bson:"ytd_stake,omitempty" json:"ytd_stake"`
	//MonthlyStake   float64    `bson:"montly_stake,omitempty" json:"monthly_stake"`
	//WeeklyStake    float64    `bson:"weekly_stake,omitempty" json:"weekly_stake"`
	DailyStake float64 `bson:"daily_stake,omitempty" json:"daily_stake"`
	DailyPnl   float64 `bson:"daily_pnl,omitempty" json:"daily_pnl"`
}

type AllocationStrInfo struct {
	Sport         sport.Sport `bson:"sport,omitempty" json:"sport"`
	Strategy      string      `bson:"strategy,omitempty" json:"strategy"`
	StrategyDescr string      `bson:"strategy_descr,omitempty" json:"strategy_descr"`
}
