package trading

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type adjustmentType string

var (
	BET_ADJUSTMENT adjustmentType = "bet"
)

type Adjustment struct {
	ID            bson.ObjectId  `bson:"_id,omitempty" json:"_id"`
	Dt            time.Time      `bson:"dt,omitempty" json:"dt"`
	Type          adjustmentType `bson:"type,omitempty" json:"type"`
	Value         float64        `bson:"value" json:"value"`
	TradingUserID bson.ObjectId  `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
}
