package trading

import (
	"bitbucket.org/onsideanalysis/data_api/schemas/bookmakers"
	"bitbucket.org/onsideanalysis/data_api/schemas/sport"
	"gopkg.in/mgo.v2/bson"
)

type SummarisedMetrics struct {
	TradingUserID bson.ObjectId                 `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	Dt            string                        `bson:"dt,omitempty" json:"dt,omitempty"`
	Metrics       map[MetricsPeriod]MetricsData `bson:"metrics,omitempty" json:"metrics,omitempty"`
	ParentName    string                        `bson:"parent_name,omitempty" json:"parent_name,omitempty"`
	Name          string                        `bson:"name,omitempty" json:"name,omitempty"`
	Level         int                           `bson:"level,omitempty" json:"level,omitempty"`
}
type MetricsData struct {
	Metrics   Metrics     `json:"metrics"`
	DailyData []DailyData `json:"dailyData"`
}

type Metrics struct {
	NBets                        int                    `json:"nBets"`
	NTrades                      int                    `json:"nTrades"`
	NFixtures                    int                    `json:"nFixtures"`
	NTradeDays                   int                    `json:"nTradeDays"`
	NActualTradeDays             int                    `json:"nActualTradeDays"`
	DateRange                    string                 `json:"dateRange"`
	SizePlaced                   float64                `json:"sizePlaced"`
	SizeMatched                  float64                `json:"sizeMatched"`
	NetPnl                       float64                `json:"netPnl"`
	GrossPnl                     float64                `json:"grossPnl"`
	Commission                   float64                `json:"commission"`
	NBetWins                     int                    `json:"nBetWins"`
	NBetLosses                   int                    `json:"nBetLosses"`
	NBetReturn                   int                    `json:"nBetReturn"`
	AvgBetWins                   float64                `json:"avgBetWins"`
	AvgBetLosses                 float64                `json:"avgBetLosses"`
	NTradesWins                  int                    `json:"nTradeWins"`
	NFixturesWins                int                    `json:"nFixturesWins"`
	NFixturesLosses              int                    `json:"nFixturesLosses"`
	NFixturesReturn              int                    `json:"nFixturesReturn"`
	NTradesLosses                int                    `json:"nTradeLosses"`
	NTradesReturn                int                    `json:"nTradesReturned"`
	AvgTradesWins                float64                `json:"avgTradesWins"`
	AvgTradesLosses              float64                `json:"avgTradesLosses"`
	AvgFixturesWins              float64                `json:"avgFixturesWins"`
	AvgFixturesLosses            float64                `json:"avgFixturesLosses"`
	PnlPerSize                   float64                `json:"pnlPerSize"`
	BetWLRatio                   float64                `json:"betWLRatio"`
	TradeWLRatio                 float64                `json:"tradeWLRatio"`
	FixtureWLRatio               float64                `json:"fixtureWLRatio"`
	CrBet                        float64                `json:"crBet"`
	CrTrade                      float64                `json:"crTrade"`
	CrFixture                    float64                `json:"crFixture"`
	CrDay                        float64                `json:"crDay"`
	AvgDayWins                   float64                `json:"avgDayWins"`
	AvgDayLosses                 float64                `json:"avgDayLosses"`
	NDayWins                     int                    `json:"nDayWins"`
	NDayLosses                   int                    `json:"nDayLosses"`
	DailyWLRatio                 float64                `json:"dailyWLRatio"`
	PnlDD                        float64                `json:"pnlDD"`
	RetDD                        float64                `json:"retDD"`
	AnnualisedReturn             float64                `json:"annualisedReturn"`
	Volatility                   float64                `json:"volatility"`
	SharpeRatio                  float64                `json:"sharpeRatio"`
	CalmarRatio                  float64                `json:"calmarRatio"`
	RunUp                        float64                `json:"runUpPct"`
	DrawDown                     float64                `json:"drawDownPct"`
	AvgRetOnCapital              float64                `json:"avgRetOnCapital"`
	RetOnCapital                 float64                `json:"retOnCapital"`
	AvgDayWinLogReturn           float64                `json:"avgDayWinLogReturn"`
	AvgDayLossLogReturn          float64                `json:"avgDayLossLogReturn"`
	AvgBetLogReturn              float64                `json:"avgBetLogReturn"`
	AvgBetWinLogReturn           float64                `json:"avgBetWinLogReturn"`
	AvgBetLossLogReturn          float64                `json:"avgBetLossLogReturn"`
	WinToTotal                   float64                `json:"winToTotal"`
	AvgDayLogReturn              float64                `json:"avgDayLogReturn"`
	LastCapital                  float64                `json:"lastCapital"`
	MarketMetrics                map[string]MetricsData `json:"marketMetrics,omitempty"`
	RetOnUnitStake               float64                `json:"retOnUnitStake"`
	RetOnUnitStakePerTradeID     float64                `json:"retOnUnitStakePerTradeID"`
	RetOnUnitStakePerEventMarket float64                `json:"retOnUnitStakePerEventMarket"`
}

type MetricsPeriod string

var (
	MTD    MetricsPeriod = "mtd"
	YTD    MetricsPeriod = "ytd"
	WEEKLY MetricsPeriod = "weekly"
	DAILY  MetricsPeriod = "daily"
	CUSTOM MetricsPeriod = "custom"
)

type CompetitionTradingDays struct {
	TradingDays TradingDays `json:"tradingDays,omitempty"`
	Total       int         `json:"total,omitempty"`
}

type DailyData struct {
	NetPnl      float64 `json:"netPnl"`
	GrossPnl    float64 `json:"grossPnl"`
	Date        string  `json:"date"`
	Capital     float64 `json:"capital"`
	Commission  float64 `json:"commission"`
	NBets       int     `json:"nBets"`
	Return      float64 `json:"return"`
	LogReturn   float64 `json:"logReturn"`
	SizePlaced  float64 `json:"sizePlaced"`
	SizeMatched float64 `json:"sizeMatched"`
}

type SportStats struct {
	TotalSize     float64 `json:"sizeTotal"`
	SizeMatched   float64 `json:"sizeMatched"`
	SizeCancelled float64 `json:"sizeCancelled"`
	SizeRejected  float64 `json:"sizeRejected"`
	SizeVoided    float64 `json:"sizeVoided"`
	SizeLapsed    float64 `json:"sizeLapsed"`
	Commission    float64 `json:"commission"`
	Pnl           float64 `json:"netPnl"`
	Wins          int     `json:"wins"`
	Loses         int     `json:"loses"`
	HalfWins      int     `json:"halfWins"`
	HalfLoses     int     `json:"halfLoses"`
	ReturnOfMoney int     `json:"returnOfStake"`
	Nbets         int     `json:"numberOfBets"`
}

type BookmakerStats struct {
	TotalSize     float64               `json:"sizeTotal"`
	Pnl           float64               `json:"netPnl"`
	SizeMatched   float64               `json:"sizeMatched"`
	SizeCancelled float64               `json:"sizeCancelled"`
	SizeRejected  float64               `json:"sizeRejected"`
	SizeVoided    float64               `json:"sizeVoided"`
	SizeLapsed    float64               `json:"sizeLapsed"`
	SportStats    map[string]SportStats `json:"sports"`
	Commission    float64               `json:"commission"`
	Wins          int                   `json:"wins"`
	Loses         int                   `json:"loses"`
	HalfWins      int                   `json:"halfWins"`
	HalfLoses     int                   `json:"halfLoses"`
	ReturnOfMoney int                   `json:"returnOfStake"`
	Nbets         int                   `json:"numberOfBets"`
}

type ProviderStats struct {
	TotalSize      float64                                 `json:"sizeTotal"`
	Pnl            float64                                 `json:"netPnl"`
	SizeMatched    float64                                 `json:"sizeMatched"`
	SizeCancelled  float64                                 `json:"sizeCancelled"`
	SizeRejected   float64                                 `json:"sizeRejected"`
	SizeVoided     float64                                 `json:"sizeVoided"`
	SizeLapsed     float64                                 `json:"sizeLapsed"`
	SportStats     map[string]SportStats                   `json:"sports"`
	BookmakerStats map[bookmakers.Bookmaker]BookmakerStats `json:"bookmakers"`
	Commission     float64                                 `json:"commission"`
	Wins           int                                     `json:"wins"`
	Loses          int                                     `json:"loses"`
	HalfWins       int                                     `json:"halfWins"`
	HalfLoses      int                                     `json:"halfLoses"`
	ReturnOfMoney  int                                     `json:"returnOfStake"`
	Nbets          int                                     `json:"numberOfBets"`
}

type SportPnl struct {
	Sport         sport.Sport
	Pnl           float64
	StrategiesPnl map[string]StrategyPnl
}

type StrategyPnl struct {
	Name             string
	Pnl              float64
	StrategyDescrPnl map[string]float64
}
