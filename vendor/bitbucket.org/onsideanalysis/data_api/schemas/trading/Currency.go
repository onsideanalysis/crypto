package trading

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

// Currency type
type Currency string

const (
	AUD Currency = "AUD" // Australian Dollar (A$)
	BGN Currency = "BGN" // Bulgarian Lev (BGN)
	BRL Currency = "BRL" // Brazilian Real (R$)
	CAD Currency = "CAD" // Canadian Dollar (CA$)
	CHF Currency = "CHF" // Swiss Franc (CHF)
	CNY Currency = "CNY" // Chinese Yuan (CN¥)
	CZK Currency = "CZK" // Czech Republic Koruna (CZK)
	DKK Currency = "DKK" // Danish Krone (DKK)
	EUR Currency = "EUR" // Euro (€)
	GBP Currency = "GBP" // British Pound Sterling (£)
	HKD Currency = "HKD" // Hong Kong Dollar (HK$)
	HRK Currency = "HRK" // Croatian Kuna (HRK)
	HUF Currency = "HUF" // Hungarian Forint (HUF)
	IDR Currency = "IDR" // Indonesian Rupiah (IDR)
	ILS Currency = "ILS" // Israeli New Sheqel (₪)
	INR Currency = "INR" // Indian Rupee (Rs.)
	JPY Currency = "JPY" // Japanese Yen (¥)
	KRW Currency = "KRW" // South Korean Won (₩)
	LTL Currency = "LTL" // Lithuanian Litas (LTL)
	MXN Currency = "MXN" // Mexican Peso (MX$)
	MYR Currency = "MYR" // Malaysian Ringgit (MYR)
	NOK Currency = "NOK" // Norwegian Krone (NOK)
	NZD Currency = "NZD" // New Zealand Dollar (NZ$)
	PHP Currency = "PHP" // Philippine Peso (Php)
	PLN Currency = "PLN" // Polish Zloty (PLN)
	RON Currency = "RON" // Romanian Leu (RON)
	RUB Currency = "RUB" // Russian Ruble (RUB)
	SEK Currency = "SEK" // Swedish Krona (SEK)
	SGD Currency = "SGD" // Singapore Dollar (SGD)
	THB Currency = "THB" // Thai Baht (฿)
	TRY Currency = "TRY" // Turkish Lira (TRY)
	USD Currency = "USD" // US Dollar ($)
	ZAR Currency = "ZAR" // South African Rand (ZAR)
	RSD Currency = "RSD" // Serbian Dinar (RSD)

)

func (c Currency) String() string {
	switch c {
	case AUD:
		return "AUD" // Australian Dollar (A$)
	case BGN:
		return "BGN" // Bulgarian Lev (BGN)
	case BRL:
		return "BRL" // Brazilian Real (R$)
	case CAD:
		return "CAD" // Canadian Dollar (CA$)
	case CHF:
		return "CHF" // Swiss Franc (CHF)
	case CNY:
		return "CNY" // Chinese Yuan (CN¥)
	case CZK:
		return "CZK" // Czech Republic Koruna (CZK)
	case DKK:
		return "DKK" // Danish Krone (DKK)
	case EUR:
		return "EUR" // Euro (€)
	case GBP:
		return "GBP" // British Pound Sterling (£)
	case HKD:
		return "HKD" // Hong Kong Dollar (HK$)
	case HRK:
		return "HRK" // Croatian Kuna (HRK)
	case HUF:
		return "HUF" // Hungarian Forint (HUF)
	case IDR:
		return "IDR" // Indonesian Rupiah (IDR)
	case ILS:
		return "ILS" // Israeli New Sheqel (₪)
	case INR:
		return "INR" // Indian Rupee (Rs.)
	case JPY:
		return "JPY" // Japanese Yen (¥)
	case KRW:
		return "KRW" // South Korean Won (₩)
	case LTL:
		return "LTL" // Lithuanian Litas (LTL)
	case MXN:
		return "MXN" // Mexican Peso (MX$)
	case MYR:
		return "MYR" // Malaysian Ringgit (MYR)
	case NOK:
		return "NOK" // Norwegian Krone (NOK)
	case NZD:
		return "NZD" // New Zealand Dollar (NZ$)
	case PHP:
		return "PHP" // Philippine Peso (Php)
	case PLN:
		return "PLN" // Polish Zloty (PLN)
	case RON:
		return "RON" // Romanian Leu (RON)
	case RUB:
		return "RUB" // Russian Ruble (RUB)
	case SEK:
		return "SEK" // Swedish Krona (SEK)
	case SGD:
		return "SGD" // Singapore Dollar (SGD)
	case THB:
		return "THB" // Thai Baht (฿)
	case TRY:
		return "TRY" // Turkish Lira (TRY)
	case USD:
		return "USD" // US Dollar ($)
	case ZAR:
		return "ZAR" // South African Rand (ZAR)
	case RSD:
		return "RSD" // Serbian Dina (RSD)
	}
	return ""
}

// Currencies are valid values for currency
var Currencies = []Currency{
	AUD, BGN, BRL, CAD, CHF, CNY, CZK, DKK, EUR, GBP, HKD,
	HRK, HUF, IDR, ILS, INR, JPY, KRW, LTL, MXN, MYR, NOK,
	NZD, PHP, PLN, RON, RUB, SEK, SGD, THB, TRY, USD, ZAR,
}

const (
	YQL_BASE = "http://query.yahooapis.com/v1/public/yql"
)

//YqlResponse structure
type YqlResponse struct {
	Query struct {
		Count   int    `json:"count"`
		Created string `json:"created"`
		Lang    string `json:"lang"`
		Results struct {
			Rate []Rate `json:"rate"`
		} `json:"results"`
	} `json:"query"`
}

type Rate struct {
	ID   string `json:"id"`
	Name string `json:"Name"`
	Rate string `json:"Rate"`
	Date string `json:"Date"`
	Time string `json:"Time"`
	Ask  string `json:"Ask"`
	Bid  string `json:"Bid"`
}

func GetExchangeRates(currencies []Currency) (map[string]float64, error) {
	if len(currencies) == 1 {
		return nil, errors.New("Input cannot have just one currency")
	}
	var pairs []string
	var currenciesCodes string
	if len(currencies) == 0 {
		currencies = Currencies
	}

	for _, currency := range currencies {
		for _, value := range currencies {
			pairs = append(pairs, `"`+currency.String()+value.String()+`"`)
		}
	}

	currenciesCodes = strings.Join(pairs, ",")

	query := `select * from yahoo.finance.xchange where pair in (` + currenciesCodes + `)`
	u, _ := url.Parse(YQL_BASE)
	q := u.Query()
	q.Set("q", query)
	q.Set("format", "json")
	q.Set("env", "store://datatables.org/alltableswithkeys")
	u.RawQuery = q.Encode()
	client := &http.Client{}
	req, _ := http.NewRequest("GET", u.String(), nil)
	response, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	var resp YqlResponse

	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(contents, &resp)
	if err != nil {
		return nil, err
	}
	currenciesRates := make(map[string]float64)
	for _, rate := range resp.Query.Results.Rate {
		exRate, err := strconv.ParseFloat(rate.Rate, 64)
		if err == nil {
			codes := strings.Split(rate.Name, "/")
			currenciesRates[codes[0]+codes[1]] = exRate
		}
	}

	return currenciesRates, nil
}

func Convert(value float64, from, to Currency) float64 {
	currencyRates, err := GetExchangeRates([]Currency{from, to})
	if err == nil {
		value = value * currencyRates[from.String()+to.String()]
	}
	return value
}

func GetExchangeRate(from, to Currency) float64 {
	currencyRates, err := GetExchangeRates([]Currency{from, to})
	if err == nil {
		return currencyRates[from.String()+to.String()]
	}
	return -1.0
}
