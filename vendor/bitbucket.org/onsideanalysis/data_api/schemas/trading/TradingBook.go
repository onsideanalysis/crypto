package trading

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type TradingBook struct {
	ID                   string        `bson:"_id" json:"_id"`
	TradingUserID        bson.ObjectId `bson:"trading_user_id,omitempty" json:"trading_user_id,omitempty"`
	TradingBookID        int           `bson:"trading_book_id" json:"trading_book_id"`
	Name                 string        `bson:"name" json:"name"`
	Sport                string        `bson:"sport" json:"sport"`
	PercentageAllocation float64       `bson:"percentage_allocation" json:"percentage_allocation"`
	Dt                   time.Time     `bson:"dt,omitempty" json:"dt"`
}
