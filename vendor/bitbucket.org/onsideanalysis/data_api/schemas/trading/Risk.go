package trading

import (
	"bitbucket.org/onsideanalysis/data_api/schemas/football"
	"bitbucket.org/onsideanalysis/data_api/schemas/stickers"
)

type AggregatedPositionRisk struct {
	Odds          float64            `json:"odds"`
	Size          float64            `json:"size"`
	IfWin         float64            `json:"if_win"`
	IfLose        float64            `json:"if_lose"`
	Ev            float64            `json:"ev"`
	Mtm           *float64           `json:"mtm"`
	Team1Delta    float64            `json:"team1_delta"`
	Team2Delta    float64            `json:"team2_delta"`
	Theta         float64            `json:"theta"`
	Payoffs       [][]float64        `json:"payoffs,omitempty"`
	SimplePayoffs map[string]float64 `json:"simple_payoffs,omitempty"`
}

type StickerPositionRisk struct {
	Sticker stickers.Sticker        `json:"sticker"`
	Back    *AggregatedPositionRisk `json:"back,omitempty"`
	Lay     *AggregatedPositionRisk `json:"lay,omitempty"`
}

type UnmatchedPosition struct {
	Strategy string  `json:"strategy"`
	Event    string  `json:"event"`
	Market   string  `json:"market"`
	Details  string  `json:"details"`
	Size     float64 `json:"size"`
	IsBack   bool    `json:"is_back"`
}

type FootballRiskEnvironment struct {
	ScoreProbabilities map[string]football.ScoreProbabilities `json:"football_score_probabilities"`
}

type TennisRiskEnvironment struct {
	FirstSetScore []int `json:"first_set_score"`
}

type RiskEnvironment struct {
	Football FootballRiskEnvironment `json:"football,omitempty"`
	Tennis   TennisRiskEnvironment   `json:"tennis,omitempty"`
}
