package trading

import (
	"time"

	"bitbucket.org/onsideanalysis/data_api/schemas/bookmakers"

	"gopkg.in/mgo.v2/bson"
)

type TradingUser struct {
	ID              bson.ObjectId          `bson:"_id,omitempty" json:"_id,omitempty"`
	TradingUserID   bson.ObjectId          `bson:"id,omitempty" json:"id,omitempty"`
	TradingUserInfo []BookmakerCredentials `bson:"bookmaker_credentials,omitempty" json:"bookmaker_credentials,omitempty"`
	Date            time.Time              `bson:"dt,omitempty" json:"dt,omitempty"`
	Internal        bool                   `bson:"internal" json:"internal"`
	Dev             bool                   `bson:"dev" json:"dev"`
}

type BookmakerCredentials struct {
	BookmakerID               int                  `bson:"bookmaker_provider_id,omitempty" json:"bookmaker_provider_id,omitempty"`
	Bookmaker                 bookmakers.Bookmaker `bson:"bookmaker,omitempty" json:"bookmaker,omitempty"`
	Provider                  bookmakers.Provider  `bson:"provider,omitempty" json:"provider,omitempty"`
	AccountID                 bson.ObjectId        `bson:"account_id,omitempty" json:"account_id,omitempty"`
	UserName                  string               `bson:"username,omitempty" json:"username,omitempty"`
	Password                  string               `json:"password,omitempty"`
	PasswordEncrypted         string               `bson:"password_encrypted,omitempty" json:"password_encrypted,omitempty"`
	ProviderUserName          string               `bson:"provider_username,omitempty" json:"provider_username,omitempty"`
	ProviderPassword          string               `bson:"provider_password,omitempty" json:"provider_password,omitempty"`
	ProviderPasswordEncrypted string               `bson:"provider_password_encrypted,omitempty" json:"provider_password_encrypted,omitempty"`
	ApplicationKey            string               `bson:"application_key,omitempty" json:"application_key,omitempty"`
	Ssl                       SslCertificate       `bson:"ssl_certificate,omitempty" json:"ssl_certificate,omitempty"`
	IsValidated               bool                 `bson:"is_validated" json:"is_validated"`
	Currency                  Currency             `bson:"currency,omitempty" json:"currency,omitempty"`
	LastCheckedDt             time.Time            `bson:"last_checked_dt,omitempty" json:"last_checked_dt,omitempty"`
	Monitoring                bool                 `bson:"monitoring" json:"monitoring"`
	Executing                 bool                 `bson:"executing" json:"executing"`
	IsActive                  bool                 `bson:"is_active" json:"is_active"`
}

type AccountInfo struct {
	Currency        Currency `bson:"currency,omitempty" json:"currency,omitempty"`
	Balance         float64  `bson:"balance,omitempty" json:"balance,omitempty"`
	CurrentExposure float64  `bson:"exposure,omitempty" json:"exposure,omitempty"`
	AvailableAmount float64  `bson:"available,omitempty" json:"available,omitempty"`
	CommissionRate  float64  `bson:"commission_rate,omitempty" json:"commission_rate,omitempty"`
	Credit          float64  `bson:"credit,omitempty" json:"credit,omitempty"`
}

type AccountStatement struct {
	OpenBalance     float64  `bson:"open_balance,omitempty" json:"open_balance,omitempty"`
	CloseBalance    float64  `bson:"close_balance,omitempty" json:"close_balance,omitempty"`
	CurrentExposure float64  `bson:"exposure,omitempty" json:"exposure,omitempty"`
	AvailableAmount float64  `bson:"available,omitempty" json:"available,omitempty"`
	TransactionsPnl float64  `bson:"transactions_pnl,omitempty" json:"transactions_pnl,omitempty"`
	SettledBetsPnl  float64  `bson:"settled_bets_pnl,omitempty" json:"settled_bets_pnl,omitempty"`
	StratagemPnl    float64  `bson:"stratagem_pnl,omitempty" json:"stratagem_pnl,omitempty"`
	Deposits        float64  `bson:"deposits,omitempty" json:"deposits,omitempty"`
	Withdrawals     float64  `bson:"withdrawals,omitempty" json:"withdrawals,omitempty"`
	Discrepancy     float64  `bson:"discrepancy,omitempty" json:"discrepancy,omitempty"`
	ExtraCharges    float64  `bson:"extra_charges,omitempty" json:"extra_charges,omitempty"`
	NumberOfBets    int      `bson:"number_of_bets,omitempty" json:"number_of_bets,omitempty"`
	PlacedStake     float64  `bson:"placed_stake,omitempty" json:"placed_stake,omitempty"`
	MatchedStake    float64  `bson:"matched_stake,omitempty" json:"matched_stake,omitempty"`
	MaxExposure     float64  `bson:"max_exposure,omitempty" json:"max_exposure,omitempty"`
	Currency        Currency `bson:"currency,omitempty" json:"currency,omitempty"`
	FxRate          float64  `bson:"fx_rate,omitempty" json:"fx_rate,omitempty"`
	Credit          float64  `bson:"credit,omitempty" json:"credit,omitempty"`
}

type SslCertificate struct {
	SslKey string `bson:"ssl_key,omitempty" json:"ssl_key,omitempty"`
	SslCrt string `bson:"ssl_crt,omitempty" json:"ssl_crt,omitempty"`
}

type TradingUserQueryOutputData struct {
	TradingUserInfo BookmakerCredentials `bson:"bookmaker_credentials,omitempty" json:"bookmaker_credentials,omitempty"`
}

type EncryptionKey struct {
	TradingUserID string `json:"trading_user_id"`
	EncryptionKey string `json:"encryption_key"`
}
