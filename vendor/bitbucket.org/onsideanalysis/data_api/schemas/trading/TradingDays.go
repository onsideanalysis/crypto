package trading

import "time"

type TradingDays []time.Time

func (slice TradingDays) Len() int {
	return len(slice)
}

func (slice TradingDays) Less(i, j int) bool {
	return slice[i].Before(slice[j])
}

func (slice TradingDays) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}
