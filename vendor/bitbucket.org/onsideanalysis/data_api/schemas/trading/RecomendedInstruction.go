package trading

import (
	"time"

	"bitbucket.org/onsideanalysis/data_api/schemas/bookmakers"
	"bitbucket.org/onsideanalysis/data_api/schemas/stickers"
	"gopkg.in/mgo.v2/bson"
)

type RecommendedInstruction struct {
	RecommendedInstructionID bson.ObjectId        `bson:"id" json:"id"`
	Bookmaker                bookmakers.Bookmaker `bson:"bookmaker" json:"bookmaker"`
	PlacedTime               time.Time            `bson:"placed_time" json:"placed_time"`
	IsBack                   bool                 `bson:"is_back" json:"is_back"`
	Sticker                  stickers.Sticker     `bson:"sticker" json:"sticker"`
	Dt                       time.Time            `bson:"dt" json:"dt"`
	Odds                     float64              `bson:"odds" json:"odds"`
	Stake                    float64              `bson:"stake" json:"stake"`
	Source                   Source               `bson:"source" json:"source"`
	Currency                 Currency             `bson:"currency" json:"currency"`
	IsActive                 bool                 `bson:"is_active" json:"is_active"`
	EmailSent                bool                 `bson:"email_sent" json:"email_sent"`
	StartDate                time.Time            `bson:"start_date" json:"start_date"`
	Event                    string               `bson:"event" json:"event"`
}
