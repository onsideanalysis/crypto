package stickers

import (
	"fmt"
	"time"

	"strings"

	"errors"
	"strconv"

	"bitbucket.org/onsideanalysis/data_api/schemas/bookmakers"
	"bitbucket.org/onsideanalysis/data_api/schemas/sport"
	"bitbucket.org/onsideanalysis/data_api/utilities"
)

type Selection string
type Scope int
type Parameter interface{}
type Market int
type Sticker string
type Outcome int
type MarketStatus int
type SelectionStatus int

const (
	MARKET_UNKNOWN   MarketStatus = -1
	MARKET_INACTIVE  MarketStatus = 0
	MARKET_OPEN      MarketStatus = 1
	MARKET_SUSPENDED MarketStatus = 2
	MARKET_CLOSED    MarketStatus = 3
)

func (m MarketStatus) String() string {
	switch m {
	case MARKET_UNKNOWN:
		return "UNKNOWN"
	case MARKET_INACTIVE:
		return "I"
	case MARKET_OPEN:
		return "O"
	case MARKET_SUSPENDED:
		return "S"
	case MARKET_CLOSED:
		return "C"
	default:
		return "Invalid MarketStatus"
	}
}

const (
	SELECTION_UNKNOWN           SelectionStatus = -1
	SELECTION_ACTIVE            SelectionStatus = 0
	SELECTION_SUSPENDED         SelectionStatus = 1
	SELECTION_WINNER            SelectionStatus = 2
	SELECTION_LOSER             SelectionStatus = 3
	SELECTION_REMOVED           SelectionStatus = 4
	SELECTION_HEARTBEAT_TIMEOUT SelectionStatus = 5
)

func (s SelectionStatus) String() string {
	switch s {
	case SELECTION_UNKNOWN:
		return "UNKNOWN"
	case SELECTION_ACTIVE:
		return "A"
	case SELECTION_SUSPENDED:
		return "S"
	case SELECTION_WINNER:
		return "W"
	case SELECTION_LOSER:
		return "L"
	case SELECTION_REMOVED:
		return "R"
	case SELECTION_HEARTBEAT_TIMEOUT:
		return "H"
	default:
		return "Invalid SelectionStatus"
	}
}

const (
	UNKNOWN_OUTCOME Outcome = -1
	WIN             Outcome = 1
	HALF_WIN        Outcome = 2
	RETURN_OF_STAKE Outcome = 3
	HALF_LOSE       Outcome = 4
	LOSE            Outcome = 5
	CASH_OUT        Outcome = 6
)

var (
	EVENT              Scope = 1
	COMPETITION_SEASON Scope = 2
)

type MarketScope struct {
	Type Scope
	IDs  []string
}

func (s Scope) Abbr() string {
	switch s {
	case EVENT:
		return "E"
	case COMPETITION_SEASON:
		return "C"
	default:
		return "UNKNOWN"
	}
}

func (s Scope) String() string {
	return string(s)
}

func (m Market) String() string {
	return fmt.Sprintf("%d", m)
}

func (s Selection) String() string {
	return string(s)
}

func (o Outcome) String() string {
	switch o {
	case WIN:
		return "win"
	case HALF_WIN:
		return "half-win"
	case RETURN_OF_STAKE:
		return "return of stake"
	case HALF_LOSE:
		return "half-lose"
	case LOSE:
		return "lose"
	case UNKNOWN_OUTCOME:
		return "unknown"
	}
	return ""
}

// String returns a string representation of the Sticker.
// Example: "S-GSM1234-FT1X2-X".
func (s Sticker) String() string {
	return string(s)
}

func (s Sticker) Scope() string {
	tokens := strings.Split(string(s), "-")
	if len(tokens) > 1 && len(tokens[1]) > 0 {
		return tokens[1][:1]
	}
	return ""
}

func (s Sticker) Sport() sport.Sport {
	stckr := string(s)
	if idx := strings.Index(stckr, "-"); idx != -1 {
		if sp, ok := sport.REVERSED_SPORT_ABBR[stckr[:idx]]; ok {
			return sp
		}
	}
	return sport.SPORT_UNKNOWN
}

func (s Sticker) EventId() string {
	tokens := strings.Split(string(s), "-")
	if len(tokens) > 1 && len(tokens[1]) > 0 {
		return tokens[1][1:]
	}
	return ""
}

func (s Sticker) Handicap() (float64, error) {
	sticker := string(s)
	idx := strings.LastIndex(sticker, "-")
	if len(sticker) > idx+1 {
		handicapVal := 1.0
		handicapString := strings.Replace(sticker[idx+1:], "_", ".", 1)
		if strings.HasPrefix(handicapString, "n") {
			handicapVal = -1.0
			handicapString = handicapString[1:]
		}
		val, err := strconv.ParseFloat(handicapString, 64)
		if err == nil {
			return handicapVal * val, nil
		}
		return 0, err
	}
	return 0, errors.New("Failed to get handicap value")
}

func (s Sticker) ProviderBookmaker() (bookmakers.Provider, bookmakers.Bookmaker) {
	stckr := string(s)
	idx := strings.LastIndex(stckr, ".")
	if idx != -1 && len(stckr) > idx+1 {
		suffix := stckr[idx+1:]
		tokens := strings.Split(suffix, "_")
		if len(tokens) == 1 {
			if bookmaker, ok := bookmakers.REVERSED_BOOKMAKER_ABBR[tokens[0]]; ok {
				return bookmakers.PROVIDER_UNKNOWN, bookmaker
			}
		} else if len(tokens) == 2 {
			if provider, ok := bookmakers.REVERSED_PROVIDER_ABBR[tokens[0]]; ok {
				if bookmaker, ok := bookmakers.REVERSED_BOOKMAKER_ABBR[tokens[1]]; ok {
					return provider, bookmaker
				}
			}
		}
	}
	return bookmakers.PROVIDER_UNKNOWN, bookmakers.BOOKMAKER_UNKNOWN
}

func (s Sticker) TrimSuffix() Sticker {
	newSticker := s
	if idx := strings.LastIndex(string(s), "."); idx != -1 {
		newSticker = s[:idx]
	}
	return newSticker
}

type StickerExecution struct {
	Bookmaker   bookmakers.Bookmaker `bson:"bookmaker,omitempty" json:"bookmaker,omitempty"`
	Provider    bookmakers.Provider  `bson:"provider,omitempty" json:"provider,omitempty"`
	MarketID    string               `bson:"market_id,omitempty" json:"market_id,omitempty"`
	SelectionID string               `bson:"selection_id,omitempty" json:"selection_id,omitempty"`
	EventID     string               `bson:"event_id,omitempty" json:"event_id,omitempty"`
	HandicapID  string               `bson:"handicap_id,omitempty" json:"handicap_id,omitempty"`
}

type ExecutionDetails map[bookmakers.Provider]map[string]string

type StickerOdds struct {
	MarketStatus    string           `bson:"market_status" json:"market_status"`
	SelectionStatus string           `bson:"selection_status" json:"selection_status"`
	BetDelay        int              `bson:"delay,omitempty" json:"delay"`
	LPM             float64          `bson:"lpm,omitempty" json:"lpm"`
	Total           float64          `bson:"total,omitempty" json:"total"`
	Dt              int64            `bson:"dt" json:"dt"`
	Odds            OddsSide         `bson:"odds" json:"odds"`
	ExecDetails     ExecutionDetails `bson:"execution_details" json:"execution_details"`
}

func (s StickerOdds) Updated(new StickerOdds) bool {
	if s.MarketStatus != new.MarketStatus {
		return true
	}
	if s.SelectionStatus != new.SelectionStatus {
		return true
	}
	if s.BetDelay != new.BetDelay {
		return true
	}
	if s.LPM != new.LPM {
		return true
	}
	if s.Total != new.Total {
		return true
	}
	if s.Dt != new.Dt {
		return true
	}
	if len(s.Odds.Back) != len(new.Odds.Back) {
		return true
	}
	if len(s.Odds.Lay) != len(new.Odds.Lay) {
		return true
	}

	for _, bodds := range s.Odds.Back {
		for _, nbOdds := range new.Odds.Back {
			if utilities.CompareOdds(bodds.Odds, nbOdds.Odds) != 0 {
				return true
			}
			if utilities.CompareSize(bodds.Volume, nbOdds.Volume) != 0 {
				return true
			}
		}
	}
	return false
}

type AggregatedOrderBook struct {
	Sticker Sticker     `bson:"sticker" json:"sticker"`
	Back    QuoteSeries `bson:"back" json:"back"`
	Lay     QuoteSeries `bson:"lay" json:"lay"`
}

type OddsSide struct {
	Back []BetOdds `bson:"back" json:"back"`
	Lay  []BetOdds `bson:"lay" json:"lay"`
}

type BetOdds struct {
	Odds   float64 `bson:"o" json:"o"`
	Volume float64 `bson:"v" json:"v"`
}

type OrderBookQuote struct {
	Bookmaker       bookmakers.Bookmaker `bson:"bookmaker,omitempty" json:"bookmaker,omitempty"`
	IsBack          bool                 `bson:"is_back" json:"is_back"`
	OddsData        []BetOdds            `bson:"odds" json:"odds"`
	ReverseOddsData []BetOdds            `bson:"reverse_odds" json:"reverse_odds"`
	Dt              int64                `bson:"dt" json:"dt"`
}

type Quote struct {
	Sticker   Sticker              `bson:"sticker" json:"sticker"`
	Bookmaker bookmakers.Bookmaker `bson:"bookmaker,omitempty" json:"bookmaker,omitempty"`
	IsBack    bool                 `bson:"is_back" json:"is_back"`
	OddsData  BetOdds              `bson:"odds" json:"odds"`
	Dt        int64                `bson:"dt" json:"dt"`
}

type OrderBookQuoteSeries []OrderBookQuote

type QuoteSeries []Quote

func (slice QuoteSeries) Len() int {
	return len(slice)
}

func (slice QuoteSeries) Less(i, j int) bool {
	if slice[i].IsBack {
		if slice[i].OddsData.Odds > slice[j].OddsData.Odds {
			return true
		} else if slice[i].OddsData.Odds == slice[j].OddsData.Odds {
			if slice[i].OddsData.Volume > slice[j].OddsData.Volume {
				return true
			} else if slice[i].OddsData.Volume == slice[j].OddsData.Volume {
				return slice[i].Bookmaker < slice[j].Bookmaker
			}
		}
	} else if !slice[i].IsBack {
		if slice[i].OddsData.Odds < slice[j].OddsData.Odds {
			return true
		} else if slice[i].OddsData.Odds == slice[j].OddsData.Odds {
			if slice[i].OddsData.Volume > slice[j].OddsData.Volume {
				return true
			} else if slice[i].OddsData.Volume == slice[j].OddsData.Volume {
				return slice[i].Bookmaker < slice[j].Bookmaker
			}
		}
	}
	return false
}

func (slice QuoteSeries) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

func (slice OrderBookQuoteSeries) Len() int {
	return len(slice)
}

func (slice OrderBookQuoteSeries) Less(i, j int) bool {
	if len(slice[i].OddsData) > 0 && len(slice[j].OddsData) > 0 {
		if slice[i].IsBack {
			if slice[i].OddsData[0].Odds > slice[j].OddsData[0].Odds {
				return true
			} else if slice[i].OddsData[0].Odds == slice[j].OddsData[0].Odds {
				if slice[i].OddsData[0].Volume > slice[j].OddsData[0].Volume {
					return true
				} else if slice[i].OddsData[0].Volume == slice[j].OddsData[0].Volume {
					return slice[i].Bookmaker < slice[j].Bookmaker
				}
			}
		} else if !slice[i].IsBack {
			if slice[i].OddsData[0].Odds < slice[j].OddsData[0].Odds {
				return true
			} else if slice[i].OddsData[0].Odds == slice[j].OddsData[0].Odds {
				if slice[i].OddsData[0].Volume > slice[j].OddsData[0].Volume {
					return true
				} else if slice[i].OddsData[0].Volume == slice[j].OddsData[0].Volume {
					return slice[i].Bookmaker < slice[j].Bookmaker
				}
			}
		}
	} else if len(slice[i].OddsData) == 0 && len(slice[j].OddsData) > 0 {
		return false
	} else if len(slice[i].OddsData) > 0 && len(slice[j].OddsData) == 0 {
		return true
	}
	return false
}

func (slice OrderBookQuoteSeries) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

type TimeSeriesOdds struct {
	Dt          time.Time `bson:"dt" json:"dt"`
	Probability float64   `bson:"p" json:"p"`
}

const (
	NATURAL_HANDICAP = 2.0
)

// Used when calculating closest handicaps to evens
type StickerDistance struct {
	StickerOne Sticker
	StickerTwo Sticker
	Distance   float64
	Bookmaker  bookmakers.Bookmaker
}

type StickerDistances []StickerDistance

func (s StickerDistances) Len() int           { return len(s) }
func (s StickerDistances) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s StickerDistances) Less(i, j int) bool { return s[i].Distance < s[j].Distance }
