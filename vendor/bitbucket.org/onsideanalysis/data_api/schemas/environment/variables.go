package environment

import "errors"

type Environment string

var (
	DEVELOPMENT Environment = "dev"
	STAGING     Environment = "staging"
	PRODUCTION  Environment = "prod"
)

func (e Environment) String() string {
	return string(e)
}

func (e *Environment) Set(value string) (err error) {
	err = nil
	switch value {
	case "prod":
		*e = PRODUCTION
	case "staging":
		*e = STAGING
	case "dev":
		*e = DEVELOPMENT
	default:
		err = errors.New("Environment not supported")
	}
	return err
}
