package football

// SharedArea is reused by fixtures, competitions, teams etc.
type SharedArea struct {
	GsmID int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	Name  `bson:"name" json:"name"`
}
