package football

import (
	"log"
	"math"
)

type FootballState int

var ANALYSIS_COMP_IDS = []int{8, 7, 10, 13, 9, 16, 18, 1, 70, 32, 33, 63, 11, 43, 15, 109, 24, 28, 29, 87, 27, 49, 107, 51}
var CUP_COMP_IDS = []int{10, 18} // Champions League, Europa League

func IsCupCompId(compId int) bool {

	for _, c := range CUP_COMP_IDS {
		if c == compId {
			return true
		}
	}
	return false
}

const (
	INSIGHTS_TEAM_COLLECTION      string = "insights_team_data"
	INSIGHTS_COMP_COLLECTION      string = "insights_competition_data"
	INSIGHTS_AVG_COLLECTION       string = "insights_avg_data"
	INSIGHTS_STDDEV_COLLECTION    string = "insights_stddev_data"
	INSIGHTS_SUM_COLLECTION       string = "insights_sum_data"
	INSIGHTS_MIN_MATCHES_PER_TEAM        = 4
)

const (
	UNKNOWN                FootballState = -1
	PRE_MATCH              FootballState = 0
	FIRST_HALF             FootballState = 1
	SECOND_HALF            FootballState = 2
	EXTRA_TIME_FIRST_HALF  FootballState = 3
	EXTRA_TIME_SECOND_HALF FootballState = 4
	HALF_TIME              FootballState = 5
	PENALTIES              FootballState = 6
	END_NORMAL_TIME        FootballState = 7
	EXTRA_TIME_HALF_TIME   FootballState = 8
	FULL_TIME              FootballState = 9
)

func (s FootballState) Int() int {
	return int(s)
}

type GameSegment string

const (
	_0_15           GameSegment = "0-15"
	_16_30          GameSegment = "16-30"
	_31_45          GameSegment = "31-45"
	_46_60          GameSegment = "46-60"
	_61_75          GameSegment = "61-75"
	_76_90          GameSegment = "76-90"
	TOTAL           GameSegment = "total"
	UNKNOWN_SEGMENT GameSegment = ""
)

var ALL_GAME_SEGMENTS = []GameSegment{_0_15, _16_30, _31_45, _46_60, _61_75, _76_90, TOTAL}

func (gs GameSegment) String() string {
	return string(gs)
}

func GetGameSegment(eventStatus *CleanFeedEventStatus) GameSegment {

	if eventStatus.State == FIRST_HALF {
		if eventStatus.Minute >= 0 && eventStatus.Minute <= 15 {
			return _0_15
		} else if eventStatus.Minute >= 16 && eventStatus.Minute <= 30 {
			return _16_30
		} else if eventStatus.Minute >= 31 {
			return _31_45
		}
	} else if eventStatus.State == SECOND_HALF {
		if eventStatus.Minute >= 46 && eventStatus.Minute <= 60 {
			return _46_60
		} else if eventStatus.Minute >= 61 && eventStatus.Minute <= 75 {
			return _61_75
		} else if eventStatus.Minute >= 76 {
			return _76_90
		}
	}

	return UNKNOWN_SEGMENT
}

type AnalystEntry string

const (
	Penalty          AnalystEntry = "penawarded"
	SuperbChance     AnalystEntry = "superbchance"
	GreatChance      AnalystEntry = "greatchance"
	VeryGoodChance   AnalystEntry = "verygoodchance"
	GoodChance       AnalystEntry = "goodchance"
	FairlyGoodChance AnalystEntry = "fairlygoodchance"
	PoorChance       AnalystEntry = "poorchance"
	Goal             AnalystEntry = "goal"
	Corner           AnalystEntry = "corner"
	_18ydEntry       AnalystEntry = "18ydEntry"
)

func (cs AnalystEntry) String() string {
	return string(cs)
}

var (
	ALL_ANALYST_ENTRIES = []AnalystEntry{Penalty, SuperbChance, GreatChance, VeryGoodChance, GoodChance, FairlyGoodChance, PoorChance, Goal, Corner, _18ydEntry}
	ALL_CHANCE_TYPES    = []AnalystEntry{SuperbChance, GreatChance, VeryGoodChance, GoodChance, FairlyGoodChance, PoorChance}
	SCORING_RATES       = map[AnalystEntry]float64{
		SuperbChance:     0.8249680436,
		GreatChance:      0.4307759303,
		VeryGoodChance:   0.222241703,
		GoodChance:       0.0876630761,
		FairlyGoodChance: 0.0524997208,
		PoorChance:       0.0364530482,
	}
)

type InsightFeature string

const (
	ResultsForm         InsightFeature = "results-form"
	HandicapForm        InsightFeature = "handicap-form"
	GoalsForm           InsightFeature = "goals-form"
	CornersForm         InsightFeature = "corners-form"
	CardsForm           InsightFeature = "cards-form"
	GroupedResultsForm  InsightFeature = "grouped-results-form"
	GroupedGoalsForm    InsightFeature = "grouped-goals-form"
	BothTeamsScoredForm InsightFeature = "both-teams-scored-form"
	FrequentScores      InsightFeature = "frequent-scores"
	AverageFirstGoal    InsightFeature = "average-first-goal"
	AverageGoals        InsightFeature = "average-goals"
	ZeroGoals           InsightFeature = "zero-goals"
	FairForm            InsightFeature = "fair-form"
	GameStates          InsightFeature = "game-states"
	GameSegments        InsightFeature = "game-segments"
	NextGoal            InsightFeature = "next-goal"

	HomeAway InsightFeature = "home-away"
	Overall  InsightFeature = "overall"

	FT1X2      InsightFeature = "ft1x2"
	HT1X2      InsightFeature = "ht1x2"
	HTFT1X2    InsightFeature = "htft1x2"
	FTAHG      InsightFeature = "ftahg"
	FTOUG      InsightFeature = "ftoug"
	FTOUC      InsightFeature = "ftouc"
	FTOUCD     InsightFeature = "ftoucd"
	FTCS       InsightFeature = "ftcs"
	BTTS       InsightFeature = "btts"
	NTTS       InsightFeature = "ntts"
	FTDC       InsightFeature = "ftdc"
	HT1X2FT1X2 InsightFeature = "ht1x2ft1x2"
	NoMarket   InsightFeature = "no-market"

	CompStat InsightFeature = "comp-stat"
	TeamStat InsightFeature = "team-stat"
)

const NA = -1.0

func GetImportanceScore(features []InsightFeature, weights map[InsightFeature]float64, rawScore float64) float64 {

	var sum float64
	var count float64
	for _, feature := range features {
		if weight, ok := weights[feature]; ok {

			count++
			if weight == 0 {
				sum = 0
				break
			} else {
				sum += weight
			}

		} else {
			log.Println("Feature does not have a weight", feature)
			return -1.0
		}
	}

	averageWeight := sum / count

	if math.IsNaN(rawScore) || math.IsInf(rawScore, 0) {
		return -1.0
	}

	return averageWeight * rawScore
}

func GetZScore(value, mean, stdDev float64) float64 {
	return (value - mean) / stdDev
}

type UpdateType string

const (
	Undefined   UpdateType = ""
	KickOff     UpdateType = "kick-off"
	HalfTime    UpdateType = "half-time"
	FullTime    UpdateType = "full-time"
	HomeGoal    UpdateType = "home-goal"
	AwayGoal    UpdateType = "away-goal"
	HomeRedCard UpdateType = "home-red-card"
	AwayRedCard UpdateType = "away-red-card"
	Odds        UpdateType = "odds"
)
