package football

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

const (
	CHECK_TYPE_PREDICTIONS string = "predictions"
	CHECK_TYPE_REASONS     string = "reasons"
)

type PredictionPool struct {
	ID                   bson.ObjectId `bson:"_id" json:"_id"`
	CompetitionID        int           `bson:"competition_id" json:"competition_id"`
	CompetitionShortName string        `bson:"competition_short_name" json:"competition_short_name"`
	DeadlineSchedule     struct {
		Hour       int `bson:"hour" json:"hour"`
		Minute     int `bson:"minute" json:"minute"`
		ReasonHour int `bson:"reason_hour" json:"reason_hour"`
	} `bson:"deadline_schedule" json:"deadline_schedule"`
	Analysts []PredictionPoolAnalyst `bson:"analysts" json:"analysts"`
}

type PredictionPoolAnalyst struct {
	ID        string    `bson:"_id" json:"_id"`
	Name      string    `bson:"name" json:"name"`
	Email     string    `bson:"email" json:"email"`
	StartDate time.Time `bson:"startDate" json:"startDate"`
	EndDate   time.Time `bson:"endDate,omitempty" json:"endDate,omitempty"`
}
