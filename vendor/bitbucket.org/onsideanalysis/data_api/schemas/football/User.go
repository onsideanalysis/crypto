package football

//import (
//	"time"
//
//	"gopkg.in/mgo.v2/bson"
//)
//
//// Spcific access allows specific competitions for some action (access models, previews, reviews etc.)
//type SpecificAccess struct {
//	Competitions []int `bson:"competitions,omitempty" json:"competitions,omitempty"`
//}
//
//// Access struct represents the permission access of an user
//type Access struct {
//	Previews SpecificAccess `bson:"previews,omitempty" json:"previews,omitempty"`
//	Reviews  SpecificAccess `bson:"reviews,omitempty" json:"reviews,omitempty"`
//	Data     SpecificAccess `bson:"data,omitempty" json:"data,omitempty"`
//	Models   SpecificAccess `bson:"models,omitempty" json:"models,omitempty"`
//}
//
//// Permissions represents an user's permissions
//type Permissions struct {
//	Access                  Access `bson:"access,omitempty" json:"access,omitempty"`
//	CompetitionDisplayOrder []int  `bson:"competition_display_order" json:"competition_display_order,omitempty"`
//	HasAPIAccess            bool   `bson:"has_api_access" json:"has_api_access"`
//	IsCustomer              bool   `bson:"is_customer" json:"is_customer"`
//	IsQuant                 bool   `bson:"is_quant" json:"is_quant"`
//	IsViewer                bool   `bson:"is_viewer" json:"is_viewer"`
//}
//
//// User struct represents a customer document.
//type User struct {
//	ID                           bson.ObjectId `bson:"_id,omitempty" json:"_id"`
//	Version                      int           `bson:"__v,omitempty" json:"__v"`
//	Active                       bool          `bson:"active" json:"active"`
//	APIToken                     string        `bson:"api_token,omitempty" json:"api_token,omitempty"`
//	Email                        string        `bson:"email" json:"email"`
//	FirstName                    string        `bson:"first_name,omitempty" json:"first_name,omitempty"`
//	LastLoginTime                time.Time     `bson:"last_login_time" json:"last_login_time"`
//	LastName                     string        `bson:"last_name,omitempty" json:"last_name,omitempty"`
//	Password                     string        `bson:"password" json:"password"`
//	PasswordResetToken           string        `bson:"passwordResetToken,omitempty" json:"passwordResetToken,omitempty"`
//	PasswordResetTokenValidUntil time.Time     `bson:"passwordResetTokenValidUntil,omitempty" json:"passwordResetTokenValidUntil,omitempty"`
//	Permissions                  Permissions   `bson:"permissions" json:"permissions,omitempty"`
//	Phone                        string        `bson:"phone,omitempty" json:"phone,omitempty"`
//	Skype                        string        `bson:"skype,omitempty" json:"skype,omitempty"`
//}
