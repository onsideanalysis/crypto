package football

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type View struct {
	Username    string                   `bson:"username,omitempty" json:"username,omitempty"`
	UserID      bson.ObjectId            `bson:"user_id,omitempty" json:"user_id,omitempty"`
	LastUpdated time.Time                `bson:"last_updated" json:"last_updated"`
	Graphs      []map[string]interface{} `bson:"graphs,omitempty" json:"graphs,omitempty"`
}

type Workspace struct {
	Username    string                   `bson:"username,omitempty" json:"username,omitempty"`
	UserID      bson.ObjectId            `bson:"user_id,omitempty" json:"user_id,omitempty"`
	LastUpdated time.Time                `bson:"last_updated" json:"last_updated"`
	Graphs      []map[string]interface{} `bson:"graphs,omitempty" json:"graphs,omitempty"`
	Name        string                   `bson:"name,omitempty" json:"name,omitempty"`
	IsPublic    bool                     `bson:"is_public" json:"is_public"`
	ID          bson.ObjectId            `bson:"_id,omitempty" json:"_id,omitempty"`
}

type Graph struct {
	GraphName string        `bson:"graphName" json:"graphName"`
	GraphType string        `bson:"graphType" json:"graphType"`
	Colors    []string      `bson:"colors" json:"colors"`
	NextColor int           `bson:"nextColor" json:"nextColor"`
	Series    []GraphSeries `bson:"series" json:"series"`
}

type GraphSeries struct {
	Request FixtureEventQueryInputData `bson:"request" json:"request"`
	Color   string                     `bson:"color" json:"color"`
	Name    string                     `bson:"name" json:"name"`
	//	TeamNames map[int]string           `bson:"teamNames" json:"teamNames"`
}
