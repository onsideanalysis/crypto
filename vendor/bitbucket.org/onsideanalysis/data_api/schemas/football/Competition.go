package football

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Competition struct represents a document stored in the DB
type Competition struct {
	ID       bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Analysis *struct {
		Preview bool `bson:"preview" json:"preview"`
		Review  bool `bson:"review" json:"review"`
	} `bson:"analysis,omitempty" json:"analysis,omitempty"`
	Area *struct {
		GsmID int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
		Name  struct {
			GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
		} `bson:"name,omitempty" json:"name,omitempty"`
	} `bson:"area,omitempty" json:"area,omitempty"`
	Format *struct {
		IncludesGroupRound bool   `bson:"includes_group_round" json:"includes_group_round"`
		IsFriendly         bool   `bson:"is_friendly" json:"is_friendly"`
		IsMultiCountry     bool   `bson:"is_multi_country" json:"is_multi_country"`
		Level              int    `bson:"level,omitempty" json:"level,omitempty"`
		Name               string `bson:"name,omitempty" json:"name,omitempty"`
		Participant        *struct {
			AgeLevel string `bson:"age_level,omitempty" json:"age_level,omitempty"`
			Type     string `bson:"type,omitempty" json:"type,omitempty"`
		} `bson:"participant,omitempty" json:"participant,omitempty"`
	} `bson:"format,omitempty" json:"format,omitempty"`
	GsmID        int       `bson:"gsm_id" json:"gsm_id"`
	GsmParseTime time.Time `bson:"gsm_parse_time" json:"gsm_parse_time"`
	Name         *struct {
		FdName    string `bson:"fd_name,omitempty" json:"fd_name,omitempty"`
		GsmName   string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
		OptaCode  string `bson:"opta_code,omitempty" json:"opta_code,omitempty"`
		ShortName string `bson:"short_name,omitempty" json:"short_name,omitempty"`
	} `bson:"name,omitempty" json:"name,omitempty"`
	Seasons *[]struct {
		GsmID int `bson:"gsm_id" json:"gsm_id"`
		Name  struct {
			GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
		} `bson:"name,omitempty" json:"name,omitempty"`
		StartDate time.Time `bson:"start_date,omitempty" json:"start_date,omitempty"`
		EndDate   time.Time `bson:"end_date,omitempty" json:"end_date,omitempty"`
	} `bson:"seasons,omitempty" json:"seasons,omitempty"`
	Statplat *struct {
		ViewOrder int `bson:"view_order" json:"view_order"`
	} `bson:"statplat,omitempty" json:"statplat,omitempty"`
	UpdateTime time.Time `bson:"update_time" json:"update_time"`
}

type CompetitionSeason struct {
	CompetitionID int `bson:"competition_id" json:"competition_id"`
	SeasonID      int `bson:"season_id" json:"season_id"`
}

type CompetitionWeeks struct {
	ID   int `bson:"_id" json:"_id"`
	Area struct {
		GsmID int `bson:"gsm_id" json:"gsm_id"`
		Name  struct {
			GsmName string `bson:"gsm_name" json:"gsm_name"`
		} `bson:"name" json:"name"`
	} `bson:"area" json:"area"`
	Competition struct {
		GsmID int `bson:"gsm_id" json:"gsm_id"`
		Name  struct {
			GsmName   string `bson:"gsm_name" json:"gsm_name"`
			ShortName string `bson:"short_name" json:"short_name"`
		} `bson:"name" json:"name"`
	} `bson:"competition" json:"competition"`
	Rounds []struct {
		GsmID int    `bson:"gsm_id" json:"gsm_id"`
		Name  string `bson:"name" json:"name"`
		Weeks []struct {
			Start time.Time `bson:"start" json:"start"`
			Week  int       `bson:"week" json:"week"`
		} `bson:"weeks" json:"weeks"`
	} `bson:"rounds" json:"rounds"`
	Season struct {
		GsmID int `bson:"gsm_id" json:"gsm_id"`
		Name  struct {
			GsmName string `bson:"gsm_name" json:"gsm_name"`
		} `bson:"name" json:"name"`
	} `bson:"season" json:"season"`
}
