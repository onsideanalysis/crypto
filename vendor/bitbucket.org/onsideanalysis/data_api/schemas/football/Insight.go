package football

import (
	"bitbucket.org/onsideanalysis/data_api/schemas/stickers"
	"bitbucket.org/onsideanalysis/data_api/utilities"
	"github.com/fatih/structs"
	"github.com/gonum/stat"

	"math"
	"reflect"
	"strings"
	"time"
)

const S3_INSIGHTS_BUCKET = "stratagem-insights"

// Insight object to send to consumers
type ConsumerInsight struct {
	EventId     int                  `json:"eventId"`
	Insights    []Insight            `json:"insights"`
	Timestamp   time.Time            `json:"timestamp"`
	EventStatus CleanFeedEventStatus `json:"eventStatus"`
	UpdateType  `json:"update_type"`
}

type Insight struct {
	Markets  []stickers.Market `json:"markets"`
	Subject  string            `json:"subject"`
	Sentence string            `json:"sentence"`
}

type InsightSeason struct {
	Season          `json:"season"`
	IsCurrentSeason bool `json:"is_current_season"`
}

type InsightComponents struct {
	Name          string
	Template      string
	Features      []InsightFeature
	Markets       []stickers.Market
	ValueFunc     func(team1 interface{}, team2 interface{}, compAvg interface{}) float64
	CompFunc      func(comp interface{}) float64
	StickerFormat string
}

type InsightTeam struct {
	TeamId                                int                                     `bson:"team_id,omitempty" json:",omitempty"`
	CompetitionId                         int                                     `bson:"competition_id,omitempty" json:",omitempty"`
	SeasonId                              int                                     `bson:"season_id,omitempty" json:",omitempty"`
	IsCurrentSeason                       bool                                    `json:"is_current_season"`
	OverUnderCardsFullTime                map[string]OverUnderCornersFullTimeTeam `bson:"over_under_cards_full_time" json:",omitempty"`
	OverUnderCardsFullTimeInplay          map[string]OverUnderCornersFullTimeTeam `bson:"over_under_cards_full_time_inplay" json:",omitempty"`
	OverUnderCornersFullTime              map[string]OverUnderCornersFullTimeTeam `bson:"over_under_corners_full_time" json:",omitempty"`
	OverUnderCornersFullTimeInplay        map[string]OverUnderCornersFullTimeTeam `bson:"over_under_corners_full_time_inplay" json:",omitempty"`
	CorrectScoreFullTime                  map[string]CorrectScoreFullTimeTeam     `bson:"correct_score_full_time" json:",omitempty"`
	CorrectScoreFullTimeInplay            map[string]CorrectScoreFullTimeTeam     `bson:"correct_score_full_time_inplay" json:",omitempty"`
	AsianHandicapFullTime                 map[string]AsianHandicapFullTimeTeam    `bson:"asian_handicap_full_time" json:",omitempty"`
	AsianHandicapFullTimeInplay           map[string]AsianHandicapFullTimeTeam    `bson:"asian_handicap_full_time_inplay" json:",omitempty"`
	TotalGoalsFullTime                    map[string]TotalGoalsFullTimeTeam       `bson:"total_goals_full_time" json:",omitempty"`
	TotalGoalsFullTimeInplay              map[string]TotalGoalsFullTimeTeam       `bson:"total_goals_full_time_inplay" json:",omitempty"`
	IX2HalfTimeFullTime                   map[string]OutcomeBetweenTeam           `bson:"1X2_half_time_full_time" json:",omitempty"`
	IX2HalfTimeFullTimeInplay             map[string]OutcomeBetweenTeam           `bson:"1X2_half_time_full_time_inplay" json:",omitempty"`
	FirstHalfResultSecondHalfResult       map[string]OutcomeBetweenTeam           `bson:"first_half_result_second_half_result" json:",omitempty"`
	FirstHalfResultSecondHalfResultInplay map[string]OutcomeBetweenTeam           `bson:"first_half_result_second_half_result_inplay" json:",omitempty"`
	NextGoal                              map[string]TimeFirstGoalTeam            `bson:"next_goal" json:",omitempty"`
	NextGoalInplay                        map[string]TimeFirstGoalTeam            `bson:"next_goal_inplay" json:",omitempty"`
	TimeFirstGoal                         map[string]TimeFirstGoalTeam            `bson:"time_first_goal" json:",omitempty"`
	ComebackWin                           ComebackWinTeam                         `bson:"comeback_win" json:",omitempty"`
	AverageGoals                          AverageGoalsTeam                        `bson:"average_goals" json:",omitempty"`
	IX2FullTime                           map[string]OutcomeInFixturesTeam        `bson:"1X2_full_time" json:",omitempty"`
	IX2FullTimeInplay                     map[string]OutcomeInFixturesTeam        `bson:"1X2_full_time_inplay" json:",omitempty"`
	IX2HalfTime                           map[string]OutcomeInFixturesTeam        `bson:"1X2_half_time" json:",omitempty"`
	IX2HalfTimeInplay                     map[string]OutcomeInFixturesTeam        `bson:"1X2_half_time_inplay" json:",omitempty"`
	BothTeamsToScoreFullTime              map[string]BothTeamsToScoreFullTimeTeam `bson:"both_teams_to_score_full_time" json:",omitempty"`
	BothTeamsToScoreFullTimeInplay        map[string]BothTeamsToScoreFullTimeTeam `bson:"both_teams_to_score_full_time_inplay" json:",omitempty"`
	AverageFirstGoal                      AverageFirstGoalTotal                   `bson:"average_first_goal" json:",omitempty"`
	CleanSheetFailedToScore               CleanSheetAndFailToScoreTeam            `bson:"cleansheet_failed_to_score" json:",omitempty"`
	InterestingTrends                     map[string]InterestingTrendsTeam        `bson:"interesting_trends" json:",omitempty"`
	GameStates                            map[string]GameState                    `bson:"game_states" json:",omitempty"`
	GameSegments                          map[GameSegment]GameSegmentInfo         `bson:"game_segments" json:",omitempty"`
}

type InsightData struct {
	Team InsightTeam
}

func GenerateStats(insightSlice []InsightTeam) (averages, stdDevs, totals *InsightTeam) {

	//Assumes all map keys are the same amongst each element in the list
	s := structs.New(insightSlice[0])

	zero := reflect.ValueOf(*getZeroInsight(&insightSlice[0]))
	avgs := reflect.New(zero.Type()).Elem()
	stds := reflect.New(zero.Type()).Elem()
	tots := reflect.New(zero.Type()).Elem()

	for i, f := range s.Fields() {
		//Fields either need to be a map or a struct
		if f.Kind() == reflect.Map {

			a := reflect.MakeMap(zero.Field(i).Type())
			s := reflect.MakeMap(zero.Field(i).Type())
			t := reflect.MakeMap(zero.Field(i).Type())

			keys := reflect.ValueOf(f.Value()).MapKeys()
			for _, key := range keys {
				slice := getFieldSlice(insightSlice, f.Name(), key)
				avg := reflect.New(zero.Field(i).MapIndex(key).Type()).Elem()
				std := reflect.New(zero.Field(i).MapIndex(key).Type()).Elem()
				sum := reflect.New(zero.Field(i).MapIndex(key).Type()).Elem()

				avg, std, sum = calculateMetrics(slice, avg, std, sum)

				a.SetMapIndex(key, avg)
				s.SetMapIndex(key, std)
				t.SetMapIndex(key, sum)
			}
			avgs.FieldByName(f.Name()).Set(a)
			stds.FieldByName(f.Name()).Set(s)
			tots.FieldByName(f.Name()).Set(t)

		} else if f.Kind() == reflect.Struct {

			slice := getFieldSlice(insightSlice, f.Name(), reflect.Value{})
			avg := reflect.New(zero.Field(i).Type()).Elem()
			std := reflect.New(zero.Field(i).Type()).Elem()
			sum := reflect.New(zero.Field(i).Type()).Elem()

			avg, std, sum = calculateMetrics(slice, avg, std, sum)

			avgs.FieldByName(f.Name()).Set(avg)
			stds.FieldByName(f.Name()).Set(std)
			tots.FieldByName(f.Name()).Set(sum)
		}
	}

	aRes := avgs.Interface().(InsightTeam)
	sRes := stds.Interface().(InsightTeam)
	tRes := tots.Interface().(InsightTeam)
	return &aRes, &sRes, &tRes
}

func calculateMetrics(fieldSlice []reflect.Value, avg, std, sum reflect.Value) (reflect.Value, reflect.Value, reflect.Value) {

	for i, f := range getFieldNames(fieldSlice[0]) {

		if avg.Field(i).Kind() == reflect.Float64 {

			countName := ""
			if !strings.HasPrefix(f, "NumMatches") {
				if strings.HasSuffix(f, "Home") {
					countName = "NumMatchesHome"
				} else if strings.HasSuffix(f, "Away") {
					countName = "NumMatchesAway"
				} else {
					countName = "NumMatches"
				}
			}

			values := getFieldsValues(fieldSlice, f)
			total := utilities.Sum(values...)
			var mean, stdDev float64
			if countName != "" {
				weights := getFieldsValues(fieldSlice, countName)
				mean, stdDev = stat.MeanStdDev(values, weights)
			} else {
				mean, stdDev = stat.MeanStdDev(values, nil)
			}

			if math.IsNaN(mean) {
				mean = -1.0
			}
			if math.IsNaN(stdDev) {
				stdDev = -1.0
			}
			avg.Field(i).SetFloat(mean)
			std.Field(i).SetFloat(stdDev)
			sum.Field(i).SetFloat(total)
		}
	}
	return avg, std, sum
}

func getFieldSlice(insightSlice []InsightTeam, primary string, secondary reflect.Value) (values []reflect.Value) {

	for _, insight := range insightSlice {

		s := structs.New(insight)
		f := s.Field(primary)

		if f.Kind() == reflect.Map {
			values = append(values, reflect.ValueOf(f.Value()).MapIndex(secondary))
		} else {
			values = append(values, reflect.ValueOf(f.Value()))
		}
	}
	return
}

func getFieldNames(val reflect.Value) (names []string) {
	for i := 0; i < val.NumField(); i++ {
		names = append(names, val.Type().Field(i).Name)
	}
	return
}

func getFieldsValues(input []reflect.Value, field string) (output []float64) {

	for _, val := range input {
		if val.IsValid() {
			fieldVal := val.FieldByName(field)
			if fieldVal.IsValid() {
				floatVal := val.FieldByName(field).Float()
				output = append(output, floatVal)
			}
		}
	}
	return output
}

//TODO use reflection here instead of manually going through all the maps
func getZeroInsight(example *InsightTeam) (zero *InsightTeam) {

	zero = &InsightTeam{}

	zero.OverUnderCardsFullTime = make(map[string]OverUnderCornersFullTimeTeam)
	for key, _ := range example.OverUnderCardsFullTime {
		zero.OverUnderCardsFullTime[key] = OverUnderCornersFullTimeTeam{}
	}
	zero.OverUnderCardsFullTimeInplay = make(map[string]OverUnderCornersFullTimeTeam)
	for key, _ := range example.OverUnderCardsFullTimeInplay {
		zero.OverUnderCardsFullTimeInplay[key] = OverUnderCornersFullTimeTeam{}
	}

	zero.OverUnderCornersFullTime = make(map[string]OverUnderCornersFullTimeTeam)
	for key, _ := range example.OverUnderCornersFullTime {
		zero.OverUnderCornersFullTime[key] = OverUnderCornersFullTimeTeam{}
	}
	zero.OverUnderCornersFullTimeInplay = make(map[string]OverUnderCornersFullTimeTeam)
	for key, _ := range example.OverUnderCornersFullTimeInplay {
		zero.OverUnderCornersFullTimeInplay[key] = OverUnderCornersFullTimeTeam{}
	}

	zero.CorrectScoreFullTime = make(map[string]CorrectScoreFullTimeTeam)
	for key, _ := range example.CorrectScoreFullTime {
		zero.CorrectScoreFullTime[key] = CorrectScoreFullTimeTeam{}
	}
	zero.CorrectScoreFullTimeInplay = make(map[string]CorrectScoreFullTimeTeam)
	for key, _ := range example.CorrectScoreFullTimeInplay {
		zero.CorrectScoreFullTimeInplay[key] = CorrectScoreFullTimeTeam{}
	}

	zero.AsianHandicapFullTime = make(map[string]AsianHandicapFullTimeTeam)
	for key, _ := range example.AsianHandicapFullTime {
		zero.AsianHandicapFullTime[key] = AsianHandicapFullTimeTeam{}
	}
	zero.AsianHandicapFullTimeInplay = make(map[string]AsianHandicapFullTimeTeam)
	for key, _ := range example.AsianHandicapFullTimeInplay {
		zero.AsianHandicapFullTimeInplay[key] = AsianHandicapFullTimeTeam{}
	}

	zero.TotalGoalsFullTime = make(map[string]TotalGoalsFullTimeTeam)
	for key, _ := range example.TotalGoalsFullTime {
		zero.TotalGoalsFullTime[key] = TotalGoalsFullTimeTeam{}
	}
	zero.TotalGoalsFullTimeInplay = make(map[string]TotalGoalsFullTimeTeam)
	for key, _ := range example.TotalGoalsFullTimeInplay {
		zero.TotalGoalsFullTimeInplay[key] = TotalGoalsFullTimeTeam{}
	}

	zero.IX2HalfTimeFullTime = make(map[string]OutcomeBetweenTeam)
	for key, _ := range example.IX2HalfTimeFullTime {
		zero.IX2HalfTimeFullTime[key] = OutcomeBetweenTeam{}
	}
	zero.IX2HalfTimeFullTimeInplay = make(map[string]OutcomeBetweenTeam)
	for key, _ := range example.IX2HalfTimeFullTimeInplay {
		zero.IX2HalfTimeFullTimeInplay[key] = OutcomeBetweenTeam{}
	}

	zero.FirstHalfResultSecondHalfResult = make(map[string]OutcomeBetweenTeam)
	for key, _ := range example.FirstHalfResultSecondHalfResult {
		zero.FirstHalfResultSecondHalfResult[key] = OutcomeBetweenTeam{}
	}
	zero.FirstHalfResultSecondHalfResultInplay = make(map[string]OutcomeBetweenTeam)
	for key, _ := range example.FirstHalfResultSecondHalfResultInplay {
		zero.FirstHalfResultSecondHalfResultInplay[key] = OutcomeBetweenTeam{}
	}

	zero.NextGoal = make(map[string]TimeFirstGoalTeam)
	for key, _ := range example.NextGoal {
		zero.NextGoal[key] = TimeFirstGoalTeam{}
	}
	zero.NextGoalInplay = make(map[string]TimeFirstGoalTeam)
	for key, _ := range example.NextGoalInplay {
		zero.NextGoalInplay[key] = TimeFirstGoalTeam{}
	}

	zero.IX2FullTime = make(map[string]OutcomeInFixturesTeam)
	for key, _ := range example.IX2FullTime {
		zero.IX2FullTime[key] = OutcomeInFixturesTeam{}
	}
	zero.IX2FullTimeInplay = make(map[string]OutcomeInFixturesTeam)
	for key, _ := range example.IX2FullTimeInplay {
		zero.IX2FullTimeInplay[key] = OutcomeInFixturesTeam{}
	}

	zero.IX2HalfTime = make(map[string]OutcomeInFixturesTeam)
	for key, _ := range example.IX2HalfTime {
		zero.IX2HalfTime[key] = OutcomeInFixturesTeam{}
	}
	zero.IX2HalfTimeInplay = make(map[string]OutcomeInFixturesTeam)
	for key, _ := range example.IX2HalfTimeInplay {
		zero.IX2HalfTimeInplay[key] = OutcomeInFixturesTeam{}
	}

	zero.BothTeamsToScoreFullTime = make(map[string]BothTeamsToScoreFullTimeTeam)
	for key, _ := range example.BothTeamsToScoreFullTime {
		zero.BothTeamsToScoreFullTime[key] = BothTeamsToScoreFullTimeTeam{}
	}
	zero.BothTeamsToScoreFullTimeInplay = make(map[string]BothTeamsToScoreFullTimeTeam)
	for key, _ := range example.BothTeamsToScoreFullTimeInplay {
		zero.BothTeamsToScoreFullTimeInplay[key] = BothTeamsToScoreFullTimeTeam{}
	}

	zero.TimeFirstGoal = make(map[string]TimeFirstGoalTeam)
	for key, _ := range example.TimeFirstGoal {
		zero.TimeFirstGoal[key] = TimeFirstGoalTeam{}
	}

	zero.GameStates = make(map[string]GameState)
	for key, _ := range example.GameStates {
		zero.GameStates[key] = GameState{State: key}
	}

	zero.GameSegments = make(map[GameSegment]GameSegmentInfo)
	for key, _ := range example.GameSegments {
		zero.GameSegments[key] = GameSegmentInfo{Segment: key}
	}

	zero.InterestingTrends = make(map[string]InterestingTrendsTeam)
	for key, _ := range example.InterestingTrends {
		zero.InterestingTrends[key] = InterestingTrendsTeam{}
	}

	return zero
}

type InitialGameState struct {
	DbKey  string `json:"db_key"`
	ValueH int    `json:"value_h"`
	ValueA int    `json:"value_a"`
	CompOp string `json:"comp_op"`
}

type FixtureScope struct {
	Distinct bool `json:"distinct,omitempty"`
	Num      int  `json:"num,omitempty"`

	Key   string
	Descr string
}

type OverUnderCornersFullTimeTeam struct {
	Team             int     `bson:"team,omitempty" json:"team,omitempty"`
	Season           int     `bson:"season" json:"season"`
	Competition      int     `bson:"competition" json:"competition"`
	OverHome         float64 `bson:"over_home,omitempty" json:"over_home,omitempty"`
	OverAway         float64 `bson:"over_away,omitempty" json:"over_away,omitempty"`
	UnderHome        float64 `bson:"under_home,omitempty" json:"under_home,omitempty"`
	UnderAway        float64 `bson:"under_away,omitempty" json:"under_away,omitempty"`
	PushHome         float64 `bson:"push_home,omitempty" json:"push_home,omitempty"`
	PushAway         float64 `bson:"push_away,omitempty" json:"push_away,omitempty"`
	Line             float64 `bson:"line" json:"line"`
	NumMatches       float64 `bson:"num_matches,omitempty" json:"num_matches,omitempty"`
	NumMatchesHome   float64 `bson:"num_matches_home,omitempty" json:"num_matches_home,omitempty"`
	NumMatchesAway   float64 `bson:"num_matches_away,omitempty" json:"num_matches_away,omitempty"`
	NumMatchesGS     float64 `bson:"num_matches_gs,omitempty" json:"num_matches_gs,omitempty"`
	NumMatchesGSHome float64 `bson:"num_matches_gs_home,omitempty" json:"num_matches_gs_home,omitempty"`
	NumMatchesGSAway float64 `bson:"num_matches_gs_away,omitempty" json:"num_matches_gs_away,omitempty"`
}

type CorrectScoreFullTimeTeam struct {
	Team                      int      `bson:"team,omitempty" json:"team,omitempty"`
	Season                    int      `bson:"season" json:"season"`
	Competition               int      `bson:"competition" json:"competition"`
	CommonScoreHome           []string `bson:"common_score_home,omitempty" json:"common_score_home"`
	CommonScoreAway           []string `bson:"common_score_away,omitempty" json:"common_score_away"`
	NumMatchesCommonScoreHome []int    `bson:"num_matches_common_score_home,omitempty" json:"num_matches_common_score_home,omitempty"`
	NumMatchesCommonScoreAway []int    `bson:"num_matches_common_score_away,omitempty" json:"num_matches_common_score_away,omitempty"`
	NumMatches                float64  `bson:"num_matches,omitempty" json:"num_matches,omitempty"`
	NumMatchesHome            float64  `bson:"num_matches_home,omitempty" json:"num_matches_home,omitempty"`
	NumMatchesAway            float64  `bson:"num_matches_away,omitempty" json:"num_matches_away,omitempty"`
	NumMatchesGS              float64  `bson:"num_matches_gs,omitempty" json:"num_matches_gs,omitempty"`
	NumMatchesGSHome          float64  `bson:"num_matches_gs_home,omitempty" json:"num_matches_gs_home,omitempty"`
	NumMatchesGSAway          float64  `bson:"num_matches_gs_away,omitempty" json:"num_matches_gs_away,omitempty"`
}

type BothTeamsToScoreFullTimeTeam struct {
	Team                 int     `bson:"team,omitempty" json:"team,omitempty"`
	Season               int     `bson:"season" json:"season"`
	Competition          int     `bson:"competition" json:"competition"`
	BothTeamsToScoreHome float64 `bson:"both_teams_to_score_home,omitempty" json:"both_teams_to_score_home,omitempty"`
	BothTeamsToScoreAway float64 `bson:"both_teams_to_score_away,omitempty" json:"both_teams_to_score_away,omitempty"`
	NumMatches           float64 `bson:"num_matches,omitempty" json:"num_matches,omitempty"`
	NumMatchesHome       float64 `bson:"num_matches_home,omitempty" json:"num_matches_home,omitempty"`
	NumMatchesAway       float64 `bson:"num_matches_away,omitempty" json:"num_matches_away,omitempty"`
	NumMatchesGS         float64 `bson:"num_matches_gs,omitempty" json:"num_matches_gs,omitempty"`
	NumMatchesGSHome     float64 `bson:"num_matches_gs_home,omitempty" json:"num_matches_gs_home,omitempty"`
	NumMatchesGSAway     float64 `bson:"num_matches_gs_away,omitempty" json:"num_matches_gs_away,omitempty"`
}

type AsianHandicapFullTimeTeam struct {
	Team             int     `bson:"team,omitempty" json:"team,omitempty"`
	Season           int     `bson:"season" json:"season"`
	Competition      int     `bson:"competition" json:"competition"`
	WinHome          float64 `bson:"win_home,omitempty" json:"win_home,omitempty"`
	WinAway          float64 `bson:"win_away,omitempty" json:"win_away,omitempty"`
	LossHome         float64 `bson:"loss_home,omitempty" json:"loss_home,omitempty"`
	LossAway         float64 `bson:"loss_away,omitempty" json:"loss_away,omitempty"`
	HalfWinHome      float64 `bson:"half_win_home,omitempty" json:"half_win_home,omitempty"`
	HalfWinAway      float64 `bson:"half_win_away,omitempty" json:"half_win_away,omitempty"`
	HalfLossHome     float64 `bson:"half_loss_home,omitempty" json:"half_loss_home,omitempty"`
	HalfLossAway     float64 `bson:"half_loss_away,omitempty" json:"half_loss_away,omitempty"`
	PushHome         float64 `bson:"push_home,omitempty" json:"push_home,omitempty"`
	PushAway         float64 `bson:"push_away,omitempty" json:"push_away,omitempty"`
	Line             float64 `bson:"line" json:"line"`
	NumMatches       float64 `bson:"num_matches,omitempty" json:"num_matches,omitempty"`
	NumMatchesHome   float64 `bson:"num_matches_home,omitempty" json:"num_matches_home,omitempty"`
	NumMatchesAway   float64 `bson:"num_matches_away,omitempty" json:"num_matches_away,omitempty"`
	NumMatchesGS     float64 `bson:"num_matches_gs,omitempty" json:"num_matches_gs,omitempty"`
	NumMatchesGSHome float64 `bson:"num_matches_gs_home,omitempty" json:"num_matches_gs_home,omitempty"`
	NumMatchesGSAway float64 `bson:"num_matches_gs_away,omitempty" json:"num_matches_gs_away,omitempty"`
}

type TotalGoalsFullTimeTeam struct {
	Team             int     `bson:"team,omitempty" json:"team,omitempty"`
	Season           int     `bson:"season" json:"season"`
	Competition      int     `bson:"competition" json:"competition"`
	OverHome         float64 `bson:"over_home,omitempty" json:"over_home,omitempty"`
	OverAway         float64 `bson:"over_away,omitempty" json:"over_away,omitempty"`
	UnderHome        float64 `bson:"under_home,omitempty" json:"under_home,omitempty"`
	UnderAway        float64 `bson:"under_away,omitempty" json:"under_away,omitempty"`
	HalfOverHome     float64 `bson:"half_over_home,omitempty" json:"half_over_home,omitempty"`
	HalfOverAway     float64 `bson:"half_over_away,omitempty" json:"half_over_away,omitempty"`
	HalfUnderHome    float64 `bson:"half_under_home,omitempty" json:"half_under_home,omitempty"`
	HalfUnderAway    float64 `bson:"half_under_away,omitempty" json:"half_under_away,omitempty"`
	PushHome         float64 `bson:"push_home,omitempty" json:"push_home,omitempty"`
	PushAway         float64 `bson:"push_away,omitempty" json:"push_away,omitempty"`
	Goals            float64 `bson:"goals" json:"goals"`
	NumMatches       float64 `bson:"num_matches,omitempty" json:"num_matches,omitempty"`
	NumMatchesHome   float64 `bson:"num_matches_home,omitempty" json:"num_matches_home,omitempty"`
	NumMatchesAway   float64 `bson:"num_matches_away,omitempty" json:"num_matches_away,omitempty"`
	NumMatchesGS     float64 `bson:"num_matches_gs,omitempty" json:"num_matches_gs,omitempty"`
	NumMatchesGSHome float64 `bson:"num_matches_gs_home,omitempty" json:"num_matches_gs_home,omitempty"`
	NumMatchesGSAway float64 `bson:"num_matches_gs_away,omitempty" json:"num_matches_gs_away,omitempty"`
}

type OutcomeBetweenTeam struct {
	Team                       int     `bson:"team,omitempty" json:"team,omitempty"`
	Season                     int     `bson:"season" json:"season"`
	Competition                int     `bson:"competition" json:"competition"`
	NumMatchesWinFHWinSHHome   float64 `bson:"num_matches_winfh_winsh_home,omitempty" json:"num_matches_winfh_winsh_home,omitempty"`
	NumMatchesWinFHWinSHAway   float64 `bson:"num_matches_winfh_winsh_away,omitempty" json:"num_matches_winfh_winsh_away,omitempty"`
	NumMatchesWinFHLossSHHome  float64 `bson:"num_matches_winfh_losssh_home,omitempty" json:"num_matches_winfh_losssh_home,omitempty"`
	NumMatchesWinFHLossSHAway  float64 `bson:"num_matches_winfh_losssh_away,omitempty" json:"num_matches_winfh_losssh_away,omitempty"`
	NumMatchesWinFHDrawSHHome  float64 `bson:"num_matches_winfh_drawsh_home,omitempty" json:"num_matches_winfh_drawsh_home,omitempty"`
	NumMatchesWinFHDrawSHAway  float64 `bson:"num_matches_winfh_drawsh_away,omitempty" json:"num_matches_winfh_drawsh_away,omitempty"`
	NumMatchesLossFHWinSHHome  float64 `bson:"num_matches_lossfh_winsh_home,omitempty" json:"num_matches_lossfh_winsh_home,omitempty"`
	NumMatchesLossFHWinSHAway  float64 `bson:"num_matches_lossfh_winsh_away,omitempty" json:"num_matches_lossfh_winsh_away,omitempty"`
	NumMatchesLossFHLossSHHome float64 `bson:"num_matches_lossfh_losssh_home,omitempty" json:"num_matches_lossfh_losssh_home,omitempty"`
	NumMatchesLossFHLossSHAway float64 `bson:"num_matches_lossfh_losssh_away,omitempty" json:"num_matches_lossfh_losssh_away,omitempty"`
	NumMatchesLossFHDrawSHHome float64 `bson:"num_matches_lossfh_drawsh_home,omitempty" json:"num_matches_lossfh_drawsh_home,omitempty"`
	NumMatchesLossFHDrawSHAway float64 `bson:"num_matches_lossfh_drawsh_away,omitempty" json:"num_matches_lossfh_drawsh_away,omitempty"`
	NumMatchesDrawFHWinSHHome  float64 `bson:"num_matches_drawfh_winsh_home,omitempty" json:"num_matches_drawfh_winsh_home,omitempty"`
	NumMatchesDrawFHWinSHAway  float64 `bson:"num_matches_drawfh_winsh_away,omitempty" json:"num_matches_drawfh_winsh_away,omitempty"`
	NumMatchesDrawFHLossSHHome float64 `bson:"num_matches_drawfh_losssh_home,omitempty" json:"num_matches_drawfh_losssh_home,omitempty"`
	NumMatchesDrawFHLossSHAway float64 `bson:"num_matches_drawfh_losssh_away,omitempty" json:"num_matches_drawfh_losssh_away,omitempty"`
	NumMatchesDrawFHDrawSHHome float64 `bson:"num_matches_drawfh_drawsh_home,omitempty" json:"num_matches_drawfh_drawsh_home,omitempty"`
	NumMatchesDrawFHDrawSHAway float64 `bson:"num_matches_drawfh_drawsh_away,omitempty" json:"num_matches_drawfh_drawsh_away,omitempty"`
	NumMatches                 float64 `bson:"num_matches,omitempty" json:"num_matches,omitempty"`
	NumMatchesHome             float64 `bson:"num_matches_home,omitempty" json:"num_matches_home,omitempty"`
	NumMatchesAway             float64 `bson:"num_matches_away,omitempty" json:"num_matches_away,omitempty"`
	NumMatchesGS               float64 `bson:"num_matches_gs,omitempty" json:"num_matches_gs,omitempty"`
	NumMatchesGSHome           float64 `bson:"num_matches_gs_home,omitempty" json:"num_matches_gs_home,omitempty"`
	NumMatchesGSAway           float64 `bson:"num_matches_gs_away,omitempty" json:"num_matches_gs_away,omitempty"`
}

type OutcomeInFixturesTeam struct {
	Team               int     `bson:"team,omitempty" json:"team,omitempty"`
	Season             int     `bson:"season" json:"season"`
	Competition        int     `bson:"competition" json:"competition"`
	NumMatchesWinHome  float64 `bson:"num_matches_win_home,omitempty" json:"num_matches_win_home,omitempty"`
	NumMatchesWinAway  float64 `bson:"num_matches_win_away,omitempty" json:"num_matches_win_away,omitempty"`
	NumMatchesLossHome float64 `bson:"num_matches_loss_home,omitempty" json:"num_matches_loss_home,omitempty"`
	NumMatchesLossAway float64 `bson:"num_matches_loss_away,omitempty" json:"num_matches_loss_away,omitempty"`
	NumMatchesDrawHome float64 `bson:"num_matches_draw_home,omitempty" json:"num_matches_draw_home,omitempty"`
	NumMatchesDrawAway float64 `bson:"num_matches_draw_away,omitempty" json:"num_matches_draw_away,omitempty"`
	NumMatches         float64 `bson:"num_matches,omitempty" json:"num_matches,omitempty"`
	NumMatchesHome     float64 `bson:"num_matches_home,omitempty" json:"num_matches_home,omitempty"`
	NumMatchesAway     float64 `bson:"num_matches_away,omitempty" json:"num_matches_away,omitempty"`
	NumMatchesGS       float64 `bson:"num_matches_gs,omitempty" json:"num_matches_gs,omitempty"`
	NumMatchesGSHome   float64 `bson:"num_matches_gs_home,omitempty" json:"num_matches_gs_home,omitempty"`
	NumMatchesGSAway   float64 `bson:"num_matches_gs_away,omitempty" json:"num_matches_gs_away,omitempty"`
	Before             int     `bson:"before" json:"before"`
}

type ComebackWinTeam struct {
	Team               int     `bson:"team,omitempty" json:"team,omitempty"`
	Season             int     `bson:"season" json:"season"`
	Competition        int     `bson:"competition" json:"competition"`
	ComebackWinHome    float64 `bson:"comeback_win_home,omitempty" json:"comeback_win_home,omitempty"`
	ComebackWinAway    float64 `bson:"comeback_win_away,omitempty" json:"comeback_win_away,omitempty"`
	ComebackLossHome   float64 `bson:"comeback_loss_home,omitempty" json:"comeback_loss_home,omitempty"`
	ComebackLossAway   float64 `bson:"comeback_loss_away,omitempty" json:"comeback_loss_away,omitempty"`
	NumMatchesWin      float64 `bson:"num_matches_win,omitempty" json:"num_matches_win,omitempty"`
	NumMatchesLoss     float64 `bson:"num_matches_loss,omitempty" json:"num_matches_loss,omitempty"`
	NumMatchesWinHome  float64 `bson:"num_matches_win_home,omitempty" json:"num_matches_win_home,omitempty"`
	NumMatchesWinAway  float64 `bson:"num_matches_win_away,omitempty" json:"num_matches_win_away,omitempty"`
	NumMatchesLossHome float64 `bson:"num_matches_loss_home,omitempty" json:"num_matches_loss_home,omitempty"`
	NumMatchesLossAway float64 `bson:"num_matches_loss_away,omitempty" json:"num_matches_loss_away,omitempty"`
}

// Time of First Goal (Individual Teams & League Average)
type TimeFirstGoalTeam struct {
	Team                int     `bson:"team,omitempty" json:"team,omitempty"`
	Season              int     `bson:"season" json:"season"`
	Competition         int     `bson:"competition" json:"competition"`
	BeforeMin           float64 `bson:"before_min" json:"before_min"`
	MatchesScoredHome   float64 `bson:"matches_scored_home,omitempty" json:"matches_scored_home,omitempty"`
	MatchesConcededHome float64 `bson:"matches_conceded_home,omitempty" json:"matches_conceded_home,omitempty"`
	MatchesScoredAway   float64 `bson:"matches_scored_away,omitempty" json:"matches_scored_away,omitempty"`
	MatchesConcededAway float64 `bson:"matches_conceded_away,omitempty" json:"matches_conceded_away,omitempty"`
	NumMatches          float64 `bson:"num_matches,omitempty" json:"num_matches,omitempty"`
	NumMatchesHome      float64 `bson:"num_matches_home,omitempty" json:"num_matches_home,omitempty"`
	NumMatchesAway      float64 `bson:"num_matches_away,omitempty" json:"num_matches_away,omitempty"`
	NumMatchesGS        float64 `bson:"num_matches_gs,omitempty" json:"num_matches_gs,omitempty"`
	NumMatchesGSHome    float64 `bson:"num_matches_gs_home,omitempty" json:"num_matches_gs_home,omitempty"`
	NumMatchesGSAway    float64 `bson:"num_matches_gs_away,omitempty" json:"num_matches_gs_away,omitempty"`
}

// Average Goals
type AverageGoalsTeam struct {
	Team                 int     `bson:"team,omitempty" json:"team,omitempty"`
	Competition          int     `bson:"competition,omitempty" json:"competition,omitempty"`
	GoalsReceivedHome    float64 `bson:"goals_received_home,omitempty" json:"goals_received_home,omitempty"`
	GoalsReceivedAway    float64 `bson:"goals_received_away,omitempty" json:"goals_received_away,omitempty"`
	GoalsReceivedOverall float64 `bson:"goals_received_overall,omitempty" json:"goals_received_overall,omitempty"`
	GoalsScoredHome      float64 `bson:"goals_scored_home,omitempty" json:"goals_scored_home,omitempty"`
	GoalsScoredAway      float64 `bson:"goals_scored_away,omitempty" json:"goals_scored_away,omitempty"`
	GoalsScoredOverall   float64 `bson:"goals_scored_overall,omitempty" json:"goals_scored_overall,omitempty"`
	NumMatches           float64 `bson:"num_matches,omitempty" json:"num_matches,omitempty"`
	NumMatchesHome       float64 `bson:"num_matches_home,omitempty" json:"num_matches_home,omitempty"`
	NumMatchesAway       float64 `bson:"num_matches_away,omitempty" json:"num_matches_away,omitempty"`
	Over2dot5GoalsHome   float64 `bson:"over_2dot5_goals_home,omitempty" json:"over_2dot5_goals_home,omitempty"`
	Over2dot5GoalsAway   float64 `bson:"over_2dot5_goals_away,omitempty" json:"over_2dot5_goals_away,omitempty"`
}

// Average first goal
type AverageFirstGoalTeamAndCompetition []AverageFirstGoalItem
type AverageFirstGoalItem struct {
	GsmId int                     `bson:"_id" json:"gsm_id"`
	State []AverageFirstGoalState `bson:"states" json:"states"`
}

type AverageFirstGoalState struct {
	TotalGoals float64 `bson:"total_goals" json:"total_goals"`
	Team1      int     `bson:"team1" json:"team1"`
	Team2      int     `bson:"team2" json:"team2"`
	Minute     float64 `bson:"minute" json:"minute"`
	Supremacy  float64 `bson:"supremacy" json:"supremacy"`
	Outcome    struct {
		Team1Goals float64 `bson:"team1_goals" json:"team1_goals"`
		Team2Goals float64 `bson:"team2_goals" json:"team2_goals"`
	} `bson:"outcome" json:"outcome"`
	Team1Score float64 `bson:"team1score" json:"team1score"`
	Team2Score float64 `bson:"team2score" json:"team2score"`
}

type AverageFirstGoalTotal struct {
	ForHome        float64 `json:"for_home"`
	ForAway        float64 `json:"for_away"`
	ForOverall     float64 `json:"for_overall"`
	AgainstHome    float64 `json:"against_home"`
	AgainstAway    float64 `json:"against_away"`
	AgainstOverall float64 `json:"against_overall"`
}

// Clean Sheet vs Fail to score
type CleanSheetAndFailToScoreTeam struct {
	CleanHome      float64 `bson:"clean_home,omitempty" json:"clean_home,omitempty"`
	CleanAway      float64 `bson:"clean_away,omitempty" json:"clean_away,omitempty"`
	CleanOverall   float64 `bson:"clean_overall,omitempty" json:"clean_overall,omitempty"`
	FailHome       float64 `bson:"fail_home,omitempty" json:"fail_home,omitempty"`
	FailAway       float64 `bson:"fail_away,omitempty" json:"fail_away,omitempty"`
	FailOverall    float64 `bson:"fail_overall,omitempty" json:"fail_overall,omitempty"`
	NumMatches     float64 `bson:"num_matches,omitempty" json:"num_matches,omitempty"`
	NumMatchesHome float64 `bson:"matches_home,omitempty" json:"matches_home,omitempty"`
	NumMatchesAway float64 `bson:"matches_away,omitempty" json:"matches_away,omitempty"`
}

// Interesting trends
type InterestingTrendsTeam struct {
	Rank              float64 `json:"rank" bson:"rank"`
	TeamsInSeason     float64 `json:"teams_in_season" bson:"teams_in_season"`
	WonPercent        float64 `json:"won_percent" bson:"won_percent"`
	LostPercent       float64 `json:"lost_percent" bson:"lost_percent"`
	Over2dot5Percent  float64 `json:"over_2dot5_percent" bson:"over_2dot5_percent"`
	Under2dot5Percent float64 `json:"under_2dot5_percent" bson:"under_2dot5_percent"`
	BeatenNHPercent   float64 `json:"beaten_nh_percent" bson:"beaten_nh_percent"`
	LostNHPercent     float64 `json:"lost_nh_percent" bson:"lost_nh_percent"`
	NumMatches        float64 `json:"num_matches" bson:"num_matches"`
}

type GameState struct {
	State            string  `json:"state" bson:"state"`
	Chances          float64 `json:"chances" bson:"chances"`
	Goals            float64 `json:"goals" bson:"goals"`
	Time             float64 `json:"time" bson:"time"`
	NextGoalScored   float64 `json:"next_goal_scored" bson:"next_goal_scored"`
	NextGoalConceded float64 `json:"next_goal_conceded" bson:"next_goal_conceded"`
}

type GameSegmentInfo struct {
	Segment    GameSegment `json:"segment" bson:"segment"`
	GoalsFor   float64     `json:"goals_for" bson:"goals_for"`
	GoalsAga   float64     `json:"goals_aga" bson:"goals_aga"`
	ChancesFor float64     `json:"chances_for" bson:"chances_for"`
	ChancesAga float64     `json:"chances_aga" bson:"chances_aga"`
	CornersFor float64     `json:"corners_for" bson:"corners_for"`
	CornersAga float64     `json:"corners_aga" bson:"corners_aga"`
}
