package football

import "gopkg.in/mgo.v2/bson"
import "time"

type PlayersStats struct {
	ID     bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Rating []struct {
		Date      time.Time `bson:"date,omitempty" json:"date,omitempty"`
		Value     int       `bson:"value" json:"value"`
		FixtureID int       `bson:"fixture_id" json:"fixture_id"`
	} `bson:"rating" json:"rating"`
	Chances []struct {
		Date             time.Time `bson:"date,omitempty" json:"date,omitempty"`
		FixtureID        int       `bson:"fixture_id" json:"fixture_id"`
		GoodChance       int       `bson:"goodchance,omitempty" json:"goodchance,omitempty"`
		VeryGoodChance   int       `bson:"verygoodchance,omitempty" json:"verygoodchance,omitempty"`
		PoorChance       int       `bson:"poorchance,omitempty" json:"poorchance,omitempty"`
		FairlyGoodChance int       `bson:"fairlygoodchance,omitempty" json:"fairlygoodchance,omitempty"`
		Goal             int       `bson:"goal,omitempty" json:"goal,omitempty"`
		GreatChance      int       `bson:"greatchance,omitempty" json:"greatchance,omitempty"`
	} `bson:"chances" json:"chances"`
	Season struct {
		Name struct {
			GsmName string `bson:"gsm_name" json:"gsm_name"`
		} `bson:"name" json:"name"`
		GsmID int `bson:"gsm_id" json:"gsm_id"`
	} `bson:"season" json:"season"`
	Person struct {
		Name struct {
			GsmName string `bson:"gsm_name" json:"gsm_name"`
		} `bson:"name" json:"name"`
		GsmID int `bson:"gsm_id" json:"gsm_id"`
	} `bson:"person" json:"person"`
	Team struct {
		GsmName string `bson:"gsm_name" json:"gsm_name"`
		GsmID   int    `bson:"gsm_id" json:"gsm_id"`
	} `bson:"team" json:"team"`
}
