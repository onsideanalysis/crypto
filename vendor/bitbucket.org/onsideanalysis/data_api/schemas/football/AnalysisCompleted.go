package football

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type AnalysisCompleted struct {
	Type         string `bson:"type" json:"type"`
	SingleLeague bool   `bson:"singleLeague,omitempty" json:"singleLeague,omitempty"`
	Competitions []struct {
		GsmID   int    `bson:"gsm_id" json:"gsm_id"`
		Country string `bson:"country,omitempty" json:"country,omitempty"`
		Name    struct {
			GsmName   string `bson:"gsm_name" json:"gsm_name"`
			ShortName string `bson:"short_name" json:"short_name"`
		} `bson:"name" json:"name"`
	} `bson:"competitions" json:"competitions"`
	Manager struct {
		Id        bson.ObjectId `bson:"_id,omitempty" json:"_id"`
		FirstName string        `bson:"first_name" json:"first_name"`
		LastName  string        `bson:"last_name" json:"last_name"`
		Email     string        `bson:"email" json:"email"`
	} `bson:"manager" json:"manager"`
	Seasons      []BasicSeason `bson:"seasons" json:"seasons"`
	FixturesDate time.Time     `bson:"fixturesDate" json:"fixturesDate"`
	LastUpdated  time.Time     `bson:"last_updated,omitempty" json:"last_updated,omitempty"`
	Sport        string        `bson:"sport,omitempty" json:"sport,omitempty"`
}
