package football

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type PlayerInfo struct {
	GsmID int `bson:"gsm_id" json:"gsm_id"`
	Name  *struct {
		GsmName string `bson:"gsm_name" json:"gsm_name"`
	} `bson:"name" json:"name"`
	Positions     []string `bson:"positions,omitempty" json:"positions,omitempty"`
	ShirtNumber   int      `bson:"shirt_number,omitempty" json:"shirt_number,omitempty"`
	StatPlatID    string   `bson:"statplat_id,omitempty" json:"statplat_id,omitempty"`
	Rating        int      `bson:"rating,omitempty" json:"rating,omitempty"`
	AbilityRating int      `bson:"ability_rating,omitempty" json:"ability_rating,omitempty"`
}

type Rating struct {
	Date  time.Time `bson:"date" json:"date"`
	Value int       `bson:"value" json:"value"`
}

type InjuredPlayerInfo struct {
	Person *struct {
		GsmID int `bson:"gsm_id" json:"gsm_id"`
		Name  *struct {
			GsmName string `bson:"gsm_name" json:"gsm_name"`
		} `bson:"name" json:"name"`
	} `bson:"person,omitempty" json:"person,omitempty"`
	Positions          []string  `bson:"positions,omitempty" json:"positions,omitempty"`
	Ratings            []Rating  `bson:"ratings,omitempty" json:"ratings,omitempty"`
	ExpectedReturnDate time.Time `bson:"expected_return_date,omitempty" json:"expected_return_date,omitempty"`
}

type StatPlatPlayerInfo struct {
	Person *struct {
		GsmID int `bson:"gsm_id" json:"gsm_id"`
		Name  *struct {
			GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
		} `bson:"name,omitempty" json:"name,omitempty"`
	} `bson:"person,omitempty" json:"person,omitempty"`
	Positions      []string `bson:"positions,omitempty" json:"positions,omitempty"`
	ShirtNumber    int      `bson:"shirt_number,omitempty" json:"shirt_number,omitempty"`
	Ratings        []Rating `bson:"ratings,omitempty" json:"ratings,omitempty"`
	AbilityRatings []Rating `bson:"ability_ratings,omitempty" json:"ability_ratings,omitempty"`
}

type PlayerTransferInfo struct {
	FullName       string `bson:"full_name,omitempty" json:"full_name,omitempty"`
	FormerClub     string `bson:"former_club,omitempty" json:"former_club,omitempty"`
	TransferFee    string `bson:"transfer_fee,omitempty" json:"transfer_fee,omitempty"`
	PositionRating string `bson:"position_rating,omitempty" json:"position_rating,omitempty"`
}

type PreSeasonPreview struct {
	TeamGsmID int    `bson:"team_gsm_id" json:"team_gsm_id"`
	Preview   string `bson:"preview,omitempty" json:"preview,omitempty"`
	Ranking   int    `bson:"ranking" json:"ranking"`
}

type StrongestEleven struct {
	TacticalOverview string       `bson:"tactical_overview,omitempty" json:"tactical_overview,omitempty"`
	Players          []PlayerInfo `bson:"players,omitempty" json:"players,omitempty"`
}

type KeyPlayer struct {
	GsmID  int    `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	Review string `bson:"text,omitempty" json:"text,omitempty"`
	Name   string `bson:"name,omitempty" json:"name,omitempty"`
}

type Formation struct {
	Title string `bson:"title,omitempty" json:"title,omitempty"`
	ID    int    `bson:"id,omitempty" json:"id,omitempty"`
}

type TeamAnalysis struct {
	ID               bson.ObjectId `bson:"_id" json:"_id"`
	TeamGsmID        int           `bson:"team_gsm_id" json:"team_gsm_id"`
	CompetitionGsmID int           `bson:"competition_gsm_id" json:"competition_gsm_id"`
	Coach            *struct {
		GsmID                    int    `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
		Analysis                 string `bson:"analysis,omitempty" json:"analysis,omitempty"`
		AnalysisTacticalOverview string `bson:"analysis_tactical_overview,omitempty" json:"analysis_tactical_overview,omitempty"`
	} `bson:"coach,omitempty" json:"coach,omitempty"`
	Squad *struct {
		Overview       string              `bson:"overview,omitempty" json:"overview,omitempty"`
		InjuredPlayers []InjuredPlayerInfo `bson:"injured_players,omitempty" json:"injured_players,omitempty"`
		OnesToWatch    []KeyPlayer         `bson:"ones_to_watch,omitempty" json:"ones_to_watch,omitempty"`
		KeyPlayers     *struct {
			Defensive *KeyPlayer `bson:"defensive,omitempty" json:"defensive,omitempty"`
			Offensive *KeyPlayer `bson:"offensive,omitempty" json:"offensive,omitempty"`
			Creative  *KeyPlayer `bson:"creative,omitempty" json:"creative,omitempty"`
		} `bson:"key_players,omitempty" json:"key_players,omitempty"`
	} `bson:"squad,omitempty" json:"squad,omitempty"`
	StrongestLineup         *StrongestEleven `bson:"strongest_lineup,omitempty" json:"strongest_lineup,omitempty"`
	PreviousStrongestLineup *StrongestEleven `bson:"previous_strongest_lineup,omitempty" json:"previous_strongest_lineup,omitempty"`
	Formation               *Formation       `bson:"formation,omitempty" json:"formation,omitempty"`
	PreviousFormation       *Formation       `bson:"previous_formation,omitempty" json:"previous_formation,omitempty"`
	Transfers               *struct {
		Overview string               `bson:"overview,omitempty" json:"overview,omitempty"`
		Out      []PlayerTransferInfo `bson:"out,omitempty" json:"out,omitempty"`
		In       []PlayerTransferInfo `bson:"in,omitempty" json:"in,omitempty"`
	} `bson:"transfers,omitempty" json:"transfers,omitempty"`
	ThisSeason *struct {
		Preview     string `bson:"preview,omitempty" json:"preview,omitempty"`
		Target      string `bson:"target,omitempty" json:"target,omitempty"`
		ExpPosition int    `bson:"exp_position,omitempty" json:"exp_position,omitempty"`
	} `bson:"this_season,omitempty" json:"this_season,omitempty"`
	PreSeason *struct {
		Mood             string `bson:"mood,omitempty" json:"mood,omitempty"`
		PreSeason        string `bson:"pre_season,omitempty" json:"pre_season,omitempty"`
		PreSeasonAndMood string `bson:"pre_season_and_mood,omitempty" json:"pre_season_and_mood,omitempty"`
	} `bson:"pre_season,omitempty" json:"pre_season,omitempty"`
	LastSeason *struct {
		Results []struct {
			Competition       string `bson:"competition,omitempty" json:"competition,omitempty"`
			FinishingPosition string `bson:"finishing_position,omitempty" json:"finishing_position,omitempty"`
		} `bson:"results,omitempty" json:"results,omitempty"`
	} `bson:"last_season,omitempty" json:"last_season,omitempty"`
	Notes       string    `bson:"notes,omitempty" json:"notes,omitempty"`
	State       string    `bson:"state,omitempty" json:"state,omitempty"`
	LastUpdated time.Time `bson:"last_updated,omitempty" json:"last_updated,omitempty"`
}
