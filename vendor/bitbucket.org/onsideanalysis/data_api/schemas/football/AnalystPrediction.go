package football

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type AnalystPrediction struct {
	ID              bson.ObjectId `bson:"_id" json:"_id"`
	AnalystID       bson.ObjectId `bson:"analyst_id" json:"analyst_id"`
	DateGmt         time.Time     `bson:"date_gmt" json:"date_gmt"`
	IsAnalystActive bool          `bson:"is_analyst_active" json:"is_analyst_active"`
	MarketID        int           `bson:"market_id" json:"market_id"`
	Predictions     []struct {
		Confidence int           `bson:"confidence" json:"confidence"`
		Selection  int           `bson:"selection" json:"selection"`
		Handicap   float64       `bson:"handicap" json:"handicap"`
		Odds       float64       `bson:"odds" json:"odds"`
		Outcome    float64       `bson:"outcome" json:"outcome"`
		Prediction int           `bson:"prediction" json:"prediction"`
		SecurityID bson.ObjectId `bson:"security_id" json:"security_id"`
		ID         bson.ObjectId `bson:"_id" json:"_id"`
	} `bson:"predictions" json:"predictions"`
	Competition struct {
		Name struct {
			GsmName string `bson:"gsm_name" json:"gsm_name"`
		} `bson:"name" json:"name"`
		GsmID int `bson:"gsm_id" json:"gsm_id"`
	} `bson:"competition" json:"competition"`
	FixtureID int  `bson:"fixture_id" json:"fixture_id"`
	IsExcel   bool `bson:"is_excel" json:"is_excel"`
	Season    struct {
		Name struct {
			GsmName string `bson:"gsm_name" json:"gsm_name"`
		} `bson:"name" json:"name"`
		GsmID int `bson:"gsm_id" json:"gsm_id"`
	} `bson:"season" json:"season"`
	Reason1 string `bson:"reason1" json:"reason1"`
	Reason2 string `bson:"reason2" json:"reason2"`
	Reason3 string `bson:"reason3" json:"reason3"`
}
