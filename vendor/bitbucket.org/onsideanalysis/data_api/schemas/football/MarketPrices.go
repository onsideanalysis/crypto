package football

//representing redis document for prices
type FixturePrices struct {
	BetDetails struct {
		BetTypeId        int                    `bson:"bet_type_id" json:"bet_type_id"`
		Handicap         float32                `bson:"handicap" json:"handicap"`
		BetPlacementInfo BookmakerPlacementInfo `bson:"bet_placement_info" json:"bet_placement_info"`
	} `json:"bet_details"`
	IsBack   bool      `bson:"is_back" json:"is_back"`
	MarketId int       `bson:"market_id" json:"market_id"`
	Price    []float32 `bson:"price" json:"price"`
	Volume   []float32 `bson:"volume,omitempty" json:"volume,omitempty"`
}

//representing redis document for fixture status
type FixtureStatus struct {
	CurrentMinute int `json:"current_minute"`
	CurrentHalf   int `json:"current_half"`
	RedCards      struct {
		Participant1 int `json:"participant1"`
		Participant2 int `json:"participant2"`
	} `json:"red_cards"`
	Score struct {
		Participant1 int `json:"participant1"`
		Participant2 int `json:"participant2"`
	} `json:"score"`
}

// representing a fixture with markets, prices and status
type FixturePricesResult struct {
	FixtureId string        `json:"fixture_id"`
	Markets   []Market      `json:"markets"`
	Status    FixtureStatus `json:"status"`
}

// representing a Market
type Market struct {
	MarketId                 int       `json:"market_id"`
	Prices                   []BetType `json:"bet_type_prices"`
	BestNaturalHandicap      float32   `json:"best_natural_handicap"`
	BestNaturalHandicapPrice float32   `json:"best_natural_handicap_price"`
}

type BookmakerPlacementInfo struct {
	BetFairInfo   BetFairPlacementInfo   `json:"bet_fair_info"`
	MatchBookInfo MatchBookPlacementInfo `json:"matchbook_info"`
}

// representing a BetType
type BetType struct {
	BetTypeId   int       `json:"bet_type_id"`
	Handicap    float32   `json:"handicap"`
	Price       []float32 `json:"price"`
	Volume      []float32 `json:"volume,omitempty"`
	BookmakerId string    `json:"bookmaker_id"`
	IsBack      bool      `json:"is_back"`
}

type BetFairPlacementInfo struct {
	BookmakerID int     `json:"bookmaker_id"`
	EventId     string  `json:"event_id"`
	MarketId    string  `json:"market_id"`
	SelectionId int     `json:"selection_id"`
	HandicapId  float64 `json:"handicap_id"`
}

type MatchBookPlacementInfo struct {
	BookmakerID int `json:"bookmaker_id"`
	EventId     int `json:"event_id"`
	MarketId    int `json:"market_id"`
	RunnerId    int `json:"runner_id"` //MatchBook only
}

//representing redis document for prices
type FixturePricesBetFair struct {
	BetDetails struct {
		BetTypeId        int                  `bson:"bet_type_id" json:"bet_type_id"`
		Handicap         float32              `bson:"handicap" json:"handicap"`
		BetPlacementInfo BetFairPlacementInfo `bson:"bet_placement_info" json:"bet_placement_info"`
	} `json:"bet_details"`
	IsBack   bool      `bson:"is_back" json:"is_back"`
	MarketId int       `bson:"market_id" json:"market_id"`
	Price    []float32 `bson:"price" json:"price"`
	Volume   []float32 `bson:"volume,omitempty" json:"volume,omitempty"`
}

//representing redis document for prices
type FixturePricesMatchBook struct {
	BetDetails struct {
		BetTypeId        int                    `bson:"bet_type_id" json:"bet_type_id"`
		Handicap         float32                `bson:"handicap" json:"handicap"`
		BetPlacementInfo MatchBookPlacementInfo `bson:"bet_placement_info" json:"bet_placement_info"`
	} `json:"bet_details"`
	IsBack   bool      `bson:"is_back" json:"is_back"`
	MarketId int       `bson:"market_id" json:"market_id"`
	Price    []float32 `bson:"price" json:"price"`
	Volume   []float32 `bson:"volume,omitempty" json:"volume,omitempty"`
}
