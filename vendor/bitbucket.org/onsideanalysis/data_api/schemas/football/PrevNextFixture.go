package football

type PrevNextFixture struct {
	ShowPreviewOrReviewLink bool               `bson:"showPreviewOrReviewLink" json:"showPreviewOrReviewLink"`
	ID                      string             `bson:"_id,omitempty" json:"_id,omitempty"`
	GsmID                   int                `bson:"gsm_id" json:"gsm_id"`
	Score                   FixtureScore       `bson:"score" json:"score"`
	Team1                   FixtureTeam        `bson:"team1" json:"team1"`
	Team2                   FixtureTeam        `bson:"team2" json:"team2"`
	Competition             FixtureCompetition `bson:"competition" json:"competition"`
	Venue                   FixtureVenue       `bson:"venue" json:"venue"`
	KickOff                 FixtureKickOff     `bson:"kick_off" json:"kick_off"`
}
