package football

import (
	"time"

	"bitbucket.org/onsideanalysis/data_api/utilities"
	"gopkg.in/mgo.v2/bson"
)

type CleanFeedEventStatus struct {
	ThrowIns         []int         `bson:"throw_ins,omitempty" json:"throw_ins,omitempty"`
	DangerousAttacks []int         `bson:"dangerous_attacks,omitempty" json:"dangerous_attacks,omitempty"`
	FreeKicks        []int         `bson:"free_kicks,omitempty" json:"free_kicks,omitempty"`
	ShotsOffTarget   []int         `bson:"shots_off_target,omitempty" json:"shots_off_target,omitempty"`
	ShotsOnTarget    []int         `bson:"shots_on_target,omitempty" json:"shots_on_target,omitempty"`
	RedCards         []int         `bson:"red_cards,omitempty" json:"red_cards,omitempty"`
	YellowCards      []int         `bson:"yellow_cards,omitempty" json:"yellow_cards,omitempty"`
	Minute           int           `bson:"minute,omitempty" json:"minute,omitempty"`
	EventId          int           `bson:"event_id,omitempty" json:"event_id,omitempty"`
	Corners          []int         `bson:"corners,omitempty" json:"corners,omitempty"`
	Possession       []int         `bson:"posession,omitempty" json:"posession,omitempty"`
	Attacks          []int         `bson:"attacks,omitempty" json:"attacks,omitempty"`
	Fouls            []int         `bson:"fouls,omitempty" json:"fouls,omitempty"`
	Goals            []int         `bson:"goals,omitempty" json:"goals,omitempty"`
	State            FootballState `bson:"state,omitempty" json:"state,omitempty"`
	GoalKicks        []int         `bson:"goal_kicks,omitempty" json:"goal_kicks,omitempty"`
	Timestamp        int64         `bson:"timestamp,omitempty" json:"timestamp,omitempty"`
	GoalEvents       GoalEvents    `bson:"goal_events,omitempty" json:"goal_events,omitempty"`
	CardEvents       CardEvents    `bson:"card_events,omitempty" json:"card_events,omitempty"`
}

func (e *CleanFeedEventStatus) IsInPlay() bool {
	return e != nil &&
		e.State != PRE_MATCH &&
		e.State != UNKNOWN &&
		e.State != FULL_TIME
}

type GsmEventStatus struct {
	ID               bson.ObjectId         `bson:"_id,omitempty" json:"_id,omitempty"`
	ThrowIns         []int                 `bson:"throw_ins,omitempty" json:"throw_ins,omitempty"`
	DangerousAttacks []int                 `bson:"dangerous_attacks,omitempty" json:"dangerous_attacks,omitempty"`
	AddedMinutes     int                   `bson:"added_minutes,omitempty" json:"added_minutes,omitempty"`
	FreeKicks        []int                 `bson:"free_kicks,omitempty" json:"free_kicks,omitempty"`
	ShotsOffTarget   []int                 `bson:"shots_off_target,omitempty" json:"shots_off_target,omitempty"`
	ShotsOnTarget    []int                 `bson:"shots_on_target,omitempty" json:"shots_on_target,omitempty"`
	RedCards         []int                 `bson:"redcards,omitempty" json:"redcards,omitempty"`
	YellowCards      []int                 `bson:"yellow_cards,omitempty" json:"yellow_cards,omitempty"`
	Minute           int                   `bson:"minute,omitempty" json:"minute,omitempty"`
	EventId          int                   `bson:"event_id,omitempty" json:"event_id,omitempty"`
	Corners          []int                 `bson:"corners,omitempty" json:"corners,omitempty"`
	Possession       []int                 `bson:"posession,omitempty" json:"posession,omitempty"`
	Attacks          []int                 `bson:"attacks,omitempty" json:"attacks,omitempty"`
	Fouls            []int                 `bson:"fouls,omitempty" json:"fouls,omitempty"`
	Score            []int                 `bson:"score,omitempty" json:"score,omitempty"`
	MatchPeriod      FootballState         `bson:"match_period,omitempty" json:"match_period,omitempty"`
	GoalKicks        []int                 `bson:"goal_kicks,omitempty" json:"goal_kicks,omitempty"`
	Timestamp        time.Time             `bson:"timestamp,omitempty" json:"timestamp,omitempty"`
	Goals            map[string][]GoalInfo `bson:"goals,omitempty" json:"goals,omitempty"`
}

type GoalEvents struct {
	Team1 []GoalInfo `bson:"team1,omitempty" json:"team1,omitempty"`
	Team2 []GoalInfo `bson:"team2,omitempty" json:"team2,omitempty"`
}

type GoalInfo struct {
	Scorer      string `bson:"scorer,omitempty" json:"scorer,omitempty"`
	Minute      int    `bson:"min,omitempty" json:"min,omitempty"`
	AddedMinute int    `bson:"min_extra,omitempty" json:"min_extra,omitempty"`
	ScorerId    int    `bson:"scorer_id,omitempty" json:"scorer_id,omitempty"`
	Assist      string `bson:"assist,omitempty" json:"assist,omitempty"`
	AssistId    int    `bson:"assist_id,omitempty" json:"assist_id,omitempty"`
	GoalType    int    `bson:"goal_type,omitempty" json:"goal_type,omitempty"`
}

type CardEvents struct {
	Team1 []CardInfo `bson:"team1,omitempty" json:"team1,omitempty"`
	Team2 []CardInfo `bson:"team2,omitempty" json:"team2,omitempty"`
}

type CardInfo struct {
	Player      string `bson:"player,omitempty" json:"player,omitempty"`
	PlayerId    int    `bson:"player_id,omitempty" json:"player_id,omitempty"`
	Minute      int    `bson:"min,omitempty" json:"min,omitempty"`
	AddedMinute int    `bson:"min_extra,omitempty" json:"min_extra,omitempty"`
	CardType    int    `bson:"card_type,omitempty" json:"card_type,omitempty"`
}

func CleanFeedToGsmEventStatus(cleanfeed CleanFeedEventStatus) (gsm GsmEventStatus) {

	gsm.ThrowIns = cleanfeed.ThrowIns
	gsm.DangerousAttacks = cleanfeed.DangerousAttacks
	gsm.FreeKicks = cleanfeed.FreeKicks
	gsm.ShotsOffTarget = cleanfeed.ShotsOffTarget
	gsm.ShotsOnTarget = cleanfeed.ShotsOnTarget
	gsm.RedCards = cleanfeed.RedCards
	gsm.YellowCards = cleanfeed.YellowCards
	gsm.Minute = cleanfeed.Minute
	gsm.EventId = cleanfeed.EventId
	gsm.Corners = cleanfeed.Corners
	gsm.Possession = cleanfeed.Possession
	gsm.Attacks = cleanfeed.Attacks
	gsm.Fouls = cleanfeed.Fouls
	gsm.GoalKicks = cleanfeed.GoalKicks

	//These fields are different
	gsm.Timestamp = utilities.SecondsToTime(cleanfeed.Timestamp)
	gsm.Score = cleanfeed.Goals
	gsm.MatchPeriod = cleanfeed.State
	if cleanfeed.State == FIRST_HALF && cleanfeed.Minute > 45 {
		gsm.Minute = 45
		gsm.AddedMinutes = cleanfeed.Minute - 45
	} else if cleanfeed.State == SECOND_HALF && cleanfeed.Minute > 90 {
		gsm.Minute = 90
		gsm.AddedMinutes = cleanfeed.Minute - 90
	}

	gsm.Goals = make(map[string][]GoalInfo)
	if len(cleanfeed.GoalEvents.Team1) > 0 {
		gsm.Goals["team1"] = cleanfeed.GoalEvents.Team1
	}
	if len(cleanfeed.GoalEvents.Team2) > 0 {
		gsm.Goals["team2"] = cleanfeed.GoalEvents.Team2
	}

	return
}

func GsmToCleanFeedEventStatus(gsm GsmEventStatus) (cleanfeed CleanFeedEventStatus) {

	cleanfeed.ThrowIns = gsm.ThrowIns
	cleanfeed.DangerousAttacks = gsm.DangerousAttacks
	cleanfeed.FreeKicks = gsm.FreeKicks
	cleanfeed.ShotsOffTarget = gsm.ShotsOffTarget
	cleanfeed.ShotsOnTarget = gsm.ShotsOnTarget
	cleanfeed.RedCards = gsm.RedCards
	cleanfeed.YellowCards = gsm.YellowCards
	cleanfeed.Minute = gsm.Minute
	cleanfeed.EventId = gsm.EventId
	cleanfeed.Corners = gsm.Corners
	cleanfeed.Possession = gsm.Possession
	cleanfeed.Attacks = gsm.Attacks
	cleanfeed.Fouls = gsm.Fouls
	cleanfeed.GoalKicks = gsm.GoalKicks

	//These fields are different
	cleanfeed.Timestamp = utilities.TimeToSeconds(gsm.Timestamp)
	cleanfeed.Goals = gsm.Score
	cleanfeed.State = gsm.MatchPeriod

	if gsm.MatchPeriod == FIRST_HALF && gsm.Minute == 45 && gsm.AddedMinutes > 0 {
		cleanfeed.Minute = gsm.Minute + gsm.AddedMinutes
	} else if gsm.MatchPeriod == SECOND_HALF && gsm.Minute == 90 && gsm.AddedMinutes > 0 {
		cleanfeed.Minute = gsm.Minute + gsm.AddedMinutes
	}

	if len(gsm.Goals) > 0 {
		cleanfeed.GoalEvents.Team1 = gsm.Goals["team1"]
		cleanfeed.GoalEvents.Team2 = gsm.Goals["team2"]
	}

	return
}
