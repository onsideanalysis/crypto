package football

import (
	//"github.com/goamz/goamz/autoscaling"
	"time"

	"bitbucket.org/onsideanalysis/data_api/schemas/stickers"
	"gopkg.in/mgo.v2/bson"
)

type PreviewReviewTeamPlayer struct {
	ID            bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	GsmID         int           `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	Name          *GsmName      `bson:"name,omitempty" json:"name,omitempty"`
	Positions     []string      `bson:"positions,omitempty" json:"positions,omitempty"`
	Rating        int           `bson:"rating,omitempty" json:"rating,omitempty"`
	AbilityRating int           `bson:"ability_rating,omitempty" json:"ability_rating,omitempty"`
	Probability   int           `bson:"probability,omitempty" json:"probability,omitempty"`
	Reason        string        `bson:"reason,omitempty" json:"reason,omitempty"`
	ShirtNumber   int           `bson:"shirt_number,omitempty" json:"shirt_number,omitempty"`
	StatplatID    bson.ObjectId `bson:"statplat_id,omitempty" json:"statplat_id,omitempty"`
	StartTime     string        `bson:"start_time,omitempty" json:"start_time,omitempty"`
	EndTime       string        `bson:"end_time,omitempty" json:"end_time,omitempty"`
}

type PreviewReviewTeamPlayerExtended struct {
	ID                        bson.ObjectId            `bson:"_id,omitempty" json:"_id,omitempty"`
	GsmID                     int                      `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	Name                      *GsmName                 `bson:"name,omitempty" json:"name,omitempty"`
	Positions                 []string                 `bson:"positions,omitempty" json:"positions,omitempty"`
	Rating                    int                      `bson:"rating,omitempty" json:"rating,omitempty"`
	AbilityRating             int                      `bson:"ability_rating,omitempty" json:"ability_rating,omitempty"`
	Probability               int                      `bson:"probability,omitempty" json:"probability,omitempty"`
	ExpectedLineupProbability int                      `bson:"expected_lineup_probability,omitempty" json:"expected_lineup_probability,omitempty"`
	Reason                    string                   `bson:"reason,omitempty" json:"reason,omitempty"`
	ShirtNumber               int                      `bson:"shirt_number,omitempty" json:"shirt_number,omitempty"`
	StatplatID                bson.ObjectId            `bson:"statplat_id,omitempty" json:"statplat_id,omitempty"`
	StartTime                 string                   `bson:"start_time,omitempty" json:"start_time,omitempty"`
	EndTime                   string                   `bson:"end_time,omitempty" json:"end_time,omitempty"`
	AlternativePlayer         *PreviewReviewTeamPlayer `bson:"alternative_player,omitempty" json:"alternative_player,omitempty"`
}

type TeamFormation struct {
	ID     int     `bson:"id,omitempty" json:"id,omitempty"`
	Title  string  `bson:"title,omitempty" json:"title,omitempty"`
	Custom [][]int `bson:"custom,omitempty" json:"custom,omitempty"`
}

type PreviewTeam struct {
	GsmID          int      `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	News           string   `bson:"news,omitempty" json:"news,omitempty"`
	NewsUpdate     string   `bson:"news_update,omitempty" json:"news_update,omitempty"`
	Headlines      []string `bson:"headlines,omitempty" json:"headlines,omitempty"`
	BettingSummary []string `bson:"betting_summary,omitempty" json:"betting_summary,omitempty"`
	GameKeys       *struct {
		ExpectedMotivation   int      `bson:"expected_motivation,omitempty" json:"expected_motivation,omitempty"`
		ExpectedMood         int      `bson:"expected_mood,omitempty" json:"expected_mood,omitempty"`
		ExpectedTactical     int      `bson:"expected_tactical,omitempty" json:"expected_tactical,omitempty"`
		ApproachSixtyMinutes int      `bson:"approach_sixty_minutes,omitempty" json:"approach_sixty_minutes,omitempty"`
		Tacticals            []string `bson:"tacticals,omitempty" json:"tacticals,omitempty"`
		Motivations          []string `bson:"motivations,omitempty" json:"motivations,omitempty"`
	} `bson:"game_keys,omitempty" json:"game_keys,omitempty"`
	Formation TeamFormation `bson:"formation" json:"formation"`
	Players   *struct {
		ReturningNew []PreviewReviewTeamPlayer         `bson:"returning_new,omitempty" json:"returning_new,omitempty"`
		BenchFrom    []PreviewReviewTeamPlayer         `bson:"bench_from,omitempty" json:"bench_from,omitempty"`
		Doubtful     []PreviewReviewTeamPlayer         `bson:"doubtful,omitempty" json:"doubtful,omitempty"`
		Lineup       []PreviewReviewTeamPlayerExtended `bson:"lineup,omitempty" json:"lineup,omitempty"`
		Unavailable  []PreviewReviewTeamPlayer         `bson:"unavailable,omitempty" json:"unavailable,omitempty"`
	} `bson:"players,omitempty" json:"players,omitempty"`
	Name *GsmName `bson:"name,omitempty" json:"name,omitempty"`
}

type PreviewReviewVenue struct {
	Capacity  int     `bson:"capacity" json:"capacity"`
	Distance  float32 `bson:"distance,omitempty" json:"distance,omitempty"`
	GsmId     int     `bson:"gsm_id" json:"gsm_id"`
	PitchType string  `bson:"pitch_type" json:"pitch_type"`
	Name      struct {
		GsmName string `bson:"gsm_name" json:"gsm_name"`
	} `bson:"name" json:"name"`
	Area struct {
		GsmID int `bson:"gsm_id" json:"gsm_id"`
		Name  struct {
			GsmName string `bson:"gsm_name" json:"gsm_name"`
		} `bson:"name" json:"name"`
	} `bson:"area" json:"area"`
	Location struct {
		City      string  `bson:"city" json:"city"`
		Latitude  float64 `bson:"latitude" json:"latitude"`
		Longitude float64 `bson:"longitude" json:"longitude"`
	} `bson:"location" json:"location"`
}

type PreviewReviewFixture struct {
	GsmID       int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	Competition *struct {
		GsmID     int    `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
		ShortName string `bson:"short_name,omitempty" json:"short_name,omitempty"`
		GsmName   string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
		Country   string `bson:"country,omitempty" json:"country,omitempty"`
	} `bson:"competition,omitempty" json:"competition,omitempty"`
	KickOff *FixtureKickOff  `bson:"kick_off,omitempty" json:"kick_off,omitempty"`
	Score   *FixtureAllScore `bson:"score" json:"score"`
	Area    struct {
		GsmId int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
		Name  struct {
			GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
		} `bson:"name,omitempty" json:"name,omitempty"`
	} `bson:"area,omitempty" json:"area,omitempty"`
	Cards *struct {
		Gsm struct {
			Team1 []FixtureCard `bson:"team1,omitempty" json:"team1,omitempty"`
			Team2 []FixtureCard `bson:"team2,omitempty" json:"team2,omitempty"`
		} `bson:"gsm" json:"gsm"`
	} `bson:"cards,omitempty" json:"cards,omitempty"`
	Goals *struct {
		Gsm struct {
			Team1 []FixtureGoal `bson:"team1" json:"team1"`
			Team2 []FixtureGoal `bson:"team2" json:"team2"`
		} `bson:"gsm" json:"gsm"`
	} `bson:"goals,omitempty" json:"goals,omitempty"`
	Data *struct {
		Statistics struct {
			Gsm []struct {
				Type  string      `bson:"type" json:"type"`
				Value interface{} `bson:"value" json:"value"`
			} `bson:"gsm" json:"gsm"`
			Analysts *AnalystsData `bson:"analysts,omitempty" json:"analysts,omitempty"`
		} `bson:"statistics" json:"statistics"`
	} `bson:"data,omitempty" json:"data,omitempty"`
	Team1 struct {
		GsmID int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
		Name  struct {
			GsmName      string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
			GsmShortName string `bson:"gsm_short_name,omitempty" json:"gsm_short_name,omitempty"`
			GsmAbbr      string `bson:"gsm_abbr,omitempty" json:"gsm_abbr,omitempty"`
		} `bson:"name" json:"name"`
	} `bson:"team1,omitempty" json:"team1,omitempty"`
	Team2 struct {
		GsmID int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
		Name  struct {
			GsmName      string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
			GsmShortName string `bson:"gsm_short_name,omitempty" json:"gsm_short_name,omitempty"`
			GsmAbbr      string `bson:"gsm_abbr,omitempty" json:"gsm_abbr,omitempty"`
		} `bson:"name" json:"name"`
	} `bson:"team2,omitempty" json:"team2,omitempty"`
	Venue *PreviewReviewVenue `bson:"venue" json:"venue"`
	Odds  *FixtureOdds        `bson:"odds,omitempty" json:"odds,omitempty"`
}

type PreviewReviewSummary struct {
	NotificationType string                `bson:"type,omitempty" json:"type,omitempty"`
	State            string                `bson:"state,omitempty" json:"state,omitempty"`
	IsNew            bool                  `bson:"isNew,omitempty" json:"isNew,omitempty"`
	LastUpdated      time.Time             `bson:"last_updated,omitempty" json:"last_updated,omitempty"`
	Team1            string                `json:"team1,omitempty" bson:"team1,omitempty"`
	Team2            string                `json:"team2,omitempty" bson:"team2,omitempty"`
	Fixture          *PreviewReviewFixture `json:"fixture,omitempty" bson:"fixture,omitempty"`
	Sport            string                `bson:"sport,omitempty" json:"sport,omitempty"`
}

type PreviewConditions struct {
	Pitch          string `bson:"pitch,omitempty" json:"pitch,omitempty"`
	Temperature    int    `bson:"temperature,omitempty" json:"temperature,omitempty"`
	Travel         string `bson:"travel,omitempty" json:"travel,omitempty"`
	TravelDistance string `bson:"travel_distance,omitempty" json:"travel_distance,omitempty"`
	Weather        string `bson:"weather,omitempty" json:"weather,omitempty"`
}

type PreviewReviewActivity struct {
	Action      string    `bson:"action,omitempty" json:"action,omitempty"`
	AnalystId   string    `bson:"analyst_id,omitempty" json:"analyst_id,omitempty"`
	AnalystName string    `bson:"analyst_name,omitempty" json:"analyst_name,omitempty"`
	Date        time.Time `bson:"date,omitempty" json:"date,omitempty"`
}

type PreviewStandings struct {
	Teams []struct {
		GsmId  int     `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
		Name   GsmName `bson:"name,omitempty" json:"name,omitempty"`
		OptaId int     `bson:"opta_id,omitempty" json:"opta_id,omitempty"`
		Record struct {
			Away struct {
				Drawn        int `bson:"drawn" json:"drawn"`
				GoalsAgainst int `bson:"goals_against" json:"goals_against"`
				GoalsFor     int `bson:"goals_for" json:"goals_for"`
				Lost         int `bson:"lost" json:"lost"`
				Played       int `bson:"played" json:"played"`
				Points       int `bson:"points" json:"points"`
				Rank         int `bson:"rank" json:"rank"`
				Won          int `bson:"won" json:"won"`
			} `bson:"away,omitempty" json:"away,omitempty"`
			Home struct {
				Drawn        int `bson:"drawn" json:"drawn"`
				GoalsAgainst int `bson:"goals_against" json:"goals_against"`
				GoalsFor     int `bson:"goals_for" json:"goals_for"`
				Lost         int `bson:"lost" json:"lost"`
				Played       int `bson:"played" json:"played"`
				Points       int `bson:"points" json:"points"`
				Rank         int `bson:"rank" json:"rank"`
				Won          int `bson:"won" json:"won"`
			} `bson:"home,omitempty" json:"home,omitempty"`
			Round struct {
				GsmId int     `bson:"gsm_id" json:"gsm_id"`
				Name  GsmName `bson:"name" json:"name"`
			} `bson:"round,omitempty" json:"round,omitempty"`
			Total struct {
				Deductions   int `bson:"deductions" json:"deductions"`
				Drawn        int `bson:"drawn" json:"drawn"`
				GoalsAgainst int `bson:"goals_against" json:"goals_against"`
				GoalsFor     int `bson:"goals_for" json:"goals_for"`
				Lost         int `bson:"lost" json:"lost"`
				Played       int `bson:"played" json:"played"`
				Points       int `bson:"points" json:"points"`
				Rank         int `bson:"rank" json:"rank"`
				Won          int `bson:"won" json:"won"`
			} `bson:"total,omitempty" json:"total,omitempty"`
			Group struct {
				GsmId int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
				Name  struct {
					GsmName `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
				} `bson:"name,omitempty" json:"name,omitempty"`
			} `bson:"group,omitempty" json:"group,omitempty"`
		} `bson:"record,omitempty" json:"record,omitempty"`
	} `bson:"teams,omitempty" json:"teams,omitempty"`
	Generated string `bson:"generated,omitempty" json:"generated,omitempty"`
}

type Preview struct {
	Id          bson.ObjectId            `bson:"_id,omitempty" json:"_id,omitempty"`
	V           int                      `bson:"__v,omitempty" json:"__v,omitempty"`
	Activity    *[]PreviewReviewActivity `bson:"activity,omitempty" json:"activity,omitempty"`
	AnalystView *AnalystView             `bson:"analyst_view,omitempty" json:"analyst_view,omitempty"`
	Conditions  *PreviewConditions       `bson:"conditions,omitempty" json:"conditions,omitempty"`
	Fixture     *PreviewReviewFixture    `bson:"fixture,omitempty" json:"fixture,omitempty"`
	LastUpdated time.Time                `bson:"last_updated,omitempty" json:"last_updated,omitempty"`
	Standings   *PreviewStandings        `bson:"standings,omitempty" json:"standings,omitempty"`
	State       string                   `bson:"state,omitempty" json:"state,omitempty"`
	Team1       *PreviewTeam             `bson:"team1,omitempty" json:"team1,omitempty"`
	Team2       *PreviewTeam             `bson:"team2,omitempty" json:"team2,omitempty"`
	Version     int                      `bson:"version,omitempty" json:"version,omitempty"`
}

type AnalystSuggestion struct {
	Summary    string           `bson:"summary" json:"summary"`
	Suggestion string           `bson:"suggestion" json:"suggestion"`
	Sticker    stickers.Sticker `bson:"sticker" json:"sticker"`
	Odds       float64          `bson:"odds" json:"odds"`
	Outcome    float64          `bson:"outcome" json:"outcome"`
	Return     float64          `bson:"return" json:"return"`
}

type AnalystView struct {
	Summary12 string `bson:"summary_1_2" json:"summary_1_2"`
	SummaryAH string `bson:"summary_ah" json:"summary_ah"`
	SummaryOU string `bson:"summary_ou" json:"summary_ou"`
	Outcome12 string `bson:"outcome_1_2" json:"outcome_1_2"`
	OutcomeAH string `bson:"outcome_ah" json:"outcome_ah"`
	OutcomeOU string `bson:"outcome_ou" json:"outcome_ou"`

	Data map[string]AnalystSuggestion `bson:"data" json:"data"`
}

type PreviewBetting struct {
	Id      bson.ObjectId         `bson:"_id,omitempty" json:"_id,omitempty"`
	Fixture *PreviewReviewFixture `bson:"fixture,omitempty" json:"fixture,omitempty"`
	Team1   *PreviewBettingTeam   `bson:"team1,omitempty" json:"team1,omitempty"`
	Team2   *PreviewBettingTeam   `bson:"team2,omitempty" json:"team2,omitempty"`
}

type PreviewBettingTeam struct {
	GsmID          int      `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	BettingSummary []string `bson:"betting_summary,omitempty" json:"betting_summary,omitempty"`
	Name           *GsmName `bson:"name,omitempty" json:"name,omitempty"`
}

type PreviewResult struct {
	LastGames *[]PrevNextFixture `json:"lastGames,omitempty"`
	NextGames *[]PrevNextFixture `json:"nextGames,omitempty"`
	Preview   *Preview           `json:"preview,omitempty"`
}
