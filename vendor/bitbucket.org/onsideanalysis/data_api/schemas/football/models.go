package football

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type ModelDomain struct {
	GsmID float64 `json:"gsm_id,omitempty" bson:"gsm_id,omitempty"`
	Name  struct {
		GsmName string `json:"gsm_name,omitempty" bson:"gsm_name,omitempty"`
	} `json:"name" bson:"name"`
	SubType string `json:"sub_type,omitempty" bson:"sub_type,omitempty"`
	Type    string `json:"type,omitempty" bson:"type,omitempty"`
}

type ModelDomainOutput struct {
	GsmID float64 `json:"gsm_id,omitempty" bson:"gsm_id,omitempty"`
	Name  struct {
		GsmName string `json:"gsm_name,omitempty" bson:"gsm_name,omitempty"`
	} `json:"name" bson:"name"`
	Type    string `json:"type,omitempty" bson:"type,omitempty"`
	Options struct {
		Model *struct {
			ID   bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
			Name struct {
				CodeName []string `json:"code_name,omitempty" bson:"code_name,omitempty"`
			} `json:"name,omitempty" bson:"name,omitempty"`
		} `json:"model,omitempty" bson:"model,omitempty"`
		Values map[string]interface{} `json:"values,omitempty" bson:"values,omitempty"`
	} `json:"options,omitempty" bson:"options,omitempty"`
	UpdateTime time.Time `json:"update_time" bson:"update_time"`
}

type ModelConfig struct {
	ID     bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
	Domain *ModelDomain  `json:"domain,omitempty" bson:"domain,omitempty"`
	Model  *struct {
		ID   bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
		Name struct {
			CodeName []string `json:"code_name,omitempty" bson:"code_name,omitempty"`
		} `json:"name,omitempty" bson:"name,omitempty"`
	} `json:"model,omitempty" bson:"model,omitempty"`
	Options    map[string]interface{} `json:"options,omitempty" bson:"options,omitempty"`
	UpdateTime time.Time              `json:"update_time" bson:"update_time"`
}

type Model struct {
	ID   bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
	Name *struct {
		Model struct {
			CodeName string `json:"code_name,omitempty" bson:"code_name,omitempty"`
		} `json:"model,omitempty" bson:"model,omitempty"`
		Statplat string `json:"statplat,omitempty" bson:"statplat,omitempty"`
	} `json:"name,omitempty" bson:"name,omitempty"`
	Options    map[string]interface{} `json:"options,omitempty" bson:"options,omitempty"`
	Priority   int                    `json:"priority,omitempty" bson:"priority,omitempty"`
	UpdateTime time.Time              `json:"update_time" bson:"update_time"`
}

type ModelRun struct {
	ID      bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
	Details *struct {
		EndTime   time.Time `json:"end_time" bson:"end_time"`
		StartTime time.Time `json:"start_time" bson:"start_time"`
		Status    string    `json:"status,omitempty" bson:"status,omitempty"`
	} `json:"details,omitempty" bson:"details,omitempty"`
	ModelConfigID bson.ObjectId `json:"model_config_id,omitempty" bson:"model_config_id,omitempty"`
	Parameters    []struct {
		Name  string `json:"name" bson:"name"`
		Owner struct {
			GsmID int `json:"gsm_id,omitempty" bson:"gsm_id,omitempty"`
			Name  struct {
				GsmName string `json:"gsm_name" bson:"gsm_name"`
			} `json:"name" bson:"name"`
			Type string `json:"type"`
		} `json:"owner" bson:"owner"`
		Value float64 `json:"value" bson:"value"`
	} `json:"parameters" bson:"parameters"`
	Range *struct {
		FitEndDate       time.Time `json:"fit_end_date" bson:"fit_end_date"`
		FitStartDate     time.Time `json:"fit_start_date" bson:"fit_start_date"`
		PredictEndDate   time.Time `json:"predict_end_date" bson:"predict_end_date"`
		PredictStartDate time.Time `json:"predict_start_date" bson:"predict_start_date"`
	} `json:"range,omitempty" bson:"range,omitempty"`
	UpdateTime time.Time `json:"update_time" bson:"update_time"`
}

// ModelRatingRating used for the "Ratings" field of the ModelRating struct.
type ModelRatingRating struct {
	Name  string `json:"name" bson:"name"`
	Owner struct {
		GsmID int `json:"gsm_id" bson:"gsm_id"`
		Name  struct {
			GsmName string `json:"gsm_name" bson:"gsm_name"`
		} `json:"name" bson:"name"`
		Type string `json:"type" bson:"type"`
	} `json:"owner" bson:"owner"`
	Value float64 `json:"value" bson:"value"`
}

type ModelRating struct {
	ID  bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
	For *struct {
		GsmID int    `json:"gsm_id" bson:"gsm_id"`
		Type  string `json:"type" bson:"type"`
	} `json:"for,omitempty" bson:"for,omitempty"`
	ModelConfigID bson.ObjectId       `json:"model_config_id,omitempty" bson:"model_config_id,omitempty"`
	ModelRunID    bson.ObjectId       `json:"model_run_id,omitempty" bson:"model_run_id,omitempty"`
	Ratings       []ModelRatingRating `json:"ratings,omitempty" bson:"ratings,omitempty"`
	Season        *struct {
		GsmID int `json:"gsm_id" bson:"gsm_id"`
		Name  struct {
			GsmName string `json:"gsm_name" bson:"gsm_name"`
		} `json:"name" bson:"name"`
	} `json:"season,omitempty" bson:"season,omitempty"`
	UpdateTime    time.Time `json:"update_time" bson:"update_time"`
	ValidFromDate time.Time `json:"valid_from_date" bson:"valid_from_date"`
}

type CompetitionPredictionsValue struct {
	Name  string `json:"name" bson:"name"`
	Owner struct {
		GsmID int `json:"gsm_id" bson:"gsm_id"`
		Name  struct {
			GsmName string `json:"gsm_name" bson:"gsm_name"`
		} `json:"name" bson:"name"`
		Type string `json:"type" bson:"type"`
	} `json:"owner" bson:"owner"`
	Value float64 `json:"value" bson:"value"`
}

type CompetitionPredictions struct {
	ID  bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
	For struct {
		GsmID int    `json:"gsm_id" bson:"gsm_id"`
		Type  string `json:"type" bson:"type"`
	} `json:"for" bson:"for"`
	ModelConfigID bson.ObjectId                  `json:"model_config_id,omitempty" bson:"model_config_id,omitempty"`
	ModelRunID    bson.ObjectId                  `json:"model_run_id,omitempty" bson:"model_run_id,omitempty"`
	Predictions   *[]CompetitionPredictionsValue `json:"predictions,omitempty" bson:"predictions,omitempty"`
	Season        *struct {
		Competition struct {
			GsmID int `json:"gsm_id" bson:"gsm_id"`
			Name  struct {
				GsmName string `json:"gsm_name" bson:"gsm_name"`
			} `json:"name" bson:"name"`
		} `json:"competition" bson:"competition"`
		GsmID int `json:"gsm_id" bson:"gsm_id"`
		Name  struct {
			GsmName string `json:"gsm_name" bson:"gsm_name"`
		} `json:"name" bson:"name"`
	} `json:"season,omitempty" bson:"season,omitempty"`
	UpdateTime time.Time `json:"update_time" bson:"update_time"`
}

type CompetitionActive struct {
	GsmID int `json:"gsm_id" bson:"gsm_id"`
	Name  struct {
		GsmName   string `json:"gsm_name" bson:"gsm_name"`
		ShortName string `json:"short_name,omitempty" bson:"short_name,omitempty"`
	} `json:"name" bson:"name"`
}

type FixtureModel struct {
	ID             bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
	BetPredictions *[]struct {
		BetType string `json:"bet_type" bson:"bet_type"`
		Level   string `json:"level" bson:"level"`
		Market  string `json:"market" bson:"market"`
		Value   struct {
			PWin  float64 `json:"p_win" bson:"p_win"`
			Price float64 `json:"price" bson:"price"`
		} `json:"value" bson:"value"`
	} `json:"bet_predictions,omitempty" bson:"bet_predictions,omitempty"`
	Fixture *struct {
		Competition     GsmObject `json:"competition" bson:"competition"`
		KickOffDateTime time.Time `json:"kick_off_date_time" bson:"kick_off_date_time"`
		Season          GsmObject `json:"season" bson:"season"`
		Team1           GsmObject `json:"team1" bson:"team1"`
		Team2           GsmObject `json:"team2" bson:"team2"`
	} `json:"fixture,omitempty" bson:"fixture,omitempty"`
	For *struct {
		GsmID int    `json:"gsm_id" bson:"gsm_id"`
		Type  string `json:"type" bson:"type"`
	} `json:"for,omitempty" bson:"for,omitempty"`
	ModelConfigID    bson.ObjectId `json:"model_config_id,omitempty" bson:"model_config_id,omitempty"`
	ModelRunID       bson.ObjectId `json:"model_run_id,omitempty" bson:"model_run_id,omitempty"`
	OtherPredictions *[]struct {
		Name  string  `json:"name" bson:"name"`
		Value float64 `json:"value" bson:"value"`
	} `json:"other_predictions,omitempty" bson:"other_predictions,omitempty"`
	Predictions *[]struct {
		Name  string  `json:"name" bson:"name"`
		Value float64 `json:"value" bson:"value"`
	} `json:"predictions,omitempty" bson:"predictions,omitempty"`
	UpdateTime time.Time `json:"update_time" bson:"update_time"`
}

type InPlayPrediction struct {
	Id          bson.ObjectId   `json:"_id,omitempty" bson:"_id,omitempty"`
	EventId     int             `json:"event_id,omitempty" bson:"event_id,omitempty"`
	Dt          time.Time       `json:"dt,omitempty" bson:"dt,omitempty"`
	Minute      int             `json:"minute,omitempty" bson:"minute,omitempty"`
	Predictions []NameValuePair `json:"predictions,omitempty" bson:"predictions,omitempty"`
}

type ScoreProbabilities struct {
	EventId            int         `json:"event_id" bson:"event_id"`
	Dt                 time.Time   `json:"dt" bson:"dt"`
	Minute             int         `json:"minute" bson:"minute"`
	LiveProbabilities  [][]float64 `json:"live" bson:"live"`
	TPlusProbabilties  [][]float64 `json:"t_plus" bson:"t_plus"`
	Team1Probabilities [][]float64 `json:"team_1_goal" bson:"team_1_goal"`
	Team2Probabilities [][]float64 `json:"team_2_goal" bson:"team_2_goal"`
}

type NameValuePair struct {
	Name  string  `json:"name" bson:"name"`
	Value float64 `json:"value" bson:"value"`
}

type EmpiricalData struct {
	EventId int `bson:"event_id,omitempty" json:"event_id,omitempty"`
	Values  []struct {
		Prices             map[string]float64            `bson:"prices,omitempty" json:"prices,omitempty"`
		ProbabilitesMatrix map[string]map[string]float64 `bson:"probs_matrix,omitempty" json:"probs_matrix,omitempty"`
	} `bson:"values,omitempty" json:"values,omitempty"`
}

// Empirical data for one season only:
type EmpiricalDataLight struct {
	EventId int                `bson:"event_id,omitempty" json:"event_id,omitempty"`
	Prices  map[string]float64 `bson:"prices,omitempty" json:"prices,omitempty"`
}
