package football

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Area struct represents an area document which is stored in a DB.
type Area struct {
	ID           bson.ObjectId `bson:"_id,omitempty" json:"_id"`
	GsmID        int           `bson:"gsm_id" json:"gsm_id"`
	Name         `bson:"name" json:"name"`
	ParentArea   SharedArea `bson:"parent_area" json:"parent_area"`
	Type         string     `bson:"type,omitempty" json:"type,omitempty"`
	GsmParseTime time.Time  `bson:"gsm_parse_time" json:"gsm_parse_time"`
	UpdateTime   time.Time  `bson:"update_time" json:"update_time"`
}
