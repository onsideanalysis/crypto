package football

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type SeasonTeamRecord struct {
	Drawn        int `bson:"drawn,omitempty" json:"drawn"`
	GoalsAgainst int `bson:"goals_against,omitempty" json:"goals_against"`
	GoalsFor     int `bson:"goals_for,omitempty" json:"goals_for"`
	Lost         int `bson:"lost,omitempty" json:"lost"`
	Played       int `bson:"played,omitempty" json:"played"`
	Points       int `bson:"points,omitempty" json:"points"`
	Rank         int `bson:"rank,omitempty" json:"rank"`
	Won          int `bson:"won,omitempty" json:"won"`
}

type SeasonTeam struct {
	GsmID  int `bson:"gsm_id" json:"gsm_id"`
	OptaID int `bson:"opta_id,omitempty" json:"opta_id,omitempty"`
	Name   *struct {
		GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
	} `bson:"name,omitempty" json:"name,omitempty"`
	Record *struct {
		Away  SeasonTeamRecord `json:"away" bson:"away"`
		Home  SeasonTeamRecord `json:"home" bson:"home"`
		Total SeasonTeamRecord `json:"total" bson:"total"`
		Round GsmObject        `json:"round" bson:"round"`
		Group struct {
			GsmID int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
			Name  struct {
				GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
			} `bson:"name,omitempty" json:"name,omitempty"`
		} `json:"group,omitempty" bson:"group,omitempty"`
	} `json:"record,omitempty" bson:"record,omitempty"`
}

type SeasonRound struct {
	GsmID int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	Name  struct {
		GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
	} `bson:"name,omitempty" json:"name,omitempty"`
	StartDate time.Time `bson:"start_date,omitempty" json:"start_date,omitempty"`
	EndDate   time.Time `bson:"end_date,omitempty" json:"end_date,omitempty"`
}

// Season that struct represents a document stored in the DB
type Season struct {
	ID             bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Area           *GsmObject    `bson:"area,omitempty" json:"area,omitempty"`
	Competition    *GsmObject    `bson:"competition,omitempty" json:"competition,omitempty"`
	Available_Data *struct {
		Cards     bool `bson:"cards" json:"cards"`
		GoalTimes bool `bson:"goal_times" json:"goal_times"`
		LineUps   bool `bson:"line_ups" json:"line_ups"`
	} `bson:"available_data,omitempty" json:"available_data,omitempty"`
	GsmID        int            `bson:"gsm_id" json:"gsm_id"`
	GsmParseTime time.Time      `bson:"gsm_parse_time" json:"gsm_parse_time"`
	Name         SeasonName     `bson:"name,omitempty" json:"name,omitempty"`
	StartDate    time.Time      `bson:"start_date,omitempty" json:"start_date,omitempty"`
	EndDate      time.Time      `bson:"end_date,omitempty" json:"end_date,omitempty"`
	Rounds       *[]SeasonRound `bson:"rounds,omitempty" json:"rounds,omitempty"`
	Teams        *[]SeasonTeam  `bson:"teams,omitempty" json:"teams,omitempty"`
	Fixtures     *[]struct {
		GsmID int `bson:"gsm_id" json:"gsm_id"`
	} `bson:"fixtures,omitempty" json:"fixtures,omitempty"`
	UpdateTime          time.Time `bson:"update_time" json:"update_time"`
	GsmFixtureParseTime time.Time `bson:"gsm_fixture_parse_time" json:"gsm_fixture_parse_time"`
}

type SeasonName struct {
	GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
}

//basic Season Data
type BasicSeason struct {
	ID          bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Name        SeasonName    `bson:"name,omitempty" json:"name,omitempty"`
	GsmID       int           `bson:"gsm_id" json:"gsm_id"`
	Competition GsmObject     `bson:"competition,omitempty" json:"competition,omitempty"`
}

type BasicStatistic struct {
	Name string `bson:"name" json:"name"`
}

type AnalystStatistics struct {
	Chances   []string `bson:"chances" json:"chances"`
	Locations []string `bson:"locations" json:"locations"`
	Name      string   `bson:"name" json:"name"`
	Positions []string `bson:"positions" json:"positions"`
	Types     []string `bson:"types" json:"types"`
}

type AvailableSeasonStatistics struct {
	Analysts *[]AnalystStatistics `bson:"analysts" json:"analysts"`
	Gsm      []BasicStatistic     `bson:"gsm" json:"gsm"`
	Models   []BasicStatistic     `bson:"models" json:"models"`
}
