package football

// SharedPerson represents a shared person used in team lineups, fixtures, referees etc.
type SharedPerson struct {
	GsmID int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	Name  `bson:"name" json:"name"`
}
