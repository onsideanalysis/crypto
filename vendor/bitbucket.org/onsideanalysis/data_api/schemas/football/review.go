package football

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type UnavailablePlayerFixture struct {
	ID    string `bson:"_id,omitempty" json:"_id,omitempty"`
	GsmID int    `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	Team1 struct {
		Name GsmName `bson:"name" json:"name"`
	} `bson:"team1,omitempty" json:"team1,omitempty"`
	Team2 struct {
		Name GsmName `bson:"name,omitempty" json:"name,omitempty"`
	} `bson:"team2,omitempty" json:"team2,omitempty"`
	KickOff FixtureKickOff `bson:"kick_off,omitempty" json:"kick_off,omitempty"`
}

type ReviewTeamOverview struct {
	Attacking           string   `bson:"attacking,omitempty" json:"attacking,omitempty"`
	Defensive           string   `bson:"defensive,omitempty" json:"defensive,omitempty"`
	Strengths           string   `bson:"strengths,omitempty" json:"strengths,omitempty"`
	Weaknesses          string   `bson:"weaknesses,omitempty" json:"weaknesses,omitempty"`
	WeaknessesHeadlines []string `bson:"weaknesses_headlines,omitempty" json:"weaknesses_headlines,omitempty"`
	StrengthsHeadlines  []string `bson:"strengths_headlines,omitempty" json:"strengths_headlines,omitempty"`
	DefensiveHeadlines  []string `bson:"defensive_headlines,omitempty" json:"defensive_headlines,omitempty"`
	AttackingHeadlines  []string `bson:"attacking_headlines,omitempty" json:"attacking_headlines,omitempty"`
}

type ReviewTeam struct {
	GsmId              int                        `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	Headlines          []string                   `bson:"headlines,omitempty" json:"headlines,omitempty"`
	Overview           *ReviewTeamOverview        `bson:"overview,omitempty" json:"overview,omitempty"`
	UnavailablePlayers *[]PreviewReviewTeamPlayer `bson:"unavailablePlayers,omitempty" json:"unavailablePlayers,omitempty"`
	UnusedSubstitutes  *[]PreviewReviewTeamPlayer `bson:"unusedSubstitutes,omitempty" json:"unusedSubstitutes,omitempty"`
	UsedSubstitutes    *[]PreviewReviewTeamPlayer `bson:"usedSubstitutes,omitempty" json:"usedSubstitutes,omitempty"`
	StartingLineup     *[]PreviewReviewTeamPlayer `bson:"startingLineup,omitempty" json:"startingLineup,omitempty"`
	Formation          *TeamFormation             `bson:"formation,omitempty" json:"formation,omitempty"`
	Events             *[]map[string]interface{}  `bson:"events,omitempty" json:"events,omitempty"`
	Name               *GsmName                   `bson:"name,omitempty" json:"name,omitempty"`
	AnalystData        map[string]interface{}     `bson:"analyst_data,omitempty" json:"analyst_data,omitempty"`
}

type Review struct {
	ID          bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	V           int           `bson:"__v,omitempty" json:"__v,omitempty"`
	LastUpdated time.Time     `bson:"last_updated,omitempty" json:"last_updated,omitempty"`
	State       string        `bson:"state,omitempty" json:"state,omitempty"`
	Summary     string        `bson:"summary,omitempty" json:"summary,omitempty"`
	Takeaways   *struct {
		OutcomeTakeaways1x2 string `json:"outcome_takeaways_1_2,omitempty" bson:"outcome_takeaways_1_2,omitempty"`
		OutcomeTakeawaysAH  string `json:"outcome_takeaways_ah,omitempty" bson:"outcome_takeaways_ah,omitempty"`
		OutcomeTakeawaysOU  string `json:"outcome_takeaways_ou,omitempty" bson:"outcome_takeaways_ou,omitempty"`
		Takeaways1x2        string `json:"takeaways_1_2,omitempty" bson:"takeaways_1_2,omitempty"`
		TakeawaysAH         string `json:"takeaways_ah,omitempty" bson:"takeaways_ah,omitempty"`
		TakeawaysOU         string `json:"takeaways_ou,omitempty" bson:"takeaways_ou,omitempty"`
	} `json:"takeaways,omitempty" bson:"takeaways,omitempty"`
	Version int `bson:"version,omitempty" json:"version,omitempty"`
	Data    *struct {
		Team1 []map[string]interface{} `json:"team1,omitempty" bson:"team1,omitempty"`
		Team2 []map[string]interface{} `json:"team2,omitempty" bson:"team2,omitempty"`
	} `json:"data,omitempty" bson:"data,omitempty"`
	Activity   []PreviewReviewActivity `json:"activity,omitempty" bson:"activity,omitempty"`
	Team1      *ReviewTeam             `json:"team1,omitempty" bson:"team1,omitempty"`
	Team2      *ReviewTeam             `json:"team2,omitempty" bson:"team2,omitempty"`
	Conditions *struct {
		Pitch                string `bson:"pitch,omitempty" json:"pitch,omitempty"`
		Tfairscoreemperature int    `bson:"temperature,omitempty" json:"temperature,omitempty"`
		Travel               string `bson:"travel,omitempty" json:"travel,omitempty"`
		TravelDistance       string `bson:"travel_distance,omitempty" json:"travel_distance,omitempty"`
		Weather              string `bson:"weather,omitempty" json:"weather,omitempty"`
	} `json:"conditions,omitempty" bson:"conditions,omitempty"`
	Fixture   *PreviewReviewFixture `json:"fixture,omitempty" bson:"fixture,omitempty"`
	FairScore *struct {
		Team1 int `bson:"team1" json:"team1"`
		Team2 int `bson:"team2" json:"team2"`
	} `bson:"fairScore,omitempty" json:"fairScore,omitempty"`
}

type LineupPlayer struct {
	Rating      int    `bson:"rating,omitempty" json:"rating,omitempty"`
	GsmId       int    `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	ShirtNumber int    `bson:"shirt_number,omitempty" json:"shirt_number,omitempty"`
	StatplatId  string `bson:"statplat_id,omitempty" json:"statplat_id,omitempty"`
	Analysis    string `bson:"analysis,omitempty" json:"analysis,omitempty"`
	Id          string `bson:"_id,omitempty" json:"_id,omitempty"`
	StartTime   *struct {
		Min int `bson:"min,omitempty" json:"min,omitempty"`
	} `bson:"start_time,omitempty" json:"start_time,omitempty"`
	Positions *[]string `bson:"positions,omitempty" json:"positions,omitempty"`
	Name      *struct {
		GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
	} `bson:"name,omitempty" json:"name,omitempty"`
}

type ReviewResult struct {
	Review    *Review `bson:"review,omitempty" json:"review,omitempty"`
	LastTeam1 *struct {
		StartingLineup []LineupPlayer `bson:"startingLineup,omitempty" json:"startingLineup,omitempty"`
		GsmId          int            `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	} `bson:"lastTeam1,omitempty" json:"lastTeam1,omitempty"`
	LastTeam2 *struct {
		StartingLineup []LineupPlayer `bson:"startingLineup,omitempty" json:"startingLineup,omitempty"`
		GsmId          int            `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	} `bson:"lastTeam2,omitempty" json:"lastTeam2,omitempty"`
	LastMatchesByUnavailables *map[int]UnavailablePlayerFixture `bson:"lastMatchesByUnavailables,omitempty" json:"lastMatchesByUnavailables,omitempty"`
}
