package football

import (
	"time"

	"bitbucket.org/onsideanalysis/data_api/schemas/stickers"
)

type TeamPerformance struct {
	Outcome     float64            `bson:"outcome" json:"outcome"`
	FairOutcome float64            `bson:"fair_outcome" json:"fair_outcome"`
	Odd         float64            `bson:"odd" json:"odd"`
	Market      string             `bson:"market" json:"market"`
	Selection   stickers.Selection `bson:"selection" json:"selection"`
	Handicap    float64            `bson:"handicap" json:"handicap"`
	IsNatural   bool               `bson:"is_natural" json:"is_natural"`
	Text        string             `bson:"text" json:"text"`
	Score       []int              `bson:"score" json:"score"`
	FairScore   []int              `bson:"fair_score" json:"fair_score"`
	Date        time.Time          `bson:"date" json:"date"`
	Team1Id     int                `bson:"team_1_id" json:"team_1_id"`
	Team2Id     int                `bson:"team_2_id" json:"team_2_id"`
	SeasonId    int                `bson:"season_id" json:"season_id"`
	FixtureId   int                `bson:"fixture_id" json:"fixture_id"`
}

type TeamPerformanceResult struct {
	AHHandicap  []float64                    `json:"ahHandicap"`
	OUHandicap  []float64                    `json:"ouHandicap"`
	Performance TeamPerformanceMarketsSeries `json:"performance"`
}

type TeamPerformanceMarketsSeries map[string]TeamPerformanceGroupedByHandicap

type TeamPerformanceGroupedByHandicap map[string]TeamPerformanceGrouped

// Team performance series of 10 matches for a fixture
type TeamPerformanceGrouped struct {
	Actual TeamPerformancePlaceSeries `json:"actual"`
	Fair   TeamPerformancePlaceSeries `json:"fair"`
}

type TeamPerformancePlaceSeries struct {
	Away    TeamPerformancePlace `json:"away"`
	Home    TeamPerformancePlace `json:"home"`
	Overall TeamPerformancePlace `json:"overall"`
}

type TeamPerformancePlace struct {
	Data   TeamPerformanceResultCount   `json:"data"`
	Series TeamPerformanceGroupedSeries `json:"series"`
}

type TeamPerformanceResultCount struct {
	Draw int `json:"draw,omitempty"`
	Win  int `json:"win"`
	Loss int `json:"loss"`
}

type TeamPerformanceGroupedSeries struct {
	Draw TeamPerformanceSerie `json:"draw,omitempty"`
	Win  TeamPerformanceSerie `json:"win"`
	Loss TeamPerformanceSerie `json:"loss"`
}

type TeamPerformanceSerie struct {
	Name  string                   `json:"name"`
	Serie []TeamPerformanceSummary `json:"serie"`
}

type TeamPerformanceSummary struct {
	FixtureId   int                `json:"fixtureId"`
	Outcome     float64            `json:"outcome"`
	Name        int                `json:"name"`
	Value       float64            `json:"value"`
	Vs          string             `json:"vs"`
	IsHome      bool               `json:"isHome"`
	ActualScore []int              `json:"actualScore"`
	FairScore   []int              `json:"fairScore"`
	Selection   stickers.Selection `json:"selection"`
	Odd         float64            `json:"odd"`
	IsNatural   bool               `json:"isNatural"`
	Handicap    float64            `json:"handicap"`
	KickOff     time.Time          `json:"kickoff"`
}
