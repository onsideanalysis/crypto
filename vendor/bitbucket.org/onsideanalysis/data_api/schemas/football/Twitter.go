package football

import (
	"gopkg.in/mgo.v2/bson"
)

// Twitter struct represents an Twitter document which is stored in a DB.
type Twitter struct {
	ID                      bson.ObjectId `bson:"_id,omitempty" json:"_id"`
	Team                    *TwitterTeam  `bson:"team,omitempty" json:"team,omitempty"`
	TwitterOfficial         string        `bson:"twitter_official,omitempty" json:"twitter_official,omitempty"`
	LocalJournalistOfficial string        `bson:"local_journalist_official,omitempty" json:"local_journalist_official,omitempty"`
	ClubExpertTwitter1      string        `bson:"club_expert_twitter1,omitempty" json:"club_expert_twitter1,omitempty"`
	ClubExpertTwitter2      string        `bson:"club_expert_twitter2,omitempty" json:"club_expert_twitter2,omitempty"`
}

type TwitterTeam struct {
	GsmID int    `bson:"gsm_id" json:"gsm_id"`
	Name  string `bson:"name,omitempty" json:"name,omitempty"`
}
