package football

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type PlayerTeam struct {
	StartDate time.Time `bson:"start_date,omitempty" json:"start_date,omitempty"`
	EndDate   time.Time `bson:"end_date,omitempty" json:"end_date,omitempty"`
	Role      string    `bson:"role,omitempty" json:"role,omitempty"`
	Name      *struct {
		GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
	} `bson:"name,omitempty" json:"name,omitempty"`
	GsmID int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
}

type Record struct {
	Role   string `bson:"role,omitempty" json:"role,omitempty"`
	Season *struct {
		Name *struct {
			GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
		} `bson:"name,omitempty" json:"name,omitempty"`
		GsmID int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	} `bson:"season,omitempty" json:"season,omitempty"`
	Team *struct {
		Name *struct {
			GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
		} `bson:"name,omitempty" json:"name,omitempty"`
		GsmID int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	} `bson:"team,omitempty" json:"team,omitempty"`
	Statistics *struct {
		Gsm *struct {
			Cards *struct {
				Red    int `bson:"red,omitempty" json:"red,omitempty"`
				Yellow int `bson:"yellow,omitempty" json:"yellow,omitempty"`
			} `bson:"cards,omitempty" json:"cards,omitempty"`
			Appearances *struct {
				SubOn      int `bson:"sub_on,omitempty" json:"sub_on,omitempty"`
				Starts     int `bson:"starts,omitempty" json:"starts,omitempty"`
				MinsPlayed int `bson:"mins_played,omitempty" json:"mins_played,omitempty"`
			} `bson:"appearances,omitempty" json:"appearances,omitempty"`
			Goals   int `bson:"goals,omitempty" json:"goals,omitempty"`
			Assists int `bson:"assists,omitempty" json:"assists,omitempty"`
		} `bson:"gsm,omitempty" json:"gsm,omitempty"`
	} `bson:"statistics,omitempty" json:"statistics,omitempty"`
}

type Person struct {
	ID    bson.ObjectId `bson:"_id" json:"_id"`
	GsmID int           `bson:"gsm_id" json:"gsm_id"`
	Birth *struct {
		Date  time.Time `bson:"date,omitempty" json:"date,omitempty"`
		Place string    `bson:"place,omitempty" json:"place,omitempty"`
	} `bson:"birth,omitempty" json:"birth,omitempty"`
	Name *struct {
		GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
	} `bson:"name,omitempty" json:"name,omitempty"`
	PlayerDetails *struct {
		Opta *struct {
			Height int `bson:"height,omitempty" json:"height,omitempty"`
			Weight int `bson:"weight,omitempty" json:"weight,omitempty"`
		} `bson:"opta,omitempty" json:"opta,omitempty"`
		Height int `bson:"height,omitempty" json:"height,omitempty"`
		Weight int `bson:"weight,omitempty" json:"weight,omitempty"`
	} `bson:"player_details,omitempty" json:"player_details,omitempty"`
	CurrentTeam string `bson:"current_team,omitempty" json:"current_team,omitempty"`
	//do not send these fields to Stratabet
	Team *struct {
		Name *struct {
			OptaName string `bson:"opta_name,omitempty" json:"opta_name,omitempty"`
		} `bson:"name,omitempty" json:"name,omitempty"`
	} `bson:"team,omitempty" json:"team,omitempty"`
	Teams       []PlayerTeam `bson:"teams,omitempty" json:"teams,omitempty"`
	Nationality *struct {
		Area *struct {
			GsmID int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
			Name  *struct {
				GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
			} `bson:"name,omitempty" json:"name,omitempty"`
		} `bson:"area,omitempty" json:"area,omitempty"`
	} `bson:"nationality,omitempty" json:"nationality,omitempty"`
	Record []Record `bson:"record,omitempty" json:"record,omitempty"`
}
