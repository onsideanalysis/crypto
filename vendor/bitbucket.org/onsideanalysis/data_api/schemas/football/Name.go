package football

// Name struct is a name of player, team, season etc.
type Name struct {
	GsmName   string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
	ShortName string `bson:"short_name,omitempty" json:"short_name,omitempty"`
	OptaCode  string `bson:"opta_code,omitempty" json:"opta_code,omitempty"`
	FDName    string `bson:"fd_name,omitempty" json:"fd_name,omitempty"`
	GsmAbbr   string `bson:"gsm_abbr,omitempty" json:"gsm_abbr,omitempty"`
}
