package football

import "time"

/*
Example of a simple time-series query: (Arsenal Attacks, Corners and Posession(!) in the last season, divided by week)

{
"_id": 1,
"filter": {"season_ids": [8318], "team_ids" : ["team_ids": 660]},
"aggregation": {"time": "week"},
"features": ["Attacks", "Corners", "Posession"]  //yes, I know there is a spelling mistake, but it's in the database too :)
}

*/

type FixtureEventQueryInputData struct {
	Id          int             `bson:"_id" json:"_id"`
	Filter      FilterData      `bson:"filter" json:"filter"`
	Aggregation AggregationData `bson:"aggregation" json:"aggregation"`
	Features    []string        `bson:"features" json:"features"`
	Type        string          `bson:"type" json:"type"`
	SubTypes    []string        `bson:"subtypes" json:"subtypes"`
	SubType     string          `bson:"subtype,omitempty" json:"subtype,omitempty"`
}

type FilterData struct {
	FixtureIds         []int        `bson:"fixture_ids" json:"fixture_ids"`                 // restrict to specific fixtures
	CompetitionIds     []int        `bson:"competition_ids" json:"competition_ids"`         // restrict to the competition listed. If emty, do not restrict
	SeasonIds          []int        `bson:"season_ids" json:"season_ids"`                   // restrict to the seasons listed. If emty, do not restrict
	TimeWindow         [2]time.Time `bson:"time_window" json:"time_window"`                 //[timedate, timedate]
	OddsTimeWindow     [2]int       `bson:"odds_time_window" json:"odds_time_window"`       //[int, int]
	Location           string       `bson:"location" json:"location"`                       // "home", "away", "all"
	OppositionTeams    []int        `bson:"opposition_teams" json:"opposition_teams"`       // teams ids
	OppositionManagers []int        `bson:"opposition_managers" json:"opposition_managers"` // teams ids
	PlayersNotOnPitch  []int        `bson:"players_not_playing" json:"players_not_playing"` // teams ids
	Managers           []int        `bson:"managers" json:"managers"`                       // teams ids
	PlayersOnPitch     []int        `bson:"players" json:"players"`                         // teams ids
	BookmakerIds       []int        `bson:"bookmaker_ids" json:"bookmaker_ids"`             // teams ids

}

type AggregationData struct {
	Times            []string `bson:"times" json:"times"` // can be any of these: "round", "week", "month", "season", "year"
	Functions        []string `bson:"functions" json:"functions"`
	Type             string   `bson:"type" json:"type"`
	AsCrossSectional bool     `bson:"as_cross_sectional" json:"as_cross_sectional"`
	TeamIds          []int    `bson:"team_ids" json:"team_ids"`
	RoundNo          int      `bson:"roundNo,omitempty" json:"roundNo,omitempty"`
}

/*
Example output data structure:

[{
"_id": 1,
"feature": "Attacks",
"time_agg": 1,
"value_for": 15,
"value_against": 12,
"value_total": 27;
},
{
"_id": 1,
"feature": "Corners",
"time_agg": 1,
"value_for": 5,
"value_against": 6,
"value_total": 11,
},
{...},
{...},
]



*/

type FilteredFixturesData struct {
	FixtureIds   []int       `bson:"fixture_ids" json:"fixture_ids"`
	Dates        []time.Time `bson:"dates" json:"dates"`
	HomeFixtures []int       `bson:"home_fixtures" json:"home_fixtures"`
	AwayFixtures []int       `bson:"away_fixtures" json:"away_fixtures"`
}

//map[Feature:Attacks TimeAgg:1 ValueFor:66 ValueAgainst:45 ValueTotal:111]
//{Id:0 Feature: TimeAgg:0 TeamIds:[] CompetitionIds:[] SeasonIds:[] ValueFor:0 ValueAgainst:0 ValueTotal:0}
type FixtureEventQueryOutputData struct {
	Id         int                          //`bson:_id json:"_id"`
	Events     []string                     `bson:"events" json:"events"`
	TeamIds    []int                        //`bson:team_ids json:"team_ids"`
	Games      [][]int                      `bson:"games" json:"games"`
	Dates      [][]time.Time                `bson:"dates" json:"dates"`
	Values     []float64                    `bson:"values" json:"values"`                               // average value of the Feature
	MarketType int                          `bson:"market_type,omitempty" json:"market_type,omitempty"` // average value of the Feature
	Selection  []FixtureEventSelectionValue `bson:"selection_value,omitempty" json:"selection_value,omitempty"`
	NameAbbr   string                       `bson:"gsm_abbr,omitempty" json:"gsm_abbr,omitempty"`
}

type FixtureEventSelectionValue struct {
	Value   int `bson:"value" json:"value"`
	SelType int `bson:"type" json:"type"`
}
