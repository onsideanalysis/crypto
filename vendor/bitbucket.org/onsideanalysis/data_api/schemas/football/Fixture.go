package football

import (
	"time"

	"bitbucket.org/onsideanalysis/data_api/schemas/bookmakers"
	"bitbucket.org/onsideanalysis/data_api/schemas/stickers"
	"gopkg.in/mgo.v2/bson"
)

type GsmName struct {
	GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
}

type GsmAllNames struct {
	GsmName      string `bson:"gsm_name" json:"gsm_name"`
	GsmShortName string `bson:"gsm_short_name" json:"gsm_short_name"`
	GsmAbbr      string `bson:"gsm_abbr,omitempty" json:"gsm_abbr,omitempty"`
}

type GsmBasicNames struct {
	GsmName   string `bson:"gsm_name" json:"gsm_name"`
	ShortName string `bson:"short_name" json:"short_name"`
}

type GsmObject struct {
	GsmID int     `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	Name  GsmName `bson:"name,omitempty" json:"name,omitempty"`
}

type FixtureCard struct {
	GsmID int    `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	Type  string `bson:"type,omitempty" json:"type,omitempty"`
	Time  *struct {
		Min      int `bson:"min,omitempty" json:"min,omitempty"`
		AddedMin int `bson:"added_min,omitempty" json:"added_min,omitempty"`
	} `bson:"time,omitempty" json:"time,omitempty"`
	Person GsmObject `bson:"person,omitempty" json:"person,omitempty"`
}

type FixtureGoal struct {
	GsmID int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	Time  struct {
		Min      int `bson:"min,omitempty" json:"min,omitempty"`
		AddedMin int `bson:"added_min,omitempty" json:"added_min,omitempty"`
	} `bson:"time,omitempty" json:"time,omitempty"`
	Type   map[string]interface{} `bson:"type,omitempty" json:"type,omitempty"`
	Scorer GsmObject              `bson:"scorer,omitempty" json:"scorer,omitempty"`
}

type FixtureResult struct {
	GsmID   int              `bson:"gsm_id" json:"gsm_id"`
	KickOff *FixtureKickOff  `bson:"kick_off,omitempty" json:"kick_off,omitempty"`
	Score   *FixtureAllScore `bson:"score" json:"score"`
	Data    *struct {
		Statistics struct {
			Gsm []struct {
				Type  string      `bson:"type" json:"type"`
				Value interface{} `bson:"value" json:"value"`
			} `bson:"gsm" json:"gsm"`
			Analysts *AnalystsData `bson:"analysts,omitempty" json:"analysts,omitempty"`
		} `bson:"statistics" json:"statistics"`
	} `bson:"data,omitempty" json:"data,omitempty"`
}

type FixtureLineupPlayer struct {
	GsmID          int       `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	Person         GsmObject `bson:"person,omitempty" json:"person,omitempty"`
	StartingStatus string    `bson:"starting_status,omitempty" json:"starting_status,omitempty"`
	StartTime      *struct {
		Min int `bson:"min,omitempty" json:"min,omitempty"`
	} `bson:"start_time,omitempty" json:"start_time,omitempty"`
	EndReason string `bson:"end_reason,omitempty" json:"end_reason,omitempty"`
	EndTime   *struct {
		Min int `bson:"min,omitempty" json:"min,omitempty"`
	} `bson:"end_time,omitempty" json:"end_time,omitempty"`
}

type FixtureTeamScore struct {
	FullTimeScore        int `bson:"full_time_score" json:"full_time_score"`
	HalfTimeScore        int `bson:"half_time_score" json:"half_time_score"`
	ExtraTimeScore       int `bson:"extra_time_score,omitempty" json:"extra_time_score,omitempty"`
	PenaltyShootoutScore int `bson:"penalty_shootout_score,omitempty" json:"penalty_shootout_score,omitempty"`
}

type FixtureOdds struct {
	KickOff            map[bookmakers.Bookmaker]map[stickers.Sticker]float64 `bson:"kick_off" json:"kick_off"`
	TwoHoursPreKickOff map[bookmakers.Bookmaker]map[stickers.Sticker]float64 `bson:"2hrs_pre_kick_off" json:"2hrs_pre_kick_off"`
}

// Fixture struct representing a fixture document.
type Fixture struct {
	ID    bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Area  *GsmObject    `bson:"area,omitempty" json:"area,omitempty"`
	Cards *struct {
		Gsm struct {
			Team1 []FixtureCard `bson:"team1,omitempty" json:"team1,omitempty"`
			Team2 []FixtureCard `bson:"team2,omitempty" json:"team2,omitempty"`
		} `bson:"gsm" json:"gsm"`
	} `bson:"cards,omitempty" json:"cards,omitempty"`
	Coaches *struct {
		Gsm struct {
			Team1 GsmObject `bson:"team1" json:"team1"`
			Team2 GsmObject `bson:"team2" json:"team2"`
		} `bson:"gsm" json:"gsm"`
	} `bson:"coaches,omitempty" json:"coaches,omitempty"`
	Competition *FixtureCompetition `bson:"competition,omitempty" json:"competition,omitempty"`
	Data        *struct {
		Statistics struct {
			Gsm []struct {
				Type  string      `bson:"type" json:"type"`
				Value interface{} `bson:"value" json:"value"`
			} `bson:"gsm" json:"gsm"`
			Analysts *AnalystsData `bson:"analysts,omitempty" json:"analysts,omitempty"`
		} `bson:"statistics" json:"statistics"`
	} `bson:"data,omitempty" json:"data,omitempty"`
	Goals *struct {
		Gsm struct {
			Team1 []FixtureGoal `bson:"team1" json:"team1"`
			Team2 []FixtureGoal `bson:"team2" json:"team2"`
		} `bson:"gsm" json:"gsm"`
	} `bson:"goals,omitempty" json:"goals,omitempty"`
	GsmID        int             `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	GsmParseTime *time.Time      `bson:"gsm_parse_time,omitempty" json:"gsm_parse_time,omitempty"`
	KickOff      *FixtureKickOff `bson:"kick_off,omitempty" json:"kick_off,omitempty"`
	LineUps      *struct {
		Gsm struct {
			Team1 []FixtureLineupPlayer `bson:"team1" json:"team1"`
			Team2 []FixtureLineupPlayer `bson:"team2" json:"team2"`
		} `bson:"gsm" json:"gsm"`
	} `bson:"line_ups,omitempty" json:"line_ups,omitempty"`
	Odds      *FixtureOdds `bson:"odds,omitempty" json:"odds,omitempty"`
	Officials *struct {
		Gsm []struct {
			Role   string    `bson:"role" json:"role"`
			Person GsmObject `bson:"person" json:"person"`
		} `bson:"gsm" json:"gsm"`
	} `bson:"officials,omitempty" json:"officials,omitempty"`
	OptaID      int                  `bson:"opta_id,omitempty" json:"opta_id,omitempty"`
	Predictions *[]FixturePrediction `bson:"predictions,omitempty" json:"predictions,omitempty"`
	Round       *GsmObject           `bson:"round,omitempty" json:"round,omitempty"`
	Score       *FixtureAllScore     `bson:"score,omitempty" json:"score,omitempty"`
	Season      *GsmObject           `bson:"season,omitempty" json:"season,omitempty"`
	Team1       *FixtureTeam         `bson:"team1,omitempty" json:"team1,omitempty"`
	Team2       *FixtureTeam         `bson:"team2,omitempty" json:"team2,omitempty"`
	Type        *FixtureType         `bson:"type,omitempty" json:"type,omitempty"`
	UpdateTime  string               `bson:"update_time,omitempty" json:"update_time,omitempty"`
	Venue       *FixtureVenue        `bson:"venue,omitempty" json:"venue,omitempty"`
}

// Fixture kick off date and score - required by getHistoricalPrices
type FixtureKickOffAndScore struct {
	GsmID   int              `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	KickOff *FixtureKickOff  `bson:"kick_off,omitempty" json:"kick_off,omitempty"`
	Score   *FixtureAllScore `bson:"score,omitempty" json:"score,omitempty"`
}

type AnalystsData struct {
	Player struct {
		Team1 []TeamStatisticsPlayer `bson:"team1" json:"team1"`
		Team2 []TeamStatisticsPlayer `bson:"team2" json:"team2"`
	} `bson:"player" json:"player"`
	Team struct {
		Team1 TeamStatisticsTeam `bson:"team1" json:"team1"`
		Team2 TeamStatisticsTeam `bson:"team2" json:"team2"`
	} `bson:"team" json:"team"`
}

type AnalystEvents struct {
	Events []AnalystEvent `bson:"events,omitempty" json:"events,omitempty"`
}

type AnalystEvent struct {
	CurrentHalf string                 `bson:"currentHalf,omitempty" json:"currentHalf,omitempty"`
	Category    string                 `bson:"category,omitempty" json:"category,omitempty"`
	Type        string                 `bson:"type,omitempty" json:"type,omitempty"`
	Description string                 `bson:"description,omitempty" json:"description,omitempty"`
	Team        string                 `bson:"team,omitempty" json:"team,omitempty"`
	Details     map[string]interface{} `bson:"details,omitempty" json:"details,omitempty"`
	Player      map[string]interface{} `bson:"player1,omitempty" json:"player1,omitempty"`
	PlayerOn    map[string]interface{} `bson:"player2,omitempty" json:"player2,omitempty"`
	Hour        int64                  `bson:"hour,omitempty" json:"hour,omitempty"`
	ClockTime   string                 `bson:"clockTime,omitempty" json:"clockTime,omitempty"`
	FairScore   map[string]interface{} `bson:"fairScore,omitempty" json:"fairScore,omitempty"`
}

type KeyEventsData struct {
	Classifiers bson.M `json:"classifiers,omitempty" bson:"classifiers,omitempty"`
	Type        string `json:"type" bson:"type"`
	Value       int    `json:"value" bson:"value"`
}
type TeamStatisticsPlayer struct {
	FirstHalf  []KeyEventsData `bson:"first_half,omitempty" json:"first_half,omitempty"`
	FullTime   []KeyEventsData `bson:"full_time" json:"full_time"`
	SecondHalf []KeyEventsData `bson:"second_half,omitempty" json:"second_half,omitempty"`
	GsmID      int             `bson:"gsm_id" json:"gsm_id"`
}
type TeamStatisticsTeam struct {
	FirstHalf  []KeyEventsData `bson:"first_half,omitempty" json:"first_half,omitempty"`
	FullTime   []KeyEventsData `bson:"full_time" json:"full_time"`
	SecondHalf []KeyEventsData `bson:"second_half,omitempty" json:"second_half,omitempty"`
}

type EventMapping struct {
	GsmID  int           `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	Name   string        `bson:"name,omitempty" json:"name,omitempty"`
	ToName string        `bson:"to_name,omitempty" json:"to_name,omitempty"`
	ID     bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
}

//abbreviated fixture object with just competition, gsm_id, kick_off, team1, team2, prediction, goals, cards and score
type FixtureLite struct {
	ID          bson.ObjectId       `bson:"_id,omitempty" json:"_id,omitempty"`
	Area        *GsmObject          `bson:"area,omitempty" json:"area,omitempty"`
	Competition *FixtureCompetition `bson:"competition,omitempty" json:"competition,omitempty"`
	Season      *GsmObject          `bson:"season,omitempty" json:"season,omitempty"`
	GsmID       int                 `bson:"gsm_id" json:"gsm_id"`
	KickOff     *FixtureKickOff     `bson:"kick_off,omitempty" json:"kick_off,omitempty"`
	Team1       *FixtureTeam        `bson:"team1,omitempty" json:"team1,omitempty"`
	Team2       *FixtureTeam        `bson:"team2,omitempty" json:"team2,omitempty"`
	Type        *struct {
		Format string `bson:"format" json:"format"`
		Status string `bson:"status" json:"status"`
	} `bson:"type,omitempty" json:"type,omitempty"`
	Predictions []FixturePrediction `bson:"predictions,omitempty" json:"predictions,omitempty"`
	Goals       *struct {
		Gsm struct {
			Team1 []FixtureGoal `bson:"team1" json:"team1"`
			Team2 []FixtureGoal `bson:"team2" json:"team2"`
		} `bson:"gsm" json:"gsm"`
	} `bson:"goals,omitempty" json:"goals,omitempty"`
	Cards *struct {
		Gsm struct {
			Team1 []FixtureCard `bson:"team1,omitempty" json:"team1,omitempty"`
			Team2 []FixtureCard `bson:"team2,omitempty" json:"team2,omitempty"`
		} `bson:"gsm" json:"gsm"`
	} `bson:"cards,omitempty" json:"cards,omitempty"`
	Score *FixtureAllScore `bson:"score,omitempty" json:"score,omitempty"`
	Odds  *FixtureOdds     `bson:"odds,omitempty" json:"odds,omitempty"`
}

// FixtureSummary contains basic probablity, teams, area, season and competition.
type FixtureSummary struct {
	ID          bson.ObjectId       `bson:"_id,omitempty" json:"_id,omitempty"`
	GsmID       int                 `bson:"gsm_id" json:"gsm_id"`
	Competition *FixtureCompetition `bson:"competition,omitempty" json:"competition,omitempty"`
	Area        *GsmObject          `bson:"area,omitempty" json:"area,omitempty"`
	KickOff     *FixtureKickOff     `bson:"kick_off,omitempty" json:"kick_off,omitempty"`
	Team1       *FixtureTeam        `bson:"team1,omitempty" json:"team1,omitempty"`
	Team2       *FixtureTeam        `bson:"team2,omitempty" json:"team2,omitempty"`
	Season      *GsmObject          `bson:"season,omitempty" json:"season,omitempty"`
	Round       *GsmObject          `bson:"round,omitempty" json:"round,omitempty"`
	Group       *GsmObject          `bson:"group,omitempty" json:"group,omitempty"`
	Predictions []FixturePrediction `bson:"predictions,omitempty" json:"predictions,omitempty"`
	Type        *FixtureType        `bson:"type,omitempty" json:"type,omitempty"`
	Score       *FixtureAllScore    `bson:"score,omitempty" json:"score,omitempty"`
	Odds        *FixtureOdds        `bson:"odds,omitempty" json:"odds,omitempty"`
	//Created field
	Form *FixtureForm `bson:"form,omitempty" json:"form,omitempty"`
}

type FixtureSummarySlice []FixtureSummary

func (slice FixtureSummarySlice) Len() int {
	return len(slice)
}

//Sort by kick off time then analyst competition priority
func (slice FixtureSummarySlice) Less(i, j int) bool {
	if slice[i].KickOff.Utc.Gsm.DateTime.Equal(slice[j].KickOff.Utc.Gsm.DateTime) {
		if slice[i].Competition.GsmID != slice[j].Competition.GsmID {
			for _, compId := range ANALYSIS_COMP_IDS {
				if compId == slice[i].Competition.GsmID {
					return true
				} else if compId == slice[j].Competition.GsmID {
					return false
				}
			}
		}
		return false
	}
	return slice[i].KickOff.Utc.Gsm.DateTime.Before(slice[j].KickOff.Utc.Gsm.DateTime)
}

func (slice FixtureSummarySlice) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

type FixturePrediction struct {
	TotalGoals float64              `bson:"tg,omitempty" json:"tg,omitempty"`
	Supremacy  float64              `bson:"sup,omitempty" json:"sup,omitempty"`
	ModelRunID bson.ObjectId        `bson:"model_run_id,omitempty" json:"model_run_id,omitempty"`
	Model      string               `bson:"model,omitempty" json:"model,omitempty"`
	All        []FixtureModelResult `bson:"all,omitempty" json:"all,omitempty"`
}

type FixtureType struct {
	Aggregate *struct {
		Leg           int `bson:"leg" json:"leg"`
		PairedFixture struct {
			GsmId int `bson:"gsm_id" json:"gsm_id"`
		} `bson:"paired_fixture" json:"paired_fixture"`
	} `bson:"aggregate,omitempty" json:"aggregate,omitempty"`
	Format string `bson:"format" json:"format"`
	Status string `bson:"status" json:"status"`
}

type FixtureModelResult struct {
	Name  string  `bson:"name,omitempty" json:"name,omitempty"`
	Value float64 `bson:"value,omitempty" json:"value,omitempty"`
}

type FixtureCompetition struct {
	GsmID int           `bson:"gsm_id" json:"gsm_id"`
	Name  GsmBasicNames `bson:"name" json:"name"`
}

type FixtureTeam struct {
	GsmID  int `bson:"gsm_id" json:"gsm_id"`
	OptaId int `bson:"opta_id,omitempty" json:"opta_id,omitempty"`
	Area   *struct {
		GsmAbbr string `bson:"gsm_abbr" json:"gsm_abbr"`
	} `bson:"area,omitempty" json:"area,omitempty"`
	Name GsmAllNames `bson:"name" json:"name"`
}

type FixtureTime struct {
	DateTime time.Time `bson:"date_time" json:"date_time"`
}

type GsmTime struct {
	Gsm FixtureTime `bson:"gsm" json:"gsm"`
}
type FixtureKickOff struct {
	FixtureWeek int     `bson:"fixture_week,omitempty" json:"fixture_week,omitempty"`
	Utc         GsmTime `bson:"utc" json:"utc"`
}

type FixtureScore struct {
	Gsm struct {
		Team1 struct {
			FullTimeScore int `bson:"full_time_score" json:"full_time_score"`
			HalfTimeScore int `bson:"half_time_score" json:"half_time_score"`
		} `bson:"team1" json:"team1"`
		Team2 struct {
			FullTimeScore int `bson:"full_time_score" json:"full_time_score"`
			HalfTimeScore int `bson:"half_time_score" json:"half_time_score"`
		} `bson:"team2" json:"team2"`
	} `bson:"gsm" json:"gsm"`
}

type FixtureAllScore struct {
	Gsm struct {
		Team1 FixtureTeamScore `bson:"team1" json:"team1"`
		Team2 FixtureTeamScore `bson:"team2" json:"team2"`
	} `bson:"gsm" json:"gsm"`
}

type FixtureVenue struct {
	Capacity  int     `bson:"capacity" json:"capacity"`
	Distance  float32 `bson:"distance" json:"distance"`
	GsmId     int     `bson:"gsm_id" json:"gsm_id"`
	PitchType string  `bson:"pitch_type" json:"pitch_type"`
	Name      struct {
		GsmName string `bson:"gsm_name" json:"gsm_name"`
	} `bson:"name" json:"name"`
	Area struct {
		GsmID int `bson:"gsm_id" json:"gsm_id"`
	} `bson:"area" json:"area"`
}

type FixtureConfidence struct {
	ID         bson.ObjectId `bson:"_id,omitempty" json:"_id"`
	GsmID      int           `bson:"gsm_id" json:"gsm_id"`
	Confidence string        `bson:"confidence" json:"confidence"`
	Dt         time.Time     `bson:"dt" json:"dt"`
}

type FixtureConfidenceExpanded struct {
	FixtureConfidenceInfo FixtureConfidence
	FixtureInfo           Fixture
}

type SeasonResults struct {
	SeasonId int `bson:"_id" json:"_id"`
	Data     []struct {
		Team1    int       `bson:"team1" json:"team1"`
		Team2    int       `bson:"team2" json:"team2"`
		Desc     string    `bson:"desc" json:"desc"`
		KickOff  time.Time `bson:"kick_off" json:"kick_off"`
		Outcome1 string    `bson:"outcome1" json:"outcome1"`
		Outcome2 string    `bson:"outcome2" json:"outcome2"`
		GsmId    int       `bson:"gsm_id" json:"gsm_id"`
	} `bson:"data" json:"data"`
}

type FixtureForm struct {
	Team1 []TeamForm `json:"team1"`
	Team2 []TeamForm `json:"team2"`
}

type TeamForm struct {
	GsmId   int       `bson:"gsm_id" json:"gsm_id"`
	KickOff time.Time `bson:"kick_off" json:"kick_off"`
	Desc    string    `bson:"desc" json:"desc"`
	Outcome string    `bson:"outcome" json:"outcome"`
}

type EmpiricalState struct {
	NewGoalsTeam1 int     `bson:"new_goals_team1" json:"new_goals_team1"`
	NewGoalsTeam2 int     `bson:"new_goals_team2" json:"new_goals_team2"`
	Count         int     `bson:"count" json:"count"`
	Percentage    float64 `bson:"percentage" json:"percentage"`
}

type LineResults struct {
	TeamId  int     `bson:"team_id" json:"team_id"`
	CompId  int     `bson:"comp_id" json:"comp_id"`
	NoOver  float64 `bson:"no_over" json:"no_over"`
	NoUnder float64 `bson:"no_under" json:"no_under"`
	NoBeats float64 `bson:"no_beats" json:"no_beats"`
	NoLoses float64 `bson:"no_loses" json:"no_loses"`
	Total   float64 `bson:"total" json:"total"`
}
