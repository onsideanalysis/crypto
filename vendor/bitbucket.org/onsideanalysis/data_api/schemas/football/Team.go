package football

import "gopkg.in/mgo.v2/bson"

type TeamCoach struct {
	StartDate string `bson:"start_date,omitempty" json:"start_date,omitempty"`
	Role      string `bson:"role,omitempty" json:"role,omitempty"`
	Name      struct {
		GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
	} `bson:"name,omitempty" json:"name,omitempty"`
	GsmId int `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
}

type TeamContact struct {
	Gsm struct {
		Address string `bson:"address,omitempty" json:"address,omitempty"`
		Email   string `bson:"email,omitempty" json:"email,omitempty"`
		Fax     string `bson:"fax,omitempty" json:"fax,omitempty"`
		Phone   string `bson:"phone,omitempty" json:"phone,omitempty"`
		Url     string `bson:"url,omitempty" json:"url,omitempty"`
	} `bson:"gsm,omitempty" json:"gsm,omitempty"`
}

type TeamPlayer struct {
	StartDate string `bson:"start_date,omitempty" json:"start_date,omitempty"`
	Role      string `bson:"role,omitempty" json:"role,omitempty"`
	Name      struct {
		GsmName string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
	} `bson:"name,omitempty" json:"name,omitempty"`
	EndDate string `bson:"end_date,omitempty" json:"end_date,omitempty"`
	GsmId   int    `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
}

type TeamRecords struct {
	Drawn        int `bson:"drawn,omitempty" json:"drawn,omitempty"`
	GoalsAgainst int `bson:"goals_against,omitempty" json:"goals_against,omitempty"`
	GoalsFor     int `bson:"goals_for,omitempty" json:"goals_for,omitempty"`
	Lost         int `bson:"lost,omitempty" json:"lost,omitempty"`
	Played       int `bson:"played,omitempty" json:"played,omitempty"`
	Points       int `bson:"points,omitempty" json:"points,omitempty"`
	Rank         int `bson:"rank,omitempty" json:"rank,omitempty"`
	Won          int `bson:"won,omitempty" json:"won,omitempty"`
}

type Team struct {
	ID      bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Area    GsmObject     `bson:"area,omitempty" json:"area,omitempty"`
	Coaches []TeamCoach   `bson:"coaches,omitempty" json:"coaches,omitempty"`
	Contact TeamContact   `bson:"contact,omitempty" json:"contact,omitempty"`
	Founded struct {
		Gsm int `bson:"gsm,omitempty" json:"gsm,omitempty"`
	} `bson:"founded,omitempty" json:"founded,omitempty"`
	GsmId        int    `bson:"gsm_id,omitempty" json:"gsm_id,omitempty"`
	GsmParseTime string `bson:"gsm_parse_time,omitempty" json:"gsm_parse_time,omitempty"`
	Location     struct {
		Gsm struct {
			City string `bson:"city,omitempty" json:"city,omitempty"`
		} `bson:"gsm,omitempty" json:"gsm,omitempty"`
	} `bson:"location,omitempty" json:"location,omitempty"`
	Name struct {
		GsmAbbr         string `bson:"gsm_abbr,omitempty" json:"gsm_abbr,omitempty"`
		GsmName         string `bson:"gsm_name,omitempty" json:"gsm_name,omitempty"`
		GsmOfficialName string `bson:"gsm_official_name,omitempty" json:"gsm_official_name,omitempty"`
		GsmShortName    string `bson:"gsm_short_name,omitempty" json:"gsm_short_name,omitempty"`
	} `bson:"name,omitempty" json:"name,omitempty"`
	OptaID  int          `bson:"opta_id,omitempty" json:"opta_id,omitempty"`
	Players []TeamPlayer `bson:"players,omitempty" json:"players,omitempty"`
	Record  []struct {
		Away        TeamRecords `bson:"away,omitempty" json:"away,omitempty"`
		Competition GsmObject   `bson:"competition,omitempty" json:"competition,omitempty"`
		Home        TeamRecords `bson:"home,omitempty" json:"home,omitempty"`
		Round       GsmObject   `bson:"round,omitempty" json:"round,omitempty"`
		Season      GsmObject   `bson:"season,omitempty" json:"season,omitempty"`
		Total       TeamRecords `bson:"total,omitempty" json:"total,omitempty"`
	} `bson:"record,omitempty" json:"record,omitempty"`
	Type struct {
		Gsm struct {
			AgeLevel      string `bson:"age_level,omitempty" json:"age_level,omitempty"`
			HasParentTeam bool   `bson:"has_parent_team,omitempty" json:"has_parent_team,omitempty"`
			Sex           string `bson:"sex,omitempty" json:"sex,omitempty"`
		} `bson:"gsm,omitempty" json:"gsm,omitempty"`
	} `bson:"type,omitempty" json:"type,omitempty"`
	UpdateTime string `bson:"update_time,omitempty" json:"update_time,omitempty"`
}
