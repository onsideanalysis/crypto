package bookmakers

import (
	"strings"
)

type Bookmaker string
type Provider string

type ExecDetailsBetdaq struct {
	MarketID    string `bson:"market_id,omitempty" json:"market_id,omitempty"`
	SelectionID string `bson:"selection_id,omitempty" json:"selection_id,omitempty"`
	EventID     string `bson:"event_id,omitempty" json:"event_id,omitempty"`
}
type ExecDetailsBetfair struct {
	MarketID    string `bson:"market_id,omitempty" json:"market_id,omitempty"`
	SelectionID string `bson:"selection_id,omitempty" json:"selection_id,omitempty"`
	EventID     string `bson:"event_id,omitempty" json:"event_id,omitempty"`
}
type ExecDetailsMatchbook struct {
	MarketID    string `bson:"market_id,omitempty" json:"market_id,omitempty"`
	SelectionID string `bson:"selection_id,omitempty" json:"selection_id,omitempty"`
	EventID     string `bson:"event_id,omitempty" json:"event_id,omitempty"`
}
type ExecDetailsLadbrokes struct {
	MarketID        string `bson:"market_id,omitempty" json:"market_id,omitempty"`
	SelectionID     string `bson:"selection_id,omitempty" json:"selection_id,omitempty"`
	EventID         string `bson:"event_id,omitempty" json:"event_id,omitempty"`
	RegionName      string `bson:"region_name" json:"region_name"`
	CompetitionName string `bson:"competition_name" json:"competition_name"`
	EventName       string `bson:"event_name" json:"event_name"`
	MarketInRunning string `bson:"market_in_running" json:"market_in_running"`
}

var (
	BETFAIR             Bookmaker = "BF"
	MATCHBOOK           Bookmaker = "MB"
	BROKERAGE_MATCHBOOK Bookmaker = "BMB"
	BETDAQ              Bookmaker = "BD"
	PINNACLE            Bookmaker = "PN"
	SBO                 Bookmaker = "SBO"
	ISN                 Bookmaker = "ISN"
	IBC                 Bookmaker = "IBC"
	CROWN               Bookmaker = "CROWN"
	BET365              Bookmaker = "BET365"
	BOOKMAKER_UNKNOWN   Bookmaker = "UNKNOWN"
	GA288               Bookmaker = "GA288"
	GA88                Bookmaker = "GA88"
	SBC                 Bookmaker = "SBC"
	LX168               Bookmaker = "LX168"
	IWIN168             Bookmaker = "IWIN168"
	WT16888             Bookmaker = "WT16888"
	A3ET                Bookmaker = "A3ET"
	LADBROKES           Bookmaker = "LB"
	SINGBET             Bookmaker = "SING"
	CMDBET              Bookmaker = "CMD"
	SB188               Bookmaker = "SB188"
	MOLLY               Bookmaker = "MOLLY"
	BETCRIS             Bookmaker = "BETCRIS"
	BET_INVITE          Bookmaker = "BETINVITE"
	WHALE_RTA           Bookmaker = "WHALERTA"
	WHALE_MTA           Bookmaker = "WHALEMTA"
	STRATAGEM           Bookmaker = "SGM"
	GDAX                Bookmaker = "GDAX"
	BITMEX              Bookmaker = "BMEX"

	STRATAGEM_PROVIDER       Provider = "SGM"
	STRATABET                Provider = "SB"
	STRATASPORT              Provider = "SS"
	INSIGHTS_DEMO            Provider = "ID"
	JT                       Provider = "JT"
	A3ET_PROVIDER            Provider = "A3ET"
	JOSEPHTEY_DIST_PROVIDER  Provider = "JTDIST"
	FRACSOFT                 Provider = "FS"
	PINNACLE_PROVIDER        Provider = "PN"
	PINBET88                 Provider = "PN88"
	SAMVO                    Provider = "SV"
	BETFAIR_PROVIDER         Provider = "BF"
	BETFAIR_DIST_PROVIDER    Provider = "BFDIST"
	MATCHBOOK_PROVIDER       Provider = "MB"
	MATCHBOOK_DIST_PROVIDER  Provider = "MBDIST"
	BETDAQ_PROVIDER          Provider = "BD"
	BETDAQ_DIST_PROVIDER     Provider = "BDDIST"
	EASTBRIDGE_DIST_PROVIDER Provider = "EBDIST"
	BET365_PROVIDER          Provider = "BET365"
	WHOSCORED                Provider = "WS"
	GSM                      Provider = "GSM"
	ENETPULSE                Provider = "ENP"
	EASTBRIDGE               Provider = "EB"
	PROVIDER_UNKNOWN         Provider = "UNKNOWN"
	SIMULATOR                Provider = "SIM"
	LADBROKES_PROVIDER       Provider = "LB"
	MOLLY_PROVIDER           Provider = "MOLLY"
	BETCRIS_PROVIDER         Provider = "BETCRIS"
	BET_INVITE_PROVIDER      Provider = "BETINVITE"
	WHALE_PROVIDER           Provider = "WHALE"
	COINIGY                  Provider = "COINIGY"
	GDAX_PROVIDER            Provider = "GDAX"
	BITMEX_PROVIDER          Provider = "BMEX"
	GDAX_SANDBOX             Provider = "GDAXSANDBOX"
	BITMEX_TESTNET           Provider = "BMEXTESTNET"

	ALL_BOOKMAKERS = []Bookmaker{BETFAIR, MATCHBOOK, BROKERAGE_MATCHBOOK, BETDAQ, PINNACLE, LADBROKES}

	BOOKMAKER_NAMES = map[Bookmaker]string{MOLLY: "molly", BETFAIR: "betfair", MATCHBOOK: "matchbook", BROKERAGE_MATCHBOOK: "matchbook",
		BETDAQ: "betdaq", PINNACLE: "pinnacle", A3ET: "a3et", LADBROKES: "ladbrokes", BETCRIS: "betcris", BET_INVITE: "betInvite", WHALE_RTA: "whaleRTA",
		WHALE_MTA: "whaleMTA"}

	PROVIDER_NAMES = map[Provider]string{
		BETFAIR_PROVIDER:    "betfair",
		MATCHBOOK_PROVIDER:  "matchbook",
		BETDAQ_PROVIDER:     "betdaq",
		PINNACLE_PROVIDER:   "pinnacle",
		PINBET88:            "pinbet88",
		A3ET_PROVIDER:       "a3et",
		LADBROKES_PROVIDER:  "ladbrokes",
		STRATABET:           "stratabet",
		STRATASPORT:         "stratasport",
		INSIGHTS_DEMO:       "insights-demo",
		MOLLY_PROVIDER:      "molly",
		BETCRIS_PROVIDER:    "betcris",
		BET_INVITE_PROVIDER: "betInvite",
		WHALE_PROVIDER:      "whale",
		COINIGY:             "coinigy",
		GDAX_PROVIDER:       "gdax",
		BITMEX_PROVIDER:     "bitmex",
		GDAX_SANDBOX:        "gdaxsandbox",
		BITMEX_TESTNET:      "bitmextestnet",
	}

	EXCHANGE_BOOKMAKERS = []Bookmaker{BETFAIR, MATCHBOOK, BROKERAGE_MATCHBOOK, BETDAQ}

	// Different bookmakers for stratasport and stratabet
	STRATAGEM_PROVIDER_BOOKMAKERS = map[Provider][]Bookmaker{
		STRATASPORT:   {BETFAIR, MATCHBOOK, BETDAQ, LADBROKES},
		STRATABET:     {MATCHBOOK, BETFAIR, BETDAQ, SBO, PINNACLE, A3ET},
		INSIGHTS_DEMO: {MATCHBOOK, BETFAIR, BETDAQ, SBO, PINNACLE, A3ET},
	}

	BOOKMAKER_IDS = map[Bookmaker]int{
		BETFAIR:             24,
		MATCHBOOK:           22,
		BROKERAGE_MATCHBOOK: 22,
		PINNACLE:            5,
		A3ET:                21,
		BETDAQ:              20,
		LADBROKES:           30,
		MOLLY:               1313,
	}

	PROVIDER_IDS = map[Provider]int{
		BETFAIR_PROVIDER:         2,
		MATCHBOOK_PROVIDER:       4,
		PINNACLE_PROVIDER:        11,
		SAMVO:                    12,
		JT:                       6,
		EASTBRIDGE:               16,
		A3ET_PROVIDER:            13,
		BETDAQ_PROVIDER:          8,
		PINBET88:                 10,
		MOLLY_PROVIDER:           18,
		BETFAIR_DIST_PROVIDER:    2,
		MATCHBOOK_DIST_PROVIDER:  4,
		BETDAQ_DIST_PROVIDER:     8,
		JOSEPHTEY_DIST_PROVIDER:  6,
		EASTBRIDGE_DIST_PROVIDER: 16,
	}

	REVERSED_BOOKMAKER_ABBR = map[string]Bookmaker{"BF": BETFAIR, "MB": MATCHBOOK, "PN": PINNACLE, "BD": BETDAQ, "SBO": SBO,
		"ISN": ISN, "IBC": IBC, "CROWN": CROWN, "BET365": BET365, "UNKNOWN": BOOKMAKER_UNKNOWN, "GA288": GA288,
		"SBC": SBC, "LX168": LX168, "WT16888": WT16888, "IWIN168": IWIN168, "A3ET": A3ET, "LB": LADBROKES, "SBC168": SBC,
		"GA88": GA88, "CMD": CMDBET, "SING": SINGBET, "MOLLY": MOLLY, "BMB": BROKERAGE_MATCHBOOK, "BETCRIS": BETCRIS,
		"BETINVITE": BET_INVITE, "WHALERTA": WHALE_RTA, "WHALEMTA": WHALE_MTA, "SGM": STRATAGEM, "BMEX": BITMEX, "GDAX": GDAX}

	REVERSED_PROVIDER_NAME = map[string]Provider{"stratabet": STRATABET, "stratasport": STRATASPORT, "insights-demo": INSIGHTS_DEMO}

	REVERSED_PROVIDER_ABBR = map[string]Provider{
		BETFAIR_PROVIDER.String():      BETFAIR_PROVIDER,
		BETFAIR_DIST_PROVIDER.String(): BETFAIR_DIST_PROVIDER,

		MATCHBOOK_PROVIDER.String():      MATCHBOOK_PROVIDER,
		MATCHBOOK_DIST_PROVIDER.String(): MATCHBOOK_DIST_PROVIDER,

		BETDAQ_PROVIDER.String():      BETDAQ_PROVIDER,
		BETDAQ_DIST_PROVIDER.String(): BETDAQ_DIST_PROVIDER,

		PINNACLE_PROVIDER.String(): PINNACLE_PROVIDER,
		PINBET88.String():          PINBET88,

		EASTBRIDGE.String():               EASTBRIDGE,
		EASTBRIDGE_DIST_PROVIDER.String(): EASTBRIDGE_DIST_PROVIDER,

		JOSEPHTEY_DIST_PROVIDER.String(): JOSEPHTEY_DIST_PROVIDER,
		JT.String():                      JT,

		A3ET_PROVIDER.String(): A3ET_PROVIDER,

		LADBROKES_PROVIDER.String():  LADBROKES_PROVIDER,
		MOLLY_PROVIDER.String():      MOLLY_PROVIDER,
		BETCRIS_PROVIDER.String():    BETCRIS_PROVIDER,
		BET_INVITE_PROVIDER.String(): BET_INVITE_PROVIDER,
		STRATAGEM_PROVIDER.String():  STRATAGEM_PROVIDER,
		WHALE_PROVIDER.String():      WHALE_PROVIDER,
		BITMEX_PROVIDER.String():     BITMEX_PROVIDER,
		BITMEX_TESTNET.String():      BITMEX_TESTNET,
		GDAX_PROVIDER.String():       GDAX_PROVIDER,
		GDAX_SANDBOX.String():        GDAX_SANDBOX,
	}

	DEFAULT_COMMISSION_RATES = map[Bookmaker]float64{BETFAIR: 0.05, MATCHBOOK: 0.0115, SBO: 0, BETDAQ: 0.05,
		PINNACLE: 0, IBC: 0, ISN: 0, CROWN: 0, GA288: 0, SBC: 0, LX168: 0, A3ET: 0, BROKERAGE_MATCHBOOK: 0.0115}
)

func (b Bookmaker) String() string {
	return string(b)
}

func (b Bookmaker) Name() string {
	switch b {
	case BETFAIR:
		return "betfair"
	case MATCHBOOK, BROKERAGE_MATCHBOOK:
		return "matchbook"
	case BETDAQ:
		return "betdaq"
	case PINNACLE:
		return "pinnacle"
	case SBO:
		return "sbo"
	case A3ET:
		return "a3et"
	case LADBROKES:
		return "ladbrokes"
	case SINGBET:
		return "singbet"
	case CMDBET:
		return "cmdbet"
	case MOLLY:
		return "molly"
	case BETCRIS:
		return "betcris"
	case BET_INVITE:
		return "betInvite"
	case WHALE_RTA:
		return "whaleRTA"
	case WHALE_MTA:
		return "whaleMTA"
	case GDAX:
		return "gdax"
	case BITMEX:
		return "bitmex"
	default:
		return strings.ToLower(b.String())
	}
}

func (b Bookmaker) NamePretty() string {
	switch b {
	case BETFAIR:
		return "Betfair"
	case MATCHBOOK, BROKERAGE_MATCHBOOK:
		return "Matchbook"
	case BETDAQ:
		return "BETDAQ"
	case PINNACLE:
		return "Pinnacle"
	case SBO:
		return "SBO"
	case A3ET:
		return "3ET"
	case MOLLY:
		return "Molly"
	case LADBROKES:
		return "Ladbrokes"
	case BETCRIS:
		return "Betcris"
	case BET_INVITE:
		return "BetInvite"
	case WHALE_RTA:
		return "WhaleRTA"
	case WHALE_MTA:
		return "WhaleMTA"
	case GDAX:
		return "GDAX"
	case BITMEX:
		return "Bitmex"
	default:
		return b.String()
	}
}

func (b Bookmaker) IsExchange() bool {
	for _, bm := range EXCHANGE_BOOKMAKERS {
		if bm == b {
			return true
		}
	}
	return false
}

func (p Provider) String() string {
	return string(p)
}

func (p Provider) Name() string {
	switch p {
	case BETFAIR_PROVIDER:
		return "betfair"
	case BETFAIR_DIST_PROVIDER:
		return "betfairdist"
	case MATCHBOOK_PROVIDER:
		return "matchbook"
	case MATCHBOOK_DIST_PROVIDER:
		return "matchbookdist"
	case BETDAQ_PROVIDER:
		return "betdaq"
	case BETDAQ_DIST_PROVIDER:
		return "betdaqdist"
	case PINNACLE_PROVIDER:
		return "pinnacle"
	case PINBET88:
		return "pinbet88"
	case EASTBRIDGE:
		return "eastbridge"
	case EASTBRIDGE_DIST_PROVIDER:
		return "eastbridgedist"
	case JOSEPHTEY_DIST_PROVIDER:
		return "josephteydist"
	case JT:
		return "josephtey"
	case SIMULATOR:
		return "simulator"
	case A3ET_PROVIDER:
		return "a3et"
	case LADBROKES_PROVIDER:
		return "ladbrokes"
	case MOLLY_PROVIDER:
		return "molly"
	case BETCRIS_PROVIDER:
		return "betcris"
	case BET_INVITE_PROVIDER:
		return "betInvite"
	case WHALE_PROVIDER:
		return "whale"
	case COINIGY:
		return "coinigy"
	case GDAX_PROVIDER:
		return "gdax"
	case BITMEX_PROVIDER:
		return "bitmex"
	case GDAX_SANDBOX:
		return "gdaxsandbox"
	case BITMEX_TESTNET:
		return "bitmextestnet"
	default:
		return ""
	}
}

func BookmakerFromName(name string) Bookmaker {
	switch name {
	case "betfair":
		return BETFAIR
	case "matchbook":
		return MATCHBOOK
	case "betdaq":
		return BETDAQ
	case "pinnacle":
		return PINNACLE
	case "a3et":
		return A3ET
	case "ladbrokes":
		return LADBROKES
	case "ga288":
		return GA288
	case "crown":
		return CROWN
	case "ibcbet":
		return IBC
	case "sbobet":
		return SBO
	case "sbc":
		return SBC
	case "sbc168":
		return SBC
	case "isn":
		return ISN
	case "ga88":
		return GA88
	case "singbet":
		return SINGBET
	case "cmdbet":
		return CMDBET
	case "sb188":
		return SB188
	case "molly":
		return MOLLY
	case "betcris":
		return BETCRIS
	case "betInvite":
		return BET_INVITE
	case "whaleRTA":
		return WHALE_RTA
	case "whaleMTA":
		return WHALE_MTA
	case "gdax":
		return GDAX
	case "bitmex":
		return BITMEX
	default:
		return ""
	}
}

func ProviderFromName(name string) Provider {
	switch name {
	case "ladbrokes":
		return LADBROKES_PROVIDER
	case "betfair":
		return BETFAIR_PROVIDER
	case "betfairdist":
		return BETFAIR_DIST_PROVIDER
	case "matchbook":
		return MATCHBOOK_PROVIDER
	case "matchbookdist":
		return MATCHBOOK_DIST_PROVIDER
	case "betdaq":
		return BETDAQ_PROVIDER
	case "betdaqdist":
		return BETDAQ_DIST_PROVIDER
	case "pinnacle":
		return PINNACLE_PROVIDER
	case "pinbet88":
		return PINBET88
	case "eastbridge":
		return EASTBRIDGE
	case "eastbridgedist":
		return EASTBRIDGE_DIST_PROVIDER
	case "josephtey":
		return JT
	case "josephteydist":
		return JOSEPHTEY_DIST_PROVIDER
	case "simulator":
		return SIMULATOR
	case "a3et":
		return A3ET_PROVIDER
	case "stratabet":
		return STRATABET
	case "stratasport":
		return STRATASPORT
	case "insights-demo":
		return INSIGHTS_DEMO
	case "molly":
		return MOLLY_PROVIDER
	case "betcris":
		return BETCRIS_PROVIDER
	case "betInvite":
		return BET_INVITE_PROVIDER
	case "whale":
		return WHALE_PROVIDER
	case "coinigy":
		return COINIGY
	case "gdax":
		return GDAX_PROVIDER
	case "bitmex":
		return BITMEX_PROVIDER
	case "gdaxsandbox":
		return GDAX_SANDBOX
	case "bitmextestnet":
		return BITMEX_TESTNET
	default:
		return ""
	}
}

// GetBookmakerListForProject returns the bookmarks list for the given provider name.
// If the provider is not found, returns the list for stratabet
func GetBookmakerListForProject(project string) (bookmakersList []Bookmaker) {
	provider, ok := REVERSED_PROVIDER_NAME[project]
	if !ok {
		provider = STRATABET
	}
	bookmakersList = STRATAGEM_PROVIDER_BOOKMAKERS[provider]
	return
}
