package sport

type Fixture struct {
	EventID   string                 `json:"event_id,omitempty"`
	EventName string                 `json:"event_name,omitempty"`
	StartDate string                 `json:"start_date,omitempty"`
	StageName string                 `json:"stage_name,omitempty"`
	StageID   int                    `json:"stage_id,omitempty"`
	Country   string                 `json:"country,omitempty"`
	CountryID int                    `json:"country_id,omitempty"`
	City      string                 `json:"city,omitempty"`
	Details   map[string]interface{} `json:"details,omitempty"`
	Status    string                 `json:"status,omitempty"`
}
