package sport

import "strings"

type Sport int

var (
	FOOTBALL          Sport = 1
	TENNIS            Sport = 2
	CRICKET           Sport = 3
	GOLF              Sport = 4
	FORMULA1          Sport = 5
	BASKETBALL        Sport = 6
	HORSE_RACING      Sport = 7
	GREYHOUND_RACING  Sport = 8
	OTHER             Sport = 10
	SPORT_UNKNOWN     Sport = -1
	MULTISPORT        Sport = 9
	BASEBALL          Sport = 11
	HOCKEY            Sport = 12
	AMERICAN_FOOTBALL Sport = 13
	CRYPTO            Sport = 14

	SPORT_ABBR = map[Sport]string{
		FOOTBALL: "S", TENNIS: "T", CRICKET: "C", GOLF: "G", BASKETBALL: "BB", HORSE_RACING: "HR",
		GREYHOUND_RACING: "GR", MULTISPORT: "MS", SPORT_UNKNOWN: "UNKNOWN", BASEBALL: "BL", HOCKEY: "H",
		AMERICAN_FOOTBALL: "AF",
	}
	REVERSED_SPORT_ABBR = map[string]Sport{
		"S": FOOTBALL, "T": TENNIS, "C": CRICKET, "G": GOLF, "BB": BASKETBALL, "HR": HORSE_RACING,
		"GR": GREYHOUND_RACING, "MS": MULTISPORT, "BL": BASEBALL, "H": HOCKEY, "AF": AMERICAN_FOOTBALL,
	}
)

func (s Sport) String() string {
	switch s {
	case FOOTBALL:
		return "football"
	case TENNIS:
		return "tennis"
	case CRICKET:
		return "cricket"
	case GOLF:
		return "golf"
	case SPORT_UNKNOWN:
		return "unknown"
	case BASKETBALL:
		return "basketball"
	case HORSE_RACING:
		return "horse-racing"
	case GREYHOUND_RACING:
		return "greyhound-racing"
	case OTHER:
		return "other"
	case MULTISPORT:
		return "multisport"
	case BASEBALL:
		return "baseball"
	case HOCKEY:
		return "hockey"
	case AMERICAN_FOOTBALL:
		return "american-football"
	}
	return ""
}

func (s Sport) Abbr() string {
	return SPORT_ABBR[s]
}

func GetFromName(name string) Sport {
	name = strings.ToLower(name)

	if name == "football" || name == "soccer" {
		return FOOTBALL
	} else if name == "tennis" {
		return TENNIS
	} else if strings.Contains(name, "basketball") {
		//e.g. NCAA Basketball
		return BASKETBALL
	} else if name == "cricket" {
		return CRICKET
	} else if name == "golf" {
		return GOLF
	} else if name == "horse-racing" {
		return HORSE_RACING
	} else if name == "greyhound-racing" {
		return GREYHOUND_RACING
	} else if name == "multisport" {
		return MULTISPORT
	} else if name == "baseball" {
		return BASEBALL
	} else if name == "hockey" {
		return HOCKEY
	} else if name == "american-football" {
		return AMERICAN_FOOTBALL
	} else {
		return SPORT_UNKNOWN
	}
}
