package sport

type Action struct {
	EventID string                 `json:"event_id,omitempty"`
	Details map[string]interface{} `json:"details,omitempty"`
}
