package sport

type Outcome struct {
	EventID   string                 `json:"event_id,omitempty"`
	StartDate string                 `json:"start_date,omitempty"`
	EndDate   string                 `json:"end_date,omitempty"`
	Details   map[string]interface{} `json:"details,omitempty"`
}
