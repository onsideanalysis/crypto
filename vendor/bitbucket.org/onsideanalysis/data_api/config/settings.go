package config

import (
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v2"
)

var config Config

type MongoAccess struct {
	UserName string `yaml:"username"`
	Password string `yaml:"password"`
	AuthDB   string `yaml:"authDb"`
}

type Config struct {
	Mongo struct {
		Reference struct {
			RW struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"rw,omitempty"`
			RO struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"ro,omitempty"`
			Hosts      []string `yaml:"hosts"`
			DbName     string   `yaml:"db_name"`
			ReplicaSet string   `yaml:"replica_set,omitempty"`
		} `yaml:"reference"`

		Trading struct {
			RW struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"rw,omitempty"`
			RO struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"ro,omitempty"`
			Hosts      []string `yaml:"hosts"`
			DbName     string   `yaml:"db_name"`
			ReplicaSet string   `yaml:"replica_set,omitempty"`
		} `yaml:"trading"`

		CryptoTrading struct {
			RW struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"rw,omitempty"`
			RO struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"ro,omitempty"`
			Hosts      []string `yaml:"hosts"`
			DbName     string   `yaml:"db_name"`
			ReplicaSet string   `yaml:"replica_set,omitempty"`
		} `yaml:"crypto_trading"`

		Odds struct {
			RW struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"rw,omitempty"`
			RO struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"ro,omitempty"`
			Hosts      []string `yaml:"hosts"`
			DbName     string   `yaml:"db_name"`
			ReplicaSet string   `yaml:"replica_set,omitempty"`
		} `yaml:"odds"`

		Football struct {
			RW struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"rw,omitempty"`
			RO struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"ro,omitempty"`
			Hosts      []string `yaml:"hosts"`
			DbName     string   `yaml:"db_name"`
			ReplicaSet string   `yaml:"replica_set,omitempty"`
		} `yaml:"football"`

		Basketball struct {
			RW struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"rw,omitempty"`
			RO struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"ro,omitempty"`
			Hosts      []string `yaml:"hosts"`
			DbName     string   `yaml:"db_name"`
			ReplicaSet string   `yaml:"replica_set,omitempty"`
		} `yaml:"basketball"`

		Tennis struct {
			RW struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"rw,omitempty"`
			RO struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"ro,omitempty"`
			Hosts      []string `yaml:"hosts"`
			DbName     string   `yaml:"db_name"`
			ReplicaSet string   `yaml:"replica_set,omitempty"`
		} `yaml:"tennis"`

		Websites struct {
			RW struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"rw,omitempty"`
			RO struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"ro,omitempty"`
			Hosts      []string `yaml:"hosts"`
			DbName     string   `yaml:"db_name"`
			ReplicaSet string   `yaml:"replica_set,omitempty"`
		} `yaml:"websites"`

		Models struct {
			RW struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"rw,omitempty"`
			RO struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"ro,omitempty"`
			Hosts      []string `yaml:"hosts"`
			DbName     string   `yaml:"db_name"`
			ReplicaSet string   `yaml:"replica_set,omitempty"`
		} `yaml:"models"`

		ModelsV2 struct {
			RW struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"rw,omitempty"`
			RO struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"ro,omitempty"`
			Hosts      []string `yaml:"hosts"`
			DbName     string   `yaml:"db_name"`
			ReplicaSet string   `yaml:"replica_set,omitempty"`
		} `yaml:"models_v2"`

		AbcFootballV2 struct {
			RW struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"rw,omitempty"`
			RO struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"ro,omitempty"`
			Hosts      []string `yaml:"hosts"`
			DbName     string   `yaml:"db_name"`
			ReplicaSet string   `yaml:"replica_set,omitempty"`
		} `yaml:"abc_football_v2"`

		Metrics struct {
			RW struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"rw,omitempty"`
			RO struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"ro,omitempty"`
			Hosts      []string `yaml:"hosts"`
			DbName     string   `yaml:"db_name"`
			ReplicaSet string   `yaml:"replica_set,omitempty"`
		} `yaml:"metrics"`

		Backtesting struct {
			RW struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"rw,omitempty"`
			RO struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"ro,omitempty"`
			Hosts      []string `yaml:"hosts"`
			DbName     string   `yaml:"db_name"`
			ReplicaSet string   `yaml:"replica_set,omitempty"`
		} `yaml:"backtesting"`

		Aggregator struct {
			RW struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"rw,omitempty"`
			RO struct {
				MongoAccess `yaml:",inline"`
			} `yaml:"ro,omitempty"`
			Hosts      []string `yaml:"hosts"`
			DbName     string   `yaml:"db_name"`
			ReplicaSet string   `yaml:"replica_set,omitempty"`
		} `yaml:"aggregator"`
	} `yaml:"mongo"`

	Redis struct {
		SentinelHosts []string `yaml:"sentinel_hosts"`
		SentinelGroup string   `yaml:"sentinel_group"`
		Password      string   `yaml:"password"`
	} `yaml:"redis"`

	Cassandra struct {
		ParserData struct {
			RW struct {
				Username string `yaml:"username"`
				Password string `yaml:"password"`
			} `yaml:"rw,omitempty"`
			RO struct {
				Username string `yaml:"username"`
				Password string `yaml:"password"`
			} `yaml:"ro,omitempty"`
			Hosts    []string `yaml:"host_list"`
			KeySpace string   `yaml:"keyspace"`
		} `yaml:"parserdata"`
		StrategyData struct {
			RW struct {
				Username string `yaml:"username"`
				Password string `yaml:"password"`
			} `yaml:"rw,omitempty"`
			RO struct {
				Username string `yaml:"username"`
				Password string `yaml:"password"`
			} `yaml:"ro,omitempty"`
			Hosts    []string `yaml:"host_list"`
			KeySpace string   `yaml:"keyspace"`
		} `yaml:"strategydata"`
		Contrib struct {
			RW struct {
				Username string `yaml:"username"`
				Password string `yaml:"password"`
			} `yaml:"rw,omitempty"`
			RO struct {
				Username string `yaml:"username"`
				Password string `yaml:"password"`
			} `yaml:"ro,omitempty"`
			Hosts    []string `yaml:"host_list"`
			KeySpace string   `yaml:"keyspace"`
		} `yaml:"contrib"`
	} `yaml:"cassandra"`

	Datadog struct {
		ApiKey string `yaml:"api_key"`
	} `yaml:"datadog"`

	Mailchimp struct {
		ApiKey string `yaml:"api_key"`
	} `yaml:"mailchimp"`

	Rabbit struct {
		HostList []string `yaml:"host_list"`
	} `yaml:"rabbit"`

	Zookeeper struct {
		HostList []string `yaml:"host_list"`
	} `yaml:"zookeeper"`

	Kafka struct {
		HostList []string `yaml:"host_list"`
	} `yaml:"kafka"`

	ParsingCoordinator struct {
		ExchangeName string `yaml:"exchange_name"`
		ExchangeType string `yaml:"exchange_type"`
	} `yaml:"parsing_coordinator"`

	HttpProxy struct {
		Germany struct {
			Host     string `yaml:"host"`
			Port     string `yaml:"port"`
			Username string `yaml:"username"`
			Password string `yaml:"password"`
		} `yaml:"germany"`
	} `yaml:"http_proxy"`

	Mysql struct {
		Accounts struct {
			DataApi struct {
				Host     string `yaml:"host"`
				Port     string `yaml:"port"`
				Username string `yaml:"username"`
				Password string `yaml:"password"`
				Database string `yaml:"database"`
				Timezone string `yaml:"timezone"`
			} `yaml:"data_api"`
			EnetSports struct {
				Host     string `yaml:"host"`
				Port     string `yaml:"port"`
				Username string `yaml:"username"`
				Password string `yaml:"password"`
				Database string `yaml:"database"`
				Timezone string `yaml:"timezone"`
			} `yaml:"enetsports"`
			TennisAbstract struct {
				Host     string `yaml:"host"`
				Port     string `yaml:"port"`
				Username string `yaml:"username"`
				Password string `yaml:"password"`
				Database string `yaml:"database"`
				Timezone string `yaml:"timezone"`
			} `yaml:"tennis_abstract"`
		} `yaml:"accounts"`
	} `yaml:"mysql"`

	Apifier struct {
		ApiKey     string `yaml:"api_key"`
		ExecTokens struct {
			NumberFire   string `yaml:"numberfire"`
			Cover        string `yaml:"cover"`
			AccuScore    string `yaml:"accuscore"`
			TeamRankings string `yaml:"teamrankings"`
		} `yaml:"exec_tokens"`
	} `yaml:"apifier"`

	Amazon struct {
		S3 struct {
			AccessKey string `yaml:"aws_access_key_id"`
			SecretKey string `yaml:"aws_secret_access_key"`
		} `yaml:"s3"`
	} `yaml:"amazon"`

	Paypal struct {
		Username  string `yaml:"username"`
		Password  string `yaml:"password"`
		Signature string `yaml:"signature"`
	} `yaml:"paypal"`
}

func GetConfig(env, configPath string) Config {

	configFile := configPath + "settings_" + env + ".yml"

	yamlFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		log.Fatalf("yamlFile.Get err   #%v ", err)
	}

	config = Config{}
	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}

	return config

}
