package utilities

type GroupBy string

func (g GroupBy) String() string {
	return string(g)
}

const (
	Last  GroupBy = "$last"
	First GroupBy = "$first"
)
