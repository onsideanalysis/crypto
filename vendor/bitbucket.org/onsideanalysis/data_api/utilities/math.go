package utilities

import "math"

var (
	SIZE_PRECISION  int = 2
	PRICE_PRECISION int = 3
	SIZE_EPSILON        = math.Pow10(-SIZE_PRECISION)
)

func InitPrecision(sizePrecision, pricePrecision int) {
	SIZE_PRECISION = sizePrecision
	PRICE_PRECISION = pricePrecision
	SIZE_EPSILON = math.Pow10(-sizePrecision)
}

func Round(val float64, roundOn float64, places int) (newVal float64) {
	var round float64
	pow := math.Pow(10, float64(places))
	digit := pow * val
	_, div := math.Modf(digit)
	_div := math.Copysign(div, val)
	_roundOn := math.Copysign(roundOn, val)
	if _div >= _roundOn {
		round = math.Ceil(digit)
	} else {
		round = math.Floor(digit)
	}
	newVal = round / pow
	return
}

// An attempt at a universal odds comparison function, currently looking at 3 decimal points
// 1: odds1 > odds2
// 0: odds1 == odds2
// -1: odds1 < odds2
func CompareOdds(odds1 float64, odds2 float64) int {
	odds1Rounded := Round(odds1, 0.5, PRICE_PRECISION)
	odds2Rounded := Round(odds2, 0.5, PRICE_PRECISION)

	if odds1Rounded > odds2Rounded {
		return 1
	} else if odds1Rounded < odds2Rounded {
		return -1
	} else {
		return 0
	}
}

// An attempt at a universal size comparison function, currently looking at 2 decimal points
// 1: size1 > size2
// 0: size1 == size2
// -1: size1 < size2
func CompareSize(size1 float64, size2 float64) int {
	size1Rounded := Round(size1, 0.5, SIZE_PRECISION)
	size2Rounded := Round(size2, 0.5, SIZE_PRECISION)

	if size1Rounded > size2Rounded {
		return 1
	} else if size1Rounded < size2Rounded {
		return -1
	} else {
		return 0
	}
}

func IsSizeZero(size float64) bool {
	return size < SIZE_EPSILON
}

// Returns a list whose elements are the cumulative sums of the elements of the input list.
func CumSum(list interface{}) interface{} {

	switch l := list.(type) {
	case []int:
		var cumSum []int
		for i := 0; i < len(l); i++ {
			if i == 0 {
				cumSum = append(cumSum, l[i])
			} else {
				cumSum = append(cumSum, (cumSum[i-1] + l[i]))
			}
		}
		return cumSum
	case []float32:
		var cumSum []float32
		for i := 0; i < len(l); i++ {
			if i == 0 {
				cumSum = append(cumSum, l[i])
			} else {
				cumSum = append(cumSum, (cumSum[i-1] + l[i]))
			}
		}
		return cumSum
	case []float64:
		var cumSum []float64
		for i := 0; i < len(l); i++ {
			if i == 0 {
				cumSum = append(cumSum, l[i])
			} else {
				cumSum = append(cumSum, (cumSum[i-1] + l[i]))
			}
		}
		return cumSum
	default:
		return nil
	}
}

// Returns a list whose elements are the cumulative maxima of the input list
func CumMax(list interface{}) interface{} {

	switch l := list.(type) {
	case []int:
		var cumSum []int
		for i := 0; i < len(l); i++ {
			if i == 0 {
				cumSum = append(cumSum, l[i])
			} else {
				if l[i] > cumSum[i-1] {
					cumSum = append(cumSum, l[i])
				} else {
					cumSum = append(cumSum, cumSum[i-1])
				}
			}
		}
		return cumSum
	case []float32:
		var cumSum []float32
		for i := 0; i < len(l); i++ {
			if i == 0 {
				cumSum = append(cumSum, l[i])
			} else {
				if l[i] > cumSum[i-1] {
					cumSum = append(cumSum, l[i])
				} else {
					cumSum = append(cumSum, cumSum[i-1])
				}
			}
		}
		return cumSum
	case []float64:
		var cumSum []float64
		for i := 0; i < len(l); i++ {
			if i == 0 {
				cumSum = append(cumSum, l[i])
			} else {
				if l[i] > cumSum[i-1] {
					cumSum = append(cumSum, l[i])
				} else {
					cumSum = append(cumSum, cumSum[i-1])
				}
			}
		}
		return cumSum
	default:
		return nil
	}
}

// Returns a list whose elements are the cumulative minimum of the input list
func CumMin(list interface{}) interface{} {

	switch l := list.(type) {
	case []int:
		var cumSum []int
		for i := 0; i < len(l); i++ {
			if i == 0 {
				cumSum = append(cumSum, l[i])
			} else {
				if l[i] < cumSum[i-1] {
					cumSum = append(cumSum, l[i])
				} else {
					cumSum = append(cumSum, cumSum[i-1])
				}
			}
		}
		return cumSum
	case []float32:
		var cumSum []float32
		for i := 0; i < len(l); i++ {
			if i == 0 {
				cumSum = append(cumSum, l[i])
			} else {
				if l[i] < cumSum[i-1] {
					cumSum = append(cumSum, l[i])
				} else {
					cumSum = append(cumSum, cumSum[i-1])
				}
			}
		}
		return cumSum
	case []float64:
		var cumSum []float64
		for i := 0; i < len(l); i++ {
			if i == 0 {
				cumSum = append(cumSum, l[i])
			} else {
				if l[i] < cumSum[i-1] {
					cumSum = append(cumSum, l[i])
				} else {
					cumSum = append(cumSum, cumSum[i-1])
				}
			}
		}
		return cumSum
	default:
		return nil
	}
}

//Returns the maximum drawdown of the given list
func MDD(list interface{}) (mdd interface{}) {

	switch l := list.(type) {
	case []int:
		prevMin := 0
		prevMax := 0
		max := 0
		for index := 0; index < len(l); index++ {
			if l[index] > l[max] {
				max = index
			} else {
				if (l[max] - l[index]) > (l[prevMax] - l[prevMin]) {
					prevMax = max
					prevMin = index
				}
			}
		}
		if len(l) > 0 {
			mdd = l[prevMin] - l[prevMax]
		} else {
			mdd = 0.0
		}
		return
	case []float32:
		prevMin := 0
		prevMax := 0
		max := 0
		for index := 0; index < len(l); index++ {
			if l[index] > l[max] {
				max = index
			} else {
				if (l[max] - l[index]) > (l[prevMax] - l[prevMin]) {
					prevMax = max
					prevMin = index
				}
			}
		}
		if len(l) > 0 {
			mdd = l[prevMin] - l[prevMax]
		} else {
			mdd = 0.0
		}
		return
	case []float64:
		prevMin := 0
		prevMax := 0
		max := 0
		for index := 0; index < len(l); index++ {
			if l[index] > l[max] {
				max = index
			} else {
				if (l[max] - l[index]) > (l[prevMax] - l[prevMin]) {
					prevMax = max
					prevMin = index
				}
			}
		}
		if len(l) > 0 {
			mdd = l[prevMin] - l[prevMax]
		} else {
			mdd = 0.0
		}
		return
	default:
		return nil
	}

}

//Returns the maximum run up of the given list
func MRU(list interface{}) (mdd interface{}) {

	switch l := list.(type) {
	case []int:
		prevMin := 0
		prevMax := 0
		min := 0
		for index := 0; index < len(l); index++ {
			if l[index] < l[min] {
				min = index
			} else {
				if (l[index] - l[min]) > (l[prevMax] - l[prevMin]) {
					prevMax = index
					prevMin = min
				}
			}
		}
		if len(l) > 0 {
			mdd = l[prevMax] - l[prevMin]
		} else {
			mdd = 0.0
		}
		return
	case []float32:
		prevMin := 0
		prevMax := 0
		min := 0
		for index := 0; index < len(l); index++ {
			if l[index] < l[min] {
				min = index
			} else {
				if (l[index] - l[min]) > (l[prevMax] - l[prevMin]) {
					prevMax = index
					prevMin = min
				}
			}
		}
		if len(l) > 0 {
			mdd = l[prevMax] - l[prevMin]
		} else {
			mdd = 0.0
		}
		return
	case []float64:
		prevMin := 0
		prevMax := 0
		min := 0
		for index := 0; index < len(l); index++ {
			if l[index] < l[min] {
				min = index
			} else {
				if (l[index] - l[min]) > (l[prevMax] - l[prevMin]) {
					prevMax = index
					prevMin = min
				}
			}
		}
		if len(l) > 0 {
			mdd = l[prevMax] - l[prevMin]
		} else {
			mdd = 0.0
		}
		return
	default:
		return nil
	}

}

func Sum(nums ...float64) float64 {
	total := 0.0
	for _, num := range nums {
		total += num
	}
	return total
}

func Average(list interface{}) interface{} {
	switch l := list.(type) {
	case []int:
		total := float32(0)
		for index := 0; index < len(l); index++ {
			total += float32(l[index])
		}
		return total / float32(len(l))
	case []float32:
		total := float32(0)
		for index := 0; index < len(l); index++ {
			total += l[index]
		}
		return total / float32(len(l))
	case []float64:
		total := 0.0
		for index := 0; index < len(l); index++ {
			total += l[index]
		}
		return total / float64(len(l))
	default:
		return nil
	}
}

// Variance returns the variance of the slice of float32.
func Variance(values []float64) float64 {
	if 0 == len(values) {
		return 0.0
	}
	m := Average(values)
	var sum float64
	for _, v := range values {
		d := v - m.(float64)
		sum += d * d
	}
	return sum / float64(len(values)-1)
}

func StdDev(values []float64) float64 {
	return math.Sqrt(Variance(values))
}
