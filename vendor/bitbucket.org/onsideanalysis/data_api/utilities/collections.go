package utilities

import (
	"math"
	"strconv"
)

// Check if element is inside a list. Returns the index of the element if found otherwise -1
func Contains(list interface{}, element interface{}) int {
	switch element.(type) {
	case int:
		switch l := list.(type) {
		case []int:
			for i := 0; i < len(l); i++ {
				if l[i] == element {
					return i
				}
			}
		}
	case string:
		switch l := list.(type) {
		case []string:
			for i := 0; i < len(l); i++ {
				if l[i] == element {
					return i
				}
			}
		}
	default:
		return -1
	}
	return -1
}

// Join concatenates the elements of a to create a single string.   The separator string
// sep is placed between elements in the resulting string.
func Join(a []int, sep string) string {
	if len(a) == 0 {
		return ""
	}
	if len(a) == 1 {
		return strconv.Itoa(a[0])
	}
	n := len(sep) * (len(a) - 1)
	for i := 0; i < len(a); i++ {
		n += len(strconv.Itoa(a[i]))
	}

	b := make([]byte, n)
	bp := copy(b, strconv.Itoa(a[0]))
	for _, s := range a[1:] {
		bp += copy(b[bp:], sep)
		bp += copy(b[bp:], strconv.Itoa(s))
	}
	return string(b)
}

func Frange(start, end, inc float64) []float64 {
	if end < start {
		return []float64{}
	}
	if start == end {
		return []float64{start}
	}

	count := int(math.Ceil((end-start)/inc)) + 1

	myArray := make([]float64, count)
	myArray[0] = start
	for index := 1; index < count; index++ {
		myArray[index] = myArray[index-1] + inc
	}
	myArray[count-1] = end
	return myArray

}
