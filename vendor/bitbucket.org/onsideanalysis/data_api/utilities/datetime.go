package utilities

import "time"

func TimeToMillis(timestamp time.Time) int64 {
	return timestamp.UnixNano() / int64(time.Millisecond)
}

func MillisToTime(millis int64) time.Time {
	return time.Unix(0, int64(millis)*int64(time.Millisecond))
}

func TimeToSeconds(timestamp time.Time) int64 {
	return timestamp.UnixNano() / int64(time.Second)
}

func SecondsToTime(seconds int64) time.Time {
	return time.Unix(seconds, 0)
}
