package protocol

import (
	"math/rand"
	"time"
)

// DefaultTimeout timeout waiting for subscription response
var DefaultTimeout = 4 * time.Second

// DefaultMaxRetries default number of reconnect attempts
var DefaultMaxRetries = 10

// DefaultMaxRequestImageRetry default number of RequestImage attempts
var DefaultMaxRequestImageRetry = 4

// DefaultMaxJitter max wait time before reconnecting (ms)
var DefaultMaxJitter = 1000

// DefaultDeltaChBufferSize buffer size to accumulate deltas
var DefaultDeltaChBufferSize = 1000

// Base period for publishing and checking for heartbeats
var HEARTBEAT_INTERVAL = 60 * time.Second

// Multiplier for HEARTBEAT_INTERVAL for what is considered a timeout
// needs to be > 2 - I have a truly marvellous demonstration of this proposition
// which this margin is too narrow to contain
var HEARBEAT_TIMEOUT_MULTIPLIER = 2.5

type Transport interface {
	// both pub/sub
	Connect() error
	Close() error

	// pub

	// SubscriptionListener establishes a channel to listen for
	// subscription requests and forwards them to registered publishers
	SubscriptionListener() error

	// RegisterPublisher registers a publisher's channel for forwarding
	// subscription requests, one channel can be used for multiple tables
	RegisterPublisher(table string, reqCh chan SubscriptionRequestMsg) error

	// UnregisterPublisher removes a publisher's channel from the list
	// of channels that subscription requests are forwarded to for a table
	UnregisterPublisher(table string)

	// RegisterDirectPublisher registers a publisher's channel for forwarding
	// direct subscription requests ie for tables publishes by specific publishers
	// which might not have been created yet
	RegisterDirectPublisher(publisher string, reqCh chan SubscriptionRequestMsg) error

	// UnregisterPublisher removes a publisher's channel from the list
	// of channels that subscription requests are forwarded to for a table
	UnregisterDirectPublisher(publisher string)

	// SendSubscriptionResponse sends a subscription response for a given
	// table to its corresponding delivery channel
	SendSubscriptionResponse(table string, replyInbox string, subResp SubscriptionResponseMsg) error

	// SendTableUpdate sends a table update to an already established
	// channel internal to the transport implementation which is
	// unaware of the potential subscribers
	SendTableUpdate(table string, update TableUpdateMsg) error

	// sub

	// RegisterSubscriber registers a subscriber's channel for
	// forwarding received deltas, one channel can be used for multiple
	// tables
	RegisterSubscriber(table string, deltaCh chan TableUpdateMsg) error

	// UnregisterSubscriber removes a subscriber's channel from the list
	// of channels that deltas are forwarded to for a table
	UnregisterSubscriber(table string, deltaCh chan TableUpdateMsg)

	// RequestImage establishes a channel to the subscription requests queue
	// and sends the given subscription request
	RequestImage(req SubscriptionRequestMsg) (SubscriptionResponseMsg, error)

	// DeltaListener establishes a channel to listen for deltas and
	// forwards them to the registered subscribers
	DeltaListener(table string) error
}

func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

func RandomString(l int) string {
	bytes := make([]byte, l)
	for i := 0; i < l; i++ {
		bytes[i] = byte(randInt(65, 90))
	}
	return string(bytes)
}

// jitter splits a duration [0, max) in millisecods in n slots
// and pseudo-randomly allocates a transmission slot
func jitter(max int, n int) time.Duration {
	return time.Duration(rand.Intn(max/n)*n) * time.Millisecond
}
