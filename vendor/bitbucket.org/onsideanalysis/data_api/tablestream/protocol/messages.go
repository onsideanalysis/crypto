// protocol message definition, ie messages sent on the wire
package protocol

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"sync"

	"gopkg.in/mgo.v2/bson"
)

type MessageType int

const (
	IMAGE MessageType = 0
	DELTA MessageType = 1
)

type SubscriptionRequestMsg struct {
	TableName  string `json:"table_name"`
	ReplyInbox string `json:"reply_inbox"`
	Publisher  string `json:"publisher"`
}

type SubscriptionResponseMsg struct {
	PublisherID bson.ObjectId `json:"publisher_id"`
	Image       []byte        `json:"image"`
	UpdateID    uint64        `json:"update_id"`
}

type TableUpdateMsg struct {
	Type        MessageType   `json:"message_type"`
	TableName   string        `json:"table_name"`
	PublisherID bson.ObjectId `json:"publisher_id"`
	UpdateID    uint64        `json:"update_id"`
	Update      []byte        `json:"update"`
}

type HeartbeatMsg struct {
	PublisherID bson.ObjectId `json:"publisher_id"`
	Timestamp   uint64        `json:"timestamp"`
}

var EMPTY_MAP_BYTES, _ = json.Marshal(make(map[string]interface{}))

var E_SUBSCRIBER_EXISTS = errors.New("Already subscribed to this table")
var E_PUBLISHER_EXISTS = errors.New("A publisher is already registered for this table")
var E_ALREADY_SUBSCRIBED = errors.New("Already subscribed")
var E_MISSING_UPDATE = errors.New("Missing update")
var E_TABLE_NOT_FOUND = errors.New("Table not found")
var E_TABLE_EXISTS = errors.New("Table already exists")
var E_MULTIPLE_DELTA_PUBLISHERS = errors.New("Received deltas from multiple publishers")
var E_IMAGE_UPDATEID_NOT_ZERO = errors.New("Received image update with id not equal to 0")

func NewTableStreamError() *TableStreamError {
	return &TableStreamError{
		errs: make(map[string]error),
	}
}

type TableStreamError struct {
	mutex sync.RWMutex
	errs  map[string]error // table name -> error
}

func (e *TableStreamError) Errors() map[string]error {
	e.mutex.RLock()
	defer e.mutex.RUnlock()

	return e.errs
}

func (e *TableStreamError) Error() string {
	e.mutex.RLock()
	defer e.mutex.RUnlock()

	var errorMsgs []string
	for tbl, err := range e.errs {
		errorMsgs = append(errorMsgs, fmt.Sprintf("%s: %s", tbl, err.Error()))
	}
	return strings.Join(errorMsgs, "\n")
}

func (e *TableStreamError) Tables() []string {
	e.mutex.RLock()
	defer e.mutex.RUnlock()

	tables := make([]string, 0, len(e.errs))
	for tbl := range e.errs {
		tables = append(tables, tbl)
	}

	return tables
}

func (e *TableStreamError) TableError(tableName string) error {
	e.mutex.RLock()
	defer e.mutex.RUnlock()

	return e.errs[tableName]
}

func (e *TableStreamError) Append(tableName string, err error) {
	if err != nil {
		e.mutex.Lock()
		e.errs[tableName] = err
		e.mutex.Unlock()
	}
}

func (e *TableStreamError) IfNotNil() *TableStreamError {
	e.mutex.RLock()
	defer e.mutex.RUnlock()

	if len(e.errs) == 0 {
		return nil
	}
	return e
}
