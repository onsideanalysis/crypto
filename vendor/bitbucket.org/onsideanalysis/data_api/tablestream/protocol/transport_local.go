package protocol

import (
	"fmt"
	"log"
	"sync"
	"time"
)

type LocalTransport struct {
	publishers   map[string]chan SubscriptionRequestMsg
	publishersMu sync.RWMutex

	directPublishers   map[string]chan SubscriptionRequestMsg
	directPublishersMu sync.RWMutex

	deliveries   map[string][]chan SubscriptionResponseMsg
	deliveriesMu sync.RWMutex

	subscriptions   map[string][]chan TableUpdateMsg
	subscriptionsMu sync.RWMutex
}

// regDelivery registers a temporary listener
// for subscription responses for this table
func (lt *LocalTransport) regDelivery(table string) chan SubscriptionResponseMsg {
	// set buffer size to 1 to not block when sending subscription response
	subscriptionResponseChannel := make(chan SubscriptionResponseMsg, 1)

	lt.deliveriesMu.Lock()
	lt.deliveries[table] = append(lt.deliveries[table], subscriptionResponseChannel)
	lt.deliveriesMu.Unlock()

	return subscriptionResponseChannel
}

// unregDelivery unregisters a temporary listener
// for subscription responses for this table
func (lt *LocalTransport) unregDelivery(table string) {
	lt.deliveriesMu.Lock()
	delete(lt.deliveries, table)
	lt.deliveriesMu.Unlock()
}

// multicastSubscriptionRequestMsg checks if there are any subscription
// receivers for this table and if so sends the subscription request to
// all subscription receivers for this table
func (lt *LocalTransport) multicastSubscriptionRequestMsg(request SubscriptionRequestMsg) error {
	if len(request.Publisher) > 0 {
		// direct request
		lt.directPublishersMu.RLock()
		subscriptionReceiver, ok := lt.directPublishers[request.Publisher]
		lt.directPublishersMu.RUnlock()

		if !ok {
			return fmt.Errorf("no direct subscription publishers named %s", request.Publisher)
		}

		go func() {
			subscriptionReceiver <- request
		}()

	} else {
		// normal request
		lt.publishersMu.RLock()
		subscriptionReceiver, ok := lt.publishers[request.TableName]
		lt.publishersMu.RUnlock()

		if !ok {
			return fmt.Errorf("no subscription receivers for table %s", request.TableName)
		}

		go func() {
			subscriptionReceiver <- request
		}()
	}

	return nil
}

// waitSubscriptionResponse wait for a subscription response
func (lt *LocalTransport) waitSubscriptionResponse(request SubscriptionRequestMsg,
	subscriptionResponseChannel <-chan SubscriptionResponseMsg) (SubscriptionResponseMsg, error) {
	select {
	case subscriptionResponse := <-subscriptionResponseChannel:
		log.Printf("Received initial image for table %s from publisher %s",
			request.TableName, subscriptionResponse.PublisherID)

		return subscriptionResponse, nil
	case <-time.After(DefaultTimeout):
		return SubscriptionResponseMsg{}, fmt.Errorf(
			"timed out waiting for subscription response for table %s", request.TableName)
	}
}

func NewLocalTransport() *LocalTransport {
	return &LocalTransport{
		publishers:       make(map[string]chan SubscriptionRequestMsg),
		directPublishers: make(map[string]chan SubscriptionRequestMsg),
		deliveries:       make(map[string][]chan SubscriptionResponseMsg),
		subscriptions:    make(map[string][]chan TableUpdateMsg),
	}
}

func (lt *LocalTransport) Connect() error {
	return nil
}

func (lt *LocalTransport) RegisterSubscriber(table string, deltaCh chan TableUpdateMsg) error {
	lt.subscriptionsMu.Lock()
	defer lt.subscriptionsMu.Unlock()

	for _, ch := range lt.subscriptions[table] {
		if ch == deltaCh {
			return E_SUBSCRIBER_EXISTS
		}
	}

	lt.subscriptions[table] = append(lt.subscriptions[table], deltaCh)
	return nil
}

func (lt *LocalTransport) UnregisterSubscriber(table string, deltaCh chan TableUpdateMsg) {
	lt.subscriptionsMu.Lock()
	for i, ch := range lt.subscriptions[table] {
		if ch == deltaCh {
			lt.subscriptions[table] = append(lt.subscriptions[table][:i], lt.subscriptions[table][i+1:]...)
			if len(lt.subscriptions[table]) == 0 {
				delete(lt.subscriptions, table)
			}
			break
		}
	}
	lt.subscriptionsMu.Unlock()
}

func (lt *LocalTransport) RequestImage(req SubscriptionRequestMsg) (SubscriptionResponseMsg, error) {
	log.Printf("Creating new subscription for %s", req.TableName)

	subscriptionResponseChannel := lt.regDelivery(req.TableName)
	if err := lt.multicastSubscriptionRequestMsg(req); err != nil {
		lt.unregDelivery(req.TableName)
		return SubscriptionResponseMsg{}, err
	}

	return lt.waitSubscriptionResponse(req, subscriptionResponseChannel)
}

func (lt *LocalTransport) DeltaListener(table string) error {
	return nil
}

func (lt *LocalTransport) RegisterPublisher(
	tableName string, subscriptionRequestChan chan SubscriptionRequestMsg) error {
	lt.publishersMu.Lock()
	defer lt.publishersMu.Unlock()

	if _, ok := lt.publishers[tableName]; ok {
		return E_PUBLISHER_EXISTS
	}

	lt.publishers[tableName] = subscriptionRequestChan
	return nil
}

func (lt *LocalTransport) UnregisterPublisher(table string) {
	lt.publishersMu.Lock()
	delete(lt.publishers, table)
	lt.publishersMu.Unlock()
}

func (lt *LocalTransport) RegisterDirectPublisher(
	publisherName string, subscriptionRequestChan chan SubscriptionRequestMsg) error {
	lt.directPublishersMu.Lock()
	defer lt.directPublishersMu.Unlock()

	if _, ok := lt.directPublishers[publisherName]; ok {
		return E_PUBLISHER_EXISTS
	}

	lt.directPublishers[publisherName] = subscriptionRequestChan
	return nil
}

func (lt *LocalTransport) UnregisterDirectPublisher(publisherName string) {
	lt.directPublishersMu.Lock()
	delete(lt.directPublishers, publisherName)
	lt.directPublishersMu.Unlock()
}

func (lt *LocalTransport) SubscriptionListener() error {
	return nil
}

func (lt *LocalTransport) SendTableUpdate(table string, tableUpdate TableUpdateMsg) error {
	lt.subscriptionsMu.RLock()
	defer lt.subscriptionsMu.RUnlock()

	if tableUpdateChannels, ok := lt.subscriptions[table]; ok {
		for _, tableUpdateChannel := range tableUpdateChannels {
			select {
			case tableUpdateChannel <- tableUpdate:
			default:
				log.Printf("[WARN] Channel is full, dropping table update")
			}
		}
	}

	return nil
}

func (lt *LocalTransport) SendSubscriptionResponse(
	tableName string, replyInbox string, subscriptionResponse SubscriptionResponseMsg) error {
	lt.deliveriesMu.Lock()
	if deliveries, ok := lt.deliveries[tableName]; ok {
		for _, ch := range deliveries {
			select {
			case ch <- subscriptionResponse:
			default:
				log.Printf("[WARN] Channel is full, dropping current subscription response")
			}

		}
	}
	delete(lt.deliveries, tableName)
	lt.deliveriesMu.Unlock()

	return nil
}

func (lt *LocalTransport) Close() error {
	return nil
}
