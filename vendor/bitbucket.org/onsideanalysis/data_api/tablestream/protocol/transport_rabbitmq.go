package protocol

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"sync"
	"time"

	"github.com/streadway/amqp"
	"net"
)

type amqpChanOnce struct {
	ch      *amqp.Channel
	closeCh chan bool

	once *sync.Once
	mu   *sync.Mutex
}

func (a *amqpChanOnce) Reset() {
	a.mu.Lock()
	defer a.mu.Unlock()

	a.ch = nil

	if a.closeCh != nil {
		close(a.closeCh)
	}

	a.closeCh = make(chan bool)
	a.once = &sync.Once{}
}

func (a *amqpChanOnce) Do(amqpConn *amqp.Connection) (bool, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	done := false
	a.once.Do(func() {
		done = true
	})

	if !done {
		return done, nil
	}

	ch, err := amqpConn.Channel()
	if err != nil {
		return done, err
	}
	a.ch = ch

	return done, nil
}

var deltaExchangeTopicPrefix = "tablestream"
var subscriptionExchange = "tablestream_subscriptions"
var deltaExchange = "tablestream_deltas"

var defaultConnectionTimeout = 5 * time.Second
var defaultHeartbeat = 10 * time.Second
var defaultLocale = "en_US"

var exConf = []struct {
	name  string
	_type string
}{
	{subscriptionExchange, "fanout"},
	{deltaExchange, "topic"},
}

type RabbitMQTransport struct {
	// rabbitmq nodes to connect to
	urls []string

	// true if a connection has ever been made
	hasConnected bool

	amqpConn   *amqp.Connection
	amqpConnMu sync.RWMutex

	// amqp channels to be established
	// once per connection establishment
	amqpSubRecvHbs      *amqpChanOnce
	amqpSubRecvDeltas   map[string]*amqpChanOnce
	amqpSubRecvDeltasMu sync.RWMutex
	amqpPubSendDeltas   *amqpChanOnce
	amqpPubSendResp     *amqpChanOnce
	amqpPubRecv         *amqpChanOnce

	// table to slice of channels to forward subcription
	// requests
	publishers   map[string]chan SubscriptionRequestMsg
	publishersMu sync.RWMutex

	directPublishers   map[string]chan SubscriptionRequestMsg
	directPublishersMu sync.RWMutex

	// table to slice of (temporary) amqp.Delivery
	// channels to forward subscription responses.
	// an entry is added when a subscription request
	// is received and deleted when a subscription
	// response is sent.
	deliveries   map[string][]amqp.Delivery
	deliveriesMu sync.RWMutex

	// table to slice of channels to forward deltas
	subscriptions   map[string][]chan TableUpdateMsg
	subscriptionsMu sync.RWMutex

	// number of retries done in a reconnect loop
	retries int

	// max number of retries Transport will try to
	// re-connect
	maxRetries int

	// used internally to stop monitoring the connection
	stopCh chan bool

	// chan to signal permission for listeners to re-start
	enable chan bool

	// only declare exchanges once
	exDeclareOnce sync.Once
}

// this has been copied from streadway amqp code, so that we can control the default timeout
func dial(network, addr string) (net.Conn, error) {
	conn, err := net.DialTimeout(network, addr, defaultConnectionTimeout)
	if err != nil {
		return nil, err
	}

	// Heartbeating hasn't started yet, don't stall forever on a dead server.
	// A deadline is set for TLS and AMQP handshaking. After AMQP is established,
	// the deadline is cleared in openComplete.
	if err := conn.SetDeadline(time.Now().Add(defaultConnectionTimeout)); err != nil {
		return nil, err
	}

	return conn, nil
}

func NewRabbitMQTransport(urls []string) *RabbitMQTransport {
	return &RabbitMQTransport{
		urls:              urls,
		hasConnected:      false,
		amqpSubRecvDeltas: make(map[string]*amqpChanOnce),
		amqpSubRecvHbs:    &amqpChanOnce{once: &sync.Once{}, mu: &sync.Mutex{}},
		amqpPubSendDeltas: &amqpChanOnce{once: &sync.Once{}, mu: &sync.Mutex{}},
		amqpPubSendResp:   &amqpChanOnce{once: &sync.Once{}, mu: &sync.Mutex{}},
		amqpPubRecv:       &amqpChanOnce{once: &sync.Once{}, mu: &sync.Mutex{}},
		publishers:        make(map[string]chan SubscriptionRequestMsg),
		directPublishers:  make(map[string]chan SubscriptionRequestMsg),
		deliveries:        make(map[string][]amqp.Delivery),
		subscriptions:     make(map[string][]chan TableUpdateMsg),
		maxRetries:        DefaultMaxRetries,
		stopCh:            make(chan bool),
		enable:            make(chan bool),
	}
}

func (r *RabbitMQTransport) Connect() error {
	if !r.hasConnected {
		for _, url := range r.urls {
			log.Printf("Connecting to RabbitMQ at %s...", url)
			if conn, err := amqp.DialConfig(url, amqp.Config{
				Heartbeat: defaultHeartbeat,
				Locale:    defaultLocale,
				Dial:      dial,
			}); err == nil {
				log.Printf("Connected to RabbitMQ at %s...", url)
				r.amqpConn = conn
				r.hasConnected = true
				break
			}
		}
		if !r.hasConnected {
			return fmt.Errorf("Failed to connect to any of the given nodes: %v", r.urls)
		}
	} else {
		r.retries++
		rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
		nextDial := r.urls[rnd.Intn(len(r.urls))]
		log.Printf("Connecting to RabbitMQ at %s...", nextDial)
		conn, err := amqp.DialConfig(nextDial, amqp.Config{
			Heartbeat: defaultHeartbeat,
			Locale:    defaultLocale,
			Dial:      dial,
		})
		if err != nil {
			return err
		}
		log.Printf("Connected to RabbitMQ at %s...", nextDial)
		r.amqpConn = conn
	}

	r.retries = 0

	go func() {
		select {
		case err := <-r.amqpConn.NotifyClose(make(chan *amqp.Error)):
			r.amqpConnMu.Lock()
			log.Printf("[ERR] NotifyClose: %v ", err)

			// reset channels to enable re-establishment
			r.amqpSubRecvHbs.Reset()
			r.amqpSubRecvDeltasMu.Lock()
			for _, ch := range r.amqpSubRecvDeltas {
				ch.Reset()
			}
			r.amqpSubRecvDeltasMu.Unlock()

			r.amqpPubSendDeltas.Reset()
			r.amqpPubSendResp.Reset()
			r.amqpPubRecv.Reset()

			if err := r.reconnect(); err != nil {
				log.Print(err)
			}

			select {
			case r.enable <- true:
			default:
			}

			select {
			case r.enable <- true:
			default:
			}

			r.amqpConnMu.Unlock()
		case <-r.stopCh:
			return
		}
	}()

	return nil
}

func (r *RabbitMQTransport) Close() error {
	close(r.stopCh)

	if r.amqpConn != nil {
		if err := r.amqpConn.Close(); err != nil {
			return err
		}
	}

	return nil
}

func (r *RabbitMQTransport) SubscriptionListener() (err error) {

	r.amqpConnMu.RLock()
	done, err := r.amqpPubRecv.Do(r.amqpConn)
	r.amqpConnMu.RUnlock()

	if err != nil || !done {
		return err
	}

	msgs, err := r.bindAMQPConsumer(r.amqpPubRecv, subscriptionExchange, "fanout", "")
	if err != nil {
		return fmt.Errorf("Could not bind subscription listener: %v", err)
	}

	go func() {
		for d := range msgs {
			var req SubscriptionRequestMsg
			if err := json.Unmarshal(d.Body, &req); err != nil {
				log.Printf("[ERROR] Could not unmarshal subscription request msg: %v", err)
			} else {
				r.regDelivery(req.TableName, d)
				if err := r.multicastSubscription(req); err != nil {
					r.unregDelivery(req.TableName)
				}
			}
		}

		// wait here for permission to re-start
		<-r.enable

		if err := r.SubscriptionListener(); err != nil {
			log.Printf("Failed to re-start subscription listener: %v", err)
		}
	}()

	return nil
}

func (r *RabbitMQTransport) RegisterPublisher(table string, reqCh chan SubscriptionRequestMsg) error {
	r.publishersMu.Lock()
	defer r.publishersMu.Unlock()

	if _, ok := r.publishers[table]; ok {
		return E_PUBLISHER_EXISTS
	}

	r.publishers[table] = reqCh
	return nil
}

func (r *RabbitMQTransport) UnregisterPublisher(table string) {
	r.publishersMu.Lock()
	delete(r.publishers, table)
	r.publishersMu.Unlock()
}

func (r *RabbitMQTransport) RegisterDirectPublisher(
	publisherName string, subscriptionRequestChan chan SubscriptionRequestMsg) error {
	r.directPublishersMu.Lock()
	defer r.directPublishersMu.Unlock()

	if _, ok := r.directPublishers[publisherName]; ok {
		return E_PUBLISHER_EXISTS
	}

	r.directPublishers[publisherName] = subscriptionRequestChan
	return nil
}

func (r *RabbitMQTransport) UnregisterDirectPublisher(publisherName string) {
	r.directPublishersMu.Lock()
	delete(r.directPublishers, publisherName)
	r.directPublishersMu.Unlock()
}

func (r *RabbitMQTransport) SendSubscriptionResponse(
	table string, replyInbox string, subResp SubscriptionResponseMsg) error {

	r.amqpConnMu.RLock()
	_, err := r.amqpPubSendResp.Do(r.amqpConn)
	r.amqpConnMu.RUnlock()

	if err != nil {
		return err
	}

	r.deliveriesMu.Lock()
	defer r.deliveriesMu.Unlock()

	delvs, ok := r.deliveries[table]
	if !ok {
		return nil
	}

	body, err := json.Marshal(subResp)
	if err != nil {
		return fmt.Errorf("Failed to marshal subscription response: %v (%s)", err, table)
	}

	for _, delv := range delvs {
		err = r.amqpPubSendResp.ch.Publish(
			"",           // exchange
			delv.ReplyTo, // routing key
			false,        // mandatory
			false,        // immediate
			amqp.Publishing{
				ContentType:   "application/json",
				CorrelationId: delv.CorrelationId,
				Body:          body,
				Expiration:    strconv.Itoa(5000),
			})
		if err != nil {
			return fmt.Errorf("Failed to publish subscription response: %v (%s)", err, table)
		}
	}

	delete(r.deliveries, table)

	return nil
}

func (r *RabbitMQTransport) SendTableUpdate(table string, msg TableUpdateMsg) error {

	r.amqpConnMu.Lock()
	_, err := r.amqpPubSendDeltas.Do(r.amqpConn)
	r.amqpConnMu.Unlock()

	if err != nil {
		return err
	}

	body, err := json.Marshal(msg)
	if err != nil {
		return fmt.Errorf("Failed to marshal table update: %v (%s)", err, table)
	}

	var ex, key string
	if msg.Type == DELTA || msg.Type == IMAGE {
		ex = deltaExchange
		key = fmt.Sprintf("%s.%s", deltaExchangeTopicPrefix, table)
	} else {
		panic(fmt.Sprintf("Urecognized message type: %v", msg.Type))
	}

	r.amqpConnMu.RLock()
	err = r.amqpPubSendDeltas.ch.Publish(
		ex,    // exchange
		key,   // routing key
		false, // mandatory
		false, // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte(body),
		})
	r.amqpConnMu.RUnlock()

	if err != nil {
		return err
	}

	return nil
}

func (r *RabbitMQTransport) amqpRPC(ex string, msg interface{}, resp interface{}) error {

	r.amqpConnMu.RLock()
	ch, err := r.amqpConn.Channel()
	r.amqpConnMu.RUnlock()

	if err != nil {
		return err
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"",    // name
		false, // durable
		true,  // delete when unused
		true,  // exclusive
		false, // noWait
		nil,   // arguments
	)
	if err != nil {
		err = fmt.Errorf("Failed to declare a (callback) queue: %v", err)

		return err
	}

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		true,   // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	if err != nil {
		err = fmt.Errorf("Failed to register a consumer: %v", err)

		return err
	}

	corrID := RandomString(32)
	body, err := json.Marshal(msg)
	if err != nil {
		err = fmt.Errorf("Failed to marshal subscription request: %v", err)

		return err
	}

	err = ch.Publish(
		ex,    // exchange
		"",    // routing key
		false, // mandatory
		false, // immediate
		amqp.Publishing{
			ContentType:   "application/json",
			CorrelationId: corrID,
			ReplyTo:       q.Name,
			Body:          body,
		})
	if err != nil {
		err = fmt.Errorf("Failed to publish subscription request: %v ", err)

		return err
	}

	select {
	case d := <-msgs:
		if corrID == d.CorrelationId {
			if err := json.Unmarshal(d.Body, resp); err != nil {
				return fmt.Errorf("Failed to unmarshal body: %v", err)
			}
		}
	case <-time.After(DefaultTimeout):
		return fmt.Errorf("[ERR] Timeout waiting for RPC response")
	}

	return err
}

func (r *RabbitMQTransport) RequestImage(req SubscriptionRequestMsg) (
	SubscriptionResponseMsg, error) {

	var resp SubscriptionResponseMsg
	retries := 0

retry:
	err := r.amqpRPC(subscriptionExchange, req, &resp)

	if err != nil {
		retries++
		if retries < DefaultMaxRequestImageRetry {
			time.Sleep(jitter(DefaultMaxJitter, 50))

			goto retry
		}

		// give up
		return resp, err
	}

	return resp, err
}

func (r *RabbitMQTransport) RegisterSubscriber(table string, deltaCh chan TableUpdateMsg) error {
	r.subscriptionsMu.Lock()
	defer r.subscriptionsMu.Unlock()

	for _, ch := range r.subscriptions[table] {
		if ch == deltaCh {
			return E_SUBSCRIBER_EXISTS
		}
	}

	r.subscriptions[table] = append(r.subscriptions[table], deltaCh)
	return nil
}

func (r *RabbitMQTransport) UnregisterSubscriber(table string, deltaCh chan TableUpdateMsg) {
	r.subscriptionsMu.Lock()
	for i, ch := range r.subscriptions[table] {
		if ch == deltaCh {
			r.subscriptions[table] = append(r.subscriptions[table][:i], r.subscriptions[table][i+1:]...)
			if len(r.subscriptions[table]) == 0 {
				delete(r.subscriptions, table)

				r.amqpSubRecvDeltasMu.Lock()
				if ch, ok := r.amqpSubRecvDeltas[table]; ok {
					close(ch.closeCh)
					delete(r.amqpSubRecvDeltas, table)
				}
				r.amqpSubRecvDeltasMu.Unlock()
			}
			break
		}
	}
	r.subscriptionsMu.Unlock()
}

func (r *RabbitMQTransport) DeltaListener(table string) error {

	r.amqpSubRecvDeltasMu.RLock()
	ch, ok := r.amqpSubRecvDeltas[table]
	r.amqpSubRecvDeltasMu.RUnlock()

	if !ok {
		r.amqpSubRecvDeltasMu.Lock()
		ch = &amqpChanOnce{once: &sync.Once{}, mu: &sync.Mutex{}, closeCh: make(chan bool)}
		r.amqpSubRecvDeltas[table] = ch
		r.amqpSubRecvDeltasMu.Unlock()
	}

	r.amqpConnMu.RLock()
	done, err := ch.Do(r.amqpConn)
	r.amqpConnMu.RUnlock()
	if err != nil {
		return err
	}

	if !done {
		return nil
	}

	routingKey := fmt.Sprintf("tablestream.%s", table)
	msgs, err := r.bindAMQPConsumer(ch, deltaExchange, "topic", routingKey)
	if err != nil {
		return fmt.Errorf("Could not bind delta listener: %v", err)
	}

	go func() {
		defer ch.ch.Close()

		for {
			select {
			case d, ok := <-msgs:
				if ok {
					var delta TableUpdateMsg

					r.subscriptionsMu.RLock()
					chs, has := r.subscriptions[table]
					if !has {
						r.subscriptionsMu.RUnlock()
						continue
					}

					if err := json.Unmarshal(d.Body, &delta); err != nil {
						log.Printf("Failed to unmarshal body: %v (%s)", err, table)
						continue
					}

					for _, c := range chs {
						select {
						case c <- delta:
						default:
							// make sure we never hold
							// the lock indefinitely
						}
					}
					r.subscriptionsMu.RUnlock()
				} else {
					// wait here for permission to re-start
					<-r.enable
					r.DeltaListener(table)
				}
			case <-ch.closeCh:
				return
			}
		}
	}()

	return nil
}

func (r *RabbitMQTransport) exchangeDeclare(ch *amqpChanOnce) {
	for _, conf := range exConf {
		err := ch.ch.ExchangeDeclare(
			conf.name,  // name
			conf._type, // type
			false,      // durable
			false,      // auto-deleted
			false,      // internal
			false,      // no-wait
			nil,        // arguments
		)
		if err != nil {
			panic(fmt.Errorf("ExchangeDeclare(): %v (%s)", err, conf.name))
		}
	}
}

func (r *RabbitMQTransport) bindAMQPConsumer(
	ch *amqpChanOnce, exchange, _type, routingKey string) (<-chan amqp.Delivery, error) {

	r.amqpConnMu.RLock()
	defer r.amqpConnMu.RUnlock()

	r.exDeclareOnce.Do(func() {
		r.exchangeDeclare(ch)
	})

	q, err := ch.ch.QueueDeclare(
		"",    // name
		false, // durable
		true,  // delete when usused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		return nil, fmt.Errorf("QueueDeclare(): %v", err)
	}

	err = ch.ch.QueueBind(
		q.Name,     // queue name
		routingKey, // routing key
		exchange,   // exchange
		false,
		nil,
	)
	if err != nil {
		return nil, fmt.Errorf("QueueBind(): %v", err)
	}

	msgs, err := ch.ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto ack
		true,   // exclusive
		false,  // no local
		false,  // no wait
		nil,    // args
	)

	if err != nil {
		return nil, fmt.Errorf("Consume(): %v", err)
	}

	return msgs, nil
}

// multicastSubscription checks for subscription receivers for this
// table and if any found, it sends them the subscription request
func (r *RabbitMQTransport) multicastSubscription(req SubscriptionRequestMsg) error {
	var publisher chan SubscriptionRequestMsg
	var ok bool

	if len(req.Publisher) > 0 {
		r.directPublishersMu.RLock()
		publisher, ok = r.directPublishers[req.Publisher]
		r.directPublishersMu.RUnlock()

		if !ok {
			return fmt.Errorf("No direct subscription receivers named %s", req.Publisher)
		}

	} else {
		r.publishersMu.RLock()
		publisher, ok = r.publishers[req.TableName]
		r.publishersMu.RUnlock()

		if !ok {
			return fmt.Errorf("No subscription receivers for table %s", req.TableName)
		}
	}

	select {
	case publisher <- req:
	default:
		log.Printf("[WARN] Publisher's channel is full, dropping request for: %s", req.TableName)
	}

	return nil
}

func (r *RabbitMQTransport) unregDelivery(table string) {
	r.deliveriesMu.Lock()
	delete(r.deliveries, table)
	r.deliveriesMu.Unlock()
}

func (r *RabbitMQTransport) regDelivery(table string, d amqp.Delivery) {
	r.deliveriesMu.Lock()
	r.deliveries[table] = append(r.deliveries[table], d)
	r.deliveriesMu.Unlock()
}

func (r *RabbitMQTransport) reconnect() error {
	if r.retries <= r.maxRetries {
		time.Sleep(jitter(DefaultMaxJitter, 50))

		if err := r.Connect(); err != nil {
			log.Printf("Could not reconnect: %v", err)

			return r.reconnect()
		}
	} else {
		panic("Max retries reached. Giving up now!")
	}

	return nil
}
