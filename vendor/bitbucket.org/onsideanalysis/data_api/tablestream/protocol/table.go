package protocol

import (
	"bytes"
	"encoding/json"
	"log"
	"sync"
	"time"
)

type Table struct {
	name string

	mutex          sync.RWMutex
	updateID       uint64
	lastImage      map[string]interface{}
	lastImageBytes []byte
	lastUpdateTime time.Time
}

func NewTable(name string) *Table {
	return &Table{
		name:           name,
		lastImage:      make(map[string]interface{}),
		lastImageBytes: EMPTY_MAP_BYTES,
		lastUpdateTime: time.Now(),
	}
}

func (t *Table) SetImage(image map[string]interface{}) ([]byte, error) {
	t.mutex.Lock()
	defer t.mutex.Unlock()

	if image == nil {
		image = make(map[string]interface{})
	}

	imageBytes, err := json.Marshal(image)
	if err != nil {
		return nil, err
	}

	t.lastImageBytes = imageBytes
	t.lastImage = image
	t.updateID = 0
	t.lastUpdateTime = time.Now()

	return imageBytes, nil
}

func (t *Table) SetImageBytes(imageBytes []byte, updateId uint64) (map[string]interface{}, error) {
	t.mutex.Lock()
	defer t.mutex.Unlock()

	var image map[string]interface{}
	if err := json.Unmarshal(imageBytes, &image); err != nil {
		return nil, err
	}

	t.lastImageBytes = imageBytes
	t.lastImage = image
	t.updateID = updateId
	t.lastUpdateTime = time.Now()

	return image, nil
}

func (t *Table) GetImage() (map[string]interface{}, []byte, uint64) {
	t.mutex.Lock()
	defer t.mutex.Unlock()

	if t.lastImageBytes == nil {
		lastImageBytes, err := json.Marshal(t.lastImage)
		if err != nil {
			log.Panicf("Failed to lazily marshal lastImage: %s", err.Error())
		}
		t.lastImageBytes = lastImageBytes
	}

	return t.lastImage, t.lastImageBytes, t.updateID
}

func (t *Table) LastUpdateTime() time.Time {
	t.mutex.RLock()
	defer t.mutex.RUnlock()

	return t.lastUpdateTime
}

func (t *Table) ApplyDelta(update TableUpdateMsg) (map[string]interface{}, error) {
	t.mutex.Lock()
	defer t.mutex.Unlock()

	if update.UpdateID <= t.updateID {
		log.Printf("[table:%s] Dropping stale delta %d", update.TableName, update.UpdateID)
		return nil, nil
	} else if update.UpdateID != t.updateID+1 {
		log.Printf("[table:%s] Error: Missed delta. Got %d but expected %d", update.TableName, update.UpdateID, t.updateID+1)
		return nil, E_MISSING_UPDATE
	}

	if bytes.Compare(update.Update, EMPTY_MAP_BYTES) == 0 {
		log.Printf("[table:%s] Received heartbeat delta %d", update.TableName, update.UpdateID)
		t.updateID = update.UpdateID
		t.lastUpdateTime = time.Now()
		return nil, nil
	} else {
		log.Printf("[table:%s] Applying delta %d", update.TableName, update.UpdateID)

		var changes map[string]interface{}
		if err := json.Unmarshal(update.Update, &changes); err != nil {
			return nil, err
		}

		for key, val := range changes {
			if val != nil {
				t.lastImage[key] = val
			} else {
				delete(t.lastImage, key)
			}
		}

		t.lastImageBytes = nil
		t.updateID = update.UpdateID
		t.lastUpdateTime = time.Now()

		return changes, nil
	}
}

func (t *Table) Update(updates map[string]interface{}) ([]byte, uint64, error) {
	t.mutex.Lock()
	defer t.mutex.Unlock()

	deltaBytes, err := json.Marshal(updates)
	if err != nil {
		return nil, 0, err
	}

	for key, val := range updates {
		if val == nil {
			delete(t.lastImage, key)
		} else {
			t.lastImage[key] = val
		}
	}

	t.lastImageBytes = nil
	t.updateID++
	t.lastUpdateTime = time.Now()

	return deltaBytes, t.updateID, nil
}

func (t *Table) EmptyDelta() uint64 {
	t.mutex.Lock()
	defer t.mutex.Unlock()

	t.updateID++
	t.lastUpdateTime = time.Now()

	return t.updateID
}

func (t *Table) ResetUpdateId() {
	t.mutex.Lock()
	t.updateID = 0
	t.lastUpdateTime = time.Now()
	t.mutex.Unlock()
}
