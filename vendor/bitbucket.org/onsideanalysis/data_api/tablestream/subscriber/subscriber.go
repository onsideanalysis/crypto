package subscriber

import (
	"log"
	"sync"
	"time"

	"bitbucket.org/onsideanalysis/data_api/tablestream/protocol"
	"gopkg.in/mgo.v2/bson"
)

var MAX_WORKERS = 100
var PENDING_UPDATES_BUFFER_SIZE = 1000 // per table

type TableStreamSubscriber struct {
	id                         bson.ObjectId
	transport                  protocol.Transport
	heartbeatInterval          time.Duration
	heartbeatTimeoutMultiplier float64

	wg       *sync.WaitGroup
	quitChan chan struct{}

	updatesChannel chan protocol.TableUpdateMsg

	subscribedTablesMutex sync.RWMutex
	subscribedTables      map[string]struct{}

	pendingDeltasMutex    sync.RWMutex
	pendingDeltasChannels map[string]chan protocol.TableUpdateMsg

	listenersMutex sync.RWMutex
	listeners      []TableStreamListener

	workersChannel   chan *SubscriptionRequest
	spawnWorkersOnce sync.Once

	heartbeatsMutex sync.RWMutex
	lastTableStatus map[string]bool

	mutex        sync.RWMutex
	publisherIds map[string]bson.ObjectId
	tables       map[string]*protocol.Table
}

type SubscriptionRequest struct {
	wg          *sync.WaitGroup
	table       string
	publisher   string
	unsubscribe bool
	err         *protocol.TableStreamError
}

func (t *TableStreamSubscriber) requestImage(tableName, publisher string) (protocol.SubscriptionResponseMsg, error) {
	replyInbox := tableName + "_subscriber_" + t.id.Hex()
	req := protocol.SubscriptionRequestMsg{
		TableName:  tableName,
		ReplyInbox: replyInbox,
		Publisher:  publisher,
	}

	return t.transport.RequestImage(req)
}

func (t *TableStreamSubscriber) updateImage(tableName string, publisherId bson.ObjectId, imgBytes []byte, updateId uint64) error {
	newTable := protocol.NewTable(tableName)

	t.mutex.Lock()
	t.publisherIds[tableName] = publisherId
	t.tables[tableName] = newTable
	t.mutex.Unlock()

	image, err := newTable.SetImageBytes(imgBytes, updateId)
	if err != nil {
		return err
	}

	log.Printf("[table:%s] Got image with update id %d from publisher %s", tableName, updateId, publisherId.Hex())

	t.notifyImage(tableName, image)

	return nil
}

func (t *TableStreamSubscriber) checkTablesAlive() {
	defer t.wg.Done()

	ticker := time.NewTicker(t.heartbeatInterval)
	cutoff := time.Duration(float64(t.heartbeatInterval) * t.heartbeatTimeoutMultiplier)

	for {
		select {
		case <-ticker.C:
			t.mutex.RLock()
			timeNow := time.Now()
			for tableName, tbl := range t.tables {
				hbUp := false
				if tbl.LastUpdateTime().After(timeNow.Add(-cutoff)) {
					hbUp = true
				}
				t.heartbeatsMutex.Lock()
				if hbUp != t.lastTableStatus[tableName] {
					t.lastTableStatus[tableName] = hbUp
					t.notifyHeartbeatStatus(tableName, hbUp)
				}
				t.heartbeatsMutex.Unlock()
			}
			t.mutex.RUnlock()
		case <-t.quitChan:
			return
		}
	}
}

func NewTableStreamSubscriber(transport protocol.Transport, heartbeatInterval time.Duration, heartbeatTimeoutMultiplier float64) *TableStreamSubscriber {
	return &TableStreamSubscriber{
		id:                         bson.NewObjectId(),
		transport:                  transport,
		heartbeatInterval:          heartbeatInterval,
		heartbeatTimeoutMultiplier: heartbeatTimeoutMultiplier,
		wg:                    &sync.WaitGroup{},
		quitChan:              make(chan struct{}),
		updatesChannel:        make(chan protocol.TableUpdateMsg, 100),
		publisherIds:          make(map[string]bson.ObjectId),
		tables:                make(map[string]*protocol.Table),
		subscribedTables:      make(map[string]struct{}),
		pendingDeltasChannels: make(map[string]chan protocol.TableUpdateMsg),
		workersChannel:        make(chan *SubscriptionRequest),
		lastTableStatus:       make(map[string]bool),
	}
}

func (t *TableStreamSubscriber) Unsubscribe(tables []string) *protocol.TableStreamError {
	wg := &sync.WaitGroup{}
	wg.Add(len(tables))

	err := protocol.NewTableStreamError()

	for _, tbl := range tables {
		t.workersChannel <- &SubscriptionRequest{
			table:       tbl,
			wg:          wg,
			unsubscribe: true,
			err:         err,
		}
	}

	wg.Wait()

	// unsubscribeImpl never returns an error so just remove all tables passed to Unsubscribe
	t.subscribedTablesMutex.Lock()
	for _, tbl := range tables {
		delete(t.subscribedTables, tbl)
	}
	t.subscribedTablesMutex.Unlock()

	log.Printf("Unsubscribed from tables %+v", tables)
	return err.IfNotNil()
}

func (t *TableStreamSubscriber) unsubscribeImpl(table string) error {
	t.transport.UnregisterSubscriber(table, t.updatesChannel)

	t.mutex.Lock()
	delete(t.publisherIds, table)
	delete(t.tables, table)
	t.mutex.Unlock()

	t.heartbeatsMutex.Lock()
	delete(t.lastTableStatus, table)
	t.heartbeatsMutex.Unlock()

	return nil
}

func (t *TableStreamSubscriber) Subscribe(tables []string, publisher string) *protocol.TableStreamError {
	t.spawnWorkersOnce.Do(func() {
		for i := 0; i < MAX_WORKERS; i++ {
			go t.subscriptionWorker()
		}
	})

	var alreadySubscribedTables []string
	var tablesToSubscribe []string

	for _, tbl := range tables {
		t.subscribedTablesMutex.Lock()
		if _, ok := t.subscribedTables[tbl]; ok {
			alreadySubscribedTables = append(alreadySubscribedTables, tbl)
		} else {
			tablesToSubscribe = append(tablesToSubscribe, tbl)
			t.subscribedTables[tbl] = struct{}{}
		}
		t.subscribedTablesMutex.Unlock()
	}

	wg := &sync.WaitGroup{}
	wg.Add(len(tablesToSubscribe))

	err := protocol.NewTableStreamError()

	for _, tbl := range tablesToSubscribe {
		t.workersChannel <- &SubscriptionRequest{
			table:     tbl,
			publisher: publisher,
			wg:        wg,
			err:       err,
		}
	}

	wg.Wait()

	var successfulSubscriptions []string
	var unsuccessfulSubscriptions []string

	for _, tbl := range tablesToSubscribe {
		if err.TableError(tbl) == nil {
			successfulSubscriptions = append(successfulSubscriptions, tbl)
		} else {
			t.subscribedTablesMutex.Lock()
			delete(t.subscribedTables, tbl)
			t.subscribedTablesMutex.Unlock()
			unsuccessfulSubscriptions = append(unsuccessfulSubscriptions, tbl)
		}
	}

	for _, tbl := range alreadySubscribedTables {
		err.Append(tbl, protocol.E_ALREADY_SUBSCRIBED)
	}

	unsuccessfulSubscriptions = append(unsuccessfulSubscriptions, alreadySubscribedTables...)

	if len(successfulSubscriptions) > 0 {
		log.Printf("Successfully subscribed to tables %+v", successfulSubscriptions)
	}

	if len(unsuccessfulSubscriptions) > 0 {
		log.Printf("Could not subscribe to tables %+v", unsuccessfulSubscriptions)
	}

	return err.IfNotNil()
}

func (t *TableStreamSubscriber) handleDelta(update protocol.TableUpdateMsg) error {
	t.mutex.RLock()
	tbl, has := t.tables[update.TableName]
	t.mutex.RUnlock()

	if !has {
		return protocol.E_TABLE_NOT_FOUND
	}

	changes, err := tbl.ApplyDelta(update)
	if err == nil {
		if changes != nil {
			t.notifyDelta(update.TableName, changes)
		}
	}

	return err
}

func (t *TableStreamSubscriber) subscribeImpl(tableName, publisher string) error {
	pendingDeltasChan := make(chan protocol.TableUpdateMsg, PENDING_UPDATES_BUFFER_SIZE)

	t.unsubscribeImpl(tableName)

	t.pendingDeltasMutex.Lock()
	t.pendingDeltasChannels[tableName] = pendingDeltasChan
	t.pendingDeltasMutex.Unlock()

	removePendingDeltasCh := func() {
		t.pendingDeltasMutex.Lock()
		delete(t.pendingDeltasChannels, tableName)
		t.pendingDeltasMutex.Unlock()
	}

	t.transport.RegisterSubscriber(tableName, t.updatesChannel)

	if err := t.transport.DeltaListener(tableName); err != nil {
		removePendingDeltasCh()
		t.unsubscribeImpl(tableName)
		return err
	}

	response, err := t.requestImage(tableName, publisher)
	if err != nil {
		removePendingDeltasCh()
		t.unsubscribeImpl(tableName)
		return err
	}

	if err := t.updateImage(tableName, response.PublisherID, response.Image, response.UpdateID); err != nil {
		removePendingDeltasCh()
		t.unsubscribeImpl(tableName)
		return err
	}

loop:
	for {
		t.pendingDeltasMutex.Lock()
		select {
		case pendingUpdate := <-pendingDeltasChan:
			if pendingUpdate.PublisherID != response.PublisherID {
				delete(t.pendingDeltasChannels, tableName)
				t.pendingDeltasMutex.Unlock()
				t.unsubscribeImpl(tableName)
				return protocol.E_MULTIPLE_DELTA_PUBLISHERS
			} else {
				t.pendingDeltasMutex.Unlock()
				log.Printf("[table:%s] Handling pending delta %d", tableName, pendingUpdate.UpdateID)
				if err := t.handleDelta(pendingUpdate); err != nil {
					removePendingDeltasCh()
					t.unsubscribeImpl(tableName)
					return err
				}
			}
		default:
			delete(t.pendingDeltasChannels, tableName)
			t.pendingDeltasMutex.Unlock()
			break loop
		}
	}

	return nil
}

func (t *TableStreamSubscriber) subscriptionWorker() {
	for msg := range t.workersChannel {
		if msg.unsubscribe {
			msg.err.Append(msg.table, t.unsubscribeImpl(msg.table))
		} else {
			msg.err.Append(msg.table, t.subscribeImpl(msg.table, msg.publisher))
		}
		msg.wg.Done()
	}
}

func (t *TableStreamSubscriber) Start() error {
	t.wg.Add(1)
	go func() {
		defer t.wg.Done()

		for {
			select {
			case update := <-t.updatesChannel:
				// todo - check and drop messages for tables we're no longer subscribed to
				if update.Type == protocol.IMAGE {
					if update.UpdateID != 0 {
						t.notifySubscriptionFailed(update.TableName, protocol.E_IMAGE_UPDATEID_NOT_ZERO)
						continue
					}

					if err := t.updateImage(update.TableName, update.PublisherID, update.Update, update.UpdateID); err != nil {
						t.notifySubscriptionFailed(update.TableName, err)
					}
				} else if update.Type == protocol.DELTA {
					t.pendingDeltasMutex.RLock()
					pendingUpdatesChan, hasPendingUpdatesChan := t.pendingDeltasChannels[update.TableName]
					if hasPendingUpdatesChan {
						pendingUpdatesChan <- update
					}
					t.pendingDeltasMutex.RUnlock()

					if !hasPendingUpdatesChan {
						t.mutex.RLock()
						publisherId := t.publisherIds[update.TableName]
						t.mutex.RUnlock()

						if publisherId != update.PublisherID {
							t.notifySubscriptionFailed(update.TableName, protocol.E_MULTIPLE_DELTA_PUBLISHERS)
							continue
						}

						if err := t.handleDelta(update); err != nil {
							t.notifySubscriptionFailed(update.TableName, err)
							continue
						}
					}
				}
			case <-t.quitChan:
				return
			}
		}
	}()

	t.wg.Add(1)
	go t.checkTablesAlive()

	return nil
}

func (t *TableStreamSubscriber) Stop() error {
	close(t.quitChan)
	t.wg.Wait()

	close(t.workersChannel)

	return nil
}

func (t *TableStreamSubscriber) AddListener(listener TableStreamListener) {
	t.listenersMutex.Lock()
	t.listeners = append(t.listeners, listener)
	t.listenersMutex.Unlock()
}

func (t *TableStreamSubscriber) notifyImage(tableName string, image map[string]interface{}) {
	t.listenersMutex.RLock()
	for _, listener := range t.listeners {
		listener.ImageUpdated(tableName, image)
	}
	t.listenersMutex.RUnlock()
}

func (t *TableStreamSubscriber) notifyDelta(tableName string, changes map[string]interface{}) {
	t.listenersMutex.RLock()
	for _, listener := range t.listeners {
		listener.ReceivedDelta(tableName, changes)
	}
	t.listenersMutex.RUnlock()
}

func (t *TableStreamSubscriber) notifySubscriptionFailed(tableName string, err error) {
	log.Printf("[table:%s] Subscription failed: %s", tableName, err.Error())
	t.Unsubscribe([]string{tableName})

	t.listenersMutex.RLock()
	for _, listener := range t.listeners {
		listener.SubscriptionFailed(tableName, err)
	}
	t.listenersMutex.RUnlock()
}

func (t *TableStreamSubscriber) notifyHeartbeatStatus(tableName string, up bool) {
	t.listenersMutex.RLock()
	for _, listener := range t.listeners {
		listener.HeartbeatStatus(tableName, up)
	}
	t.listenersMutex.RUnlock()
}
