package subscriber

type TableStreamListener interface {
	HeartbeatStatus(tableName string, up bool)
	SubscriptionFailed(tableName string, err error)
	ImageUpdated(tableName string, image map[string]interface{})
	ReceivedDelta(tableName string, changes map[string]interface{})
}

type TableStreamEventType int

const (
	IMAGE TableStreamEventType = iota
	DELTA
	HEARTBEAT_STATUS
	SUBSCRIPTION_FAILED
)

type TableStreamEvent struct {
	Table           string                 `json:"table"`
	Type            TableStreamEventType   `json:"type"`
	Data            map[string]interface{} `json:"data,omitempty"`
	HeartbeatStatus bool                   `json:"heartbeat_status,omitempty"`
	Error           string                 `json:"error,omitempty"`
}

type TableStreamEventAdapter struct {
	callback func(event TableStreamEvent)
}

func NewTableStreamEventAdapter(f func(event TableStreamEvent)) *TableStreamEventAdapter {
	return &TableStreamEventAdapter{
		callback: f,
	}
}

func (t *TableStreamEventAdapter) HeartbeatStatus(tableName string, up bool) {
	t.callback(TableStreamEvent{
		Table:           tableName,
		Type:            HEARTBEAT_STATUS,
		HeartbeatStatus: up,
	})
}

func (t *TableStreamEventAdapter) SubscriptionFailed(tableName string, err error) {
	t.callback(TableStreamEvent{
		Table: tableName,
		Type:  SUBSCRIPTION_FAILED,
		Error: err.Error(),
	})
}

func (t *TableStreamEventAdapter) ImageUpdated(tableName string, image map[string]interface{}) {
	t.callback(TableStreamEvent{
		Table: tableName,
		Type:  IMAGE,
		Data:  image,
	})
}

func (t *TableStreamEventAdapter) ReceivedDelta(tableName string, updates map[string]interface{}) {
	t.callback(TableStreamEvent{
		Table: tableName,
		Type:  DELTA,
		Data:  updates,
	})
}
