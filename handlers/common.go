package handlers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type Response map[string]interface{}

func SendJsonWithStatus(w http.ResponseWriter, statusCode int, data interface{}, maxAge int) {
	result, err := json.Marshal(data)
	if err != nil {
		log.Println("Error marshalling response", err)
		http.Error(w, "Internal server error.", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	if maxAge >= 0 {
		w.Header().Set("cache-control", fmt.Sprintf("max-age=%v", maxAge))
	} else {
		w.Header().Set("cache-control", "no-cache")
	}
	w.WriteHeader(statusCode)
	w.Write(result)
	return
}

func SendJson(w http.ResponseWriter, data interface{}) {
	SendJsonWithStatus(w, http.StatusOK, data, -1)
}
