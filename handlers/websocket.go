package handlers

import (
	"bitbucket.org/onsideanalysis/crypto/providers"
	"bitbucket.org/onsideanalysis/data_api/tablestream/protocol"
	"bitbucket.org/onsideanalysis/data_api/tablestream/subscriber"
	"github.com/gorilla/websocket"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
	"strings"
	"sync"
	"sync/atomic"
)

const TRADING_USER = "5b2cce9c497aee272968ffee"
const ORDERS_TABLE = "trading_user_orders_" + TRADING_USER
const ORDERS_PUBLISHER = "crypto_oms"
const RISK_TABLE = "trading_user_risksheet_" + TRADING_USER
const RISK_PUBLISHER = ""

type WebSocketHandler struct {
	upgrader       websocket.Upgrader
	allowedOrigins []string
	provider       providers.ITableStreamProvider
	numSockets     int64
}

func NewWebsocketHandler(provider providers.ITableStreamProvider, allowedOrigins []string) *WebSocketHandler {

	handler := &WebSocketHandler{
		allowedOrigins: allowedOrigins,
		provider:       provider,
	}

	handler.upgrader = websocket.Upgrader{
		EnableCompression: true,
		CheckOrigin: func(r *http.Request) bool {
			origin := r.Header.Get("Origin")
			for _, allowed := range allowedOrigins {
				if allowed == "*" {
					return true
				}
				if origin == allowed {
					return true
				}
			}
			return false
		},
	}

	return handler
}

func (ws *WebSocketHandler) HandleConnection(w http.ResponseWriter, r *http.Request) {

	// Upgrade initial GET request to a websocket
	client, err := ws.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("Upgrading", err)
		return
	}
	defer client.Close()

	// Keep track of how many connected sockets we have
	atomic.AddInt64(&ws.numSockets, 1)
	log.Println("Websockets connected:", atomic.LoadInt64(&ws.numSockets))
	defer func() {
		atomic.AddInt64(&ws.numSockets, -1)
		log.Println("Websockets connected:", atomic.LoadInt64(&ws.numSockets))
	}()

	for _, table := range []string{ORDERS_TABLE, RISK_TABLE} {
		latestImage, ok := ws.provider.GetImage(table)
		if ok {
			err = client.WriteJSON(latestImage)
			if err != nil {
				log.Println("Writing initial json", err)
				return
			}
		} else {
			log.Println("No initial image found")
		}
	}
	log.Println("Finished initial image check")

	// Listen for tablestream updates
	socketId := bson.NewObjectId().Hex()
	channel := make(chan subscriber.TableStreamEvent)
	ws.provider.AddListener(socketId, channel)

	log.Println("Creating waitgroup")
	var wg sync.WaitGroup
	wg.Add(1)

	// Start the listener before the subscription is started
	go func(group *sync.WaitGroup) {
		log.Println("Starting websocket listener for", socketId)
		for true {
			select {
			case data := <-channel:

				log.Println("Sending data", socketId)
				err = client.WriteJSON(data)
				if err != nil {
					if strings.HasSuffix(err.Error(), "broken pipe") {
						log.Println("Websocket disconnected", socketId, client.RemoteAddr())
					} else {
						log.Println("Writing json", socketId, err)
					}
					group.Done()
					return
				}
			}
		}
	}(&wg)

	subErr := ws.provider.AddSubscription(socketId, ORDERS_TABLE, ORDERS_PUBLISHER)
	defer ws.provider.RemoveListener(socketId)
	if subErr != nil && !strings.HasSuffix(subErr.Error(), protocol.E_ALREADY_SUBSCRIBED.Error()) {
		log.Println("Failed to subscribe to", ORDERS_TABLE, subErr)
		return
	}

	subErr = ws.provider.AddSubscription(socketId, RISK_TABLE, RISK_PUBLISHER)
	defer ws.provider.RemoveListener(socketId)
	if subErr != nil && !strings.HasSuffix(subErr.Error(), protocol.E_ALREADY_SUBSCRIBED.Error()) {
		log.Println("Failed to subscribe to", RISK_TABLE, subErr)
		return
	}

	wg.Wait()
	log.Println("Removing subscriptions for", socketId)
	ws.provider.RemoveSubscription(socketId, ORDERS_TABLE)
	ws.provider.RemoveSubscription(socketId, RISK_TABLE)
}
