package handlers

import (
	"bitbucket.org/onsideanalysis/crypto/providers"
	"bitbucket.org/onsideanalysis/data_api/tablestream/subscriber"
	"encoding/json"
	"github.com/gorilla/websocket"
	"log"
	"net/http/httptest"
	"os"
	"reflect"
	"strings"
	"testing"
)

func TestWebSocketHandler_HandleConnection(t *testing.T) {
	log.SetOutput(os.Stdout)

	op := providers.NewOrdersProviderMock()
	oh := NewOrdersHandler(op)

	tableStream := providers.NewTableStreamProviderMock()

	w := NewWebsocketHandler(tableStream, []string{"*"})
	mux := Router(oh, w)

	// Create test server
	ts := httptest.NewServer(mux)
	defer ts.Close()

	// Convert http://127.0.0.1 to ws://127.0.0.1
	u := "ws" + strings.TrimPrefix(ts.URL, "http") + "/ws"

	// Connect to the server
	ws, _, err := websocket.DefaultDialer.Dial(u, nil)
	if err != nil {
		t.Fatalf("%v", err)
	}
	defer ws.Close()

	_, p, err := ws.ReadMessage()
	if err != nil {
		t.Fatalf("%v", err)
	}

	var data subscriber.TableStreamEvent
	json.Unmarshal([]byte(string(p)), &data)

	var expected map[string]interface{}
	json.Unmarshal([]byte(providers.ORDER_JSON), &expected)

	if !reflect.DeepEqual(data.Data, expected) {
		t.Errorf("Websocket message does not equal expected")
	}
}
