package handlers

import (
	"bitbucket.org/onsideanalysis/crypto/providers"
	"bitbucket.org/onsideanalysis/data_api/schemas/trading"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type OrdersHandler struct {
	providers.IOrdersProvider
}

func NewOrdersHandler(provider providers.IOrdersProvider) *OrdersHandler {
	return &OrdersHandler{provider}
}

func (p *OrdersHandler) UpdateStrategy(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	var request trading.UpdateOrderStrategyMsg
	err := decoder.Decode(&request)
	if err != nil {
		msg := fmt.Sprintf("Error parsing UpdateOrderStrategyMsg: %s", err.Error())
		log.Println(msg)
		http.Error(w, msg, http.StatusBadRequest)
		return
	}

	err = p.UpdateOrdersStrategies(request)
	if err != nil {
		msg := fmt.Sprintf("Error updating strategy: %s", err.Error())
		log.Println(msg)
		http.Error(w, msg, http.StatusInternalServerError)
		return
	}
	SendJson(w, Response{"success": true})
}
