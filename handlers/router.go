package handlers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
)

func Router(o *OrdersHandler, ws *WebSocketHandler) *mux.Router {

	r := mux.NewRouter()
	r.HandleFunc("/healthcheck", TestHandler)
	r.HandleFunc("/version", VersionHandler)
	r.HandleFunc("/ws", ws.HandleConnection)
	r.HandleFunc("/orders/{id}/strategy", o.UpdateStrategy)

	return r
}

func TestHandler(w http.ResponseWriter, r *http.Request) {
}

type version struct {
	BuildNumber string
	TimeStamp   string
	Branch      string
	Commit      string
}

var BuildNumber = "undefined"
var TimeStamp = "undefined"
var Branch = "undefined"
var Commit = "undefined"

func VersionHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(version{
		BuildNumber: BuildNumber,
		TimeStamp:   TimeStamp,
		Branch:      Branch,
		Commit:      Commit,
	})
}
