package providers

import (
	"bitbucket.org/onsideanalysis/data_api/tablestream/protocol"
	"bitbucket.org/onsideanalysis/data_api/tablestream/subscriber"
	"encoding/json"
	"log"
	"os"
	"reflect"
	"sync"
	"testing"
)

func TestTableStream(t *testing.T) {
	log.SetOutput(os.Stdout)

	transport := protocol.NewLocalTransport()
	StartLocalTableStream(transport, "table")
	sub := subscriber.NewTableStreamSubscriber(transport, protocol.HEARTBEAT_INTERVAL, protocol.HEARBEAT_TIMEOUT_MULTIPLIER)
	tableStream := NewTableStreamProvider(sub)

	channel := make(chan subscriber.TableStreamEvent)
	tableStream.AddListener("listener", channel)

	var wg sync.WaitGroup
	wg.Add(1)

	go func(group *sync.WaitGroup) {
		for true {
			select {
			case data := <-channel:

				var expected map[string]interface{}
				json.Unmarshal([]byte(ORDER_JSON), &expected)

				if !reflect.DeepEqual(data.Data, expected) {
					t.Errorf("Websocket message does not equal expected")
				}
				wg.Done()
				return
			}
		}
	}(&wg)

	tableStream.AddSubscription("listener", "table", "")
	wg.Wait()
}
