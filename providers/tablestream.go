package providers

import (
	"bitbucket.org/onsideanalysis/data_api/tablestream/protocol"
	"bitbucket.org/onsideanalysis/data_api/tablestream/subscriber"
	"log"
	"sync"
)

type ITableStreamProvider interface {
	AddListener(name string, listener chan subscriber.TableStreamEvent)
	RemoveListener(name string)
	AddSubscription(listener, table, publisher string) (err *protocol.TableStreamError)
	RemoveSubscription(listener string, table string) (err *protocol.TableStreamError)
	GetImage(tableName string) (update subscriber.TableStreamEvent, ok bool)
}

type TableStreamProvider struct {
	tableStreamSubscriber *subscriber.TableStreamSubscriber

	listenersMutex        *sync.RWMutex
	listeners             map[string]chan subscriber.TableStreamEvent
	listenerSubscriptions map[string]map[string]bool // listener name -> table name

	imagesMutex *sync.RWMutex
	images      map[string]map[string]interface{}
}

func NewTableStreamProvider(sub *subscriber.TableStreamSubscriber) ITableStreamProvider {

	p := &TableStreamProvider{}
	p.listenersMutex = &sync.RWMutex{}
	p.listeners = make(map[string]chan subscriber.TableStreamEvent)
	p.listenerSubscriptions = make(map[string]map[string]bool)
	p.imagesMutex = &sync.RWMutex{}
	p.images = make(map[string]map[string]interface{})

	p.tableStreamSubscriber = sub
	p.tableStreamSubscriber.AddListener(subscriber.NewTableStreamEventAdapter(p.tableStreamEvent))
	p.tableStreamSubscriber.Start()
	return p
}

func (p *TableStreamProvider) tableStreamEvent(event subscriber.TableStreamEvent) {

	log.Printf("Got tablestream event: %+v %+v", event.Table, event.Type)

	// todo - handle heartbeat / fail
	if event.Type == subscriber.IMAGE || event.Type == subscriber.DELTA {

		p.imagesMutex.Lock()
		if event.Type == subscriber.IMAGE {
			p.images[event.Table] = event.Data
		} else if event.Type == subscriber.DELTA {
			img, has := p.images[event.Table]
			if !has {
				// todo - what do we do?
				log.Printf("Got DELTA without having an initial image!?")
			} else {
				log.Printf("Event data %+v", event.Data)
				for key, val := range event.Data {
					if val == nil {
						delete(img, key)
					} else {
						img[key] = val
					}
				}
			}
		}
		p.imagesMutex.Unlock()

	} else if event.Type == subscriber.HEARTBEAT_STATUS {
		log.Printf("Received HEARTBEAT_STATUS for table %s: %t", event.Table, event.HeartbeatStatus)
		return
	} else if event.Type == subscriber.SUBSCRIPTION_FAILED {
		log.Printf("Received SUBSCRIPTION_FAILED for table %s: %s", event.Table, event.Error)
		return
	}

	log.Println("Sending event")
	p.listenersMutex.RLock()
	for name, listener := range p.listeners {
		if _, ok := p.listenerSubscriptions[name][event.Table]; ok {
			listener <- event
		}
	}
	p.listenersMutex.RUnlock()
	log.Println("Sent event")
}

func (p *TableStreamProvider) AddListener(name string, listener chan subscriber.TableStreamEvent) {

	log.Println("Adding listener")
	p.listenersMutex.Lock()
	p.listeners[name] = listener
	p.listenerSubscriptions[name] = make(map[string]bool)
	p.listenersMutex.Unlock()
	log.Println("Added listener")
}

func (p *TableStreamProvider) RemoveListener(name string) {

	log.Println("Removing listener")
	p.listenersMutex.Lock()
	delete(p.listeners, name)
	delete(p.listenerSubscriptions, name)
	p.listenersMutex.Unlock()
	log.Println("Removed listener")
}

func (p *TableStreamProvider) AddSubscription(listener, table, publisher string) (err *protocol.TableStreamError) {

	log.Println("Adding subscription", listener, table, publisher)
	p.listenersMutex.Lock()

	if _, ok := p.listenerSubscriptions[listener]; !ok {
		//return errors.New(fmt.Sprintf("Listener does not exist %s", listener))
		return
	}
	p.listenerSubscriptions[listener][table] = true

	p.listenersMutex.Unlock()

	err = p.tableStreamSubscriber.Subscribe([]string{table}, publisher)
	log.Println("Adding subscription", listener, table, publisher)
	return
}

func (p *TableStreamProvider) RemoveSubscription(listener string, table string) (err *protocol.TableStreamError) {

	log.Println("Removing subscription", listener, table)
	p.listenersMutex.Lock()

	delete(p.listenerSubscriptions[listener], table)

	subscribed := false
	for _, subscriptions := range p.listenerSubscriptions {
		for subscription, _ := range subscriptions {
			if subscription == table {
				subscribed = true
				break
			}
		}
	}

	p.listenersMutex.Unlock()

	if !subscribed {
		err = p.tableStreamSubscriber.Unsubscribe([]string{table})
	}
	log.Println("Removed subscription", listener, table)
	return
}

func (p *TableStreamProvider) GetImage(tableName string) (update subscriber.TableStreamEvent, ok bool) {

	log.Println("Getting image for", tableName)
	p.imagesMutex.Lock()
	defer p.imagesMutex.Unlock()

	var img map[string]interface{}
	if img, ok = p.images[tableName]; ok {
		update = subscriber.TableStreamEvent{
			Table: tableName,
			Type:  subscriber.IMAGE,
			Data:  img,
		}
	}

	log.Println("Got image for", tableName)
	return
}
