package providers

import (
	"bitbucket.org/onsideanalysis/data_api/tablestream/protocol"
	"bitbucket.org/onsideanalysis/data_api/tablestream/subscriber"
	"github.com/dustin/gojson"
	"log"
	"sync"
)

const ORDER_JSON = `{"5b641d6189aa4e40ca509423":{"account_id":"5b2cce65497aee271eacb0f5","average_price_matched":0,"bet_side":"buy","crypto_raw_details":{"Symbol":"","Text":""},"currency":"","exchange_rate":1,"execution_details":{"bet_id":"402e93cf-515a-43ad-94b6-d68e230b398c","bookmaker":"GDAX","provider":"GDAX"},"id":"5b641d6189aa4e40ca509423","persistence":"","placed_time":"2018-08-02T12:49:43.184+01:00","price":1000,"price_requested":1000,"size":0.001,"size_cancelled":0,"size_lapsed":0,"size_matched":0,"size_rejected":0,"size_remaining":0,"size_voided":0,"source":"bookmaker","status":1,"sticker":"BTCUSD.SPOT","strategy":"test","strategy_descr":"test_v1","trading_user_id":"5b2cce9c497aee272968ffee","ut":"0001-01-01T00:00:00Z"}}`

type TableStreamProviderMock struct {
	transport             protocol.Transport
	listenersMutex        *sync.RWMutex
	listeners             map[string]chan subscriber.TableStreamEvent
	listenerSubscriptions map[string]map[string]bool // listener name -> table name
}

func NewTableStreamProviderMock() ITableStreamProvider {

	transport := protocol.NewLocalTransport()
	StartLocalTableStream(transport, "table_name")

	return &TableStreamProviderMock{
		transport:             transport,
		listeners:             make(map[string]chan subscriber.TableStreamEvent),
		listenerSubscriptions: make(map[string]map[string]bool),
		listenersMutex:        &sync.RWMutex{},
	}
}

func (p *TableStreamProviderMock) AddListener(name string, listener chan subscriber.TableStreamEvent) {

	p.listenersMutex.Lock()
	p.listeners[name] = listener
	p.listenerSubscriptions[name] = make(map[string]bool)
	p.listenersMutex.Unlock()
}

func (p *TableStreamProviderMock) RemoveListener(name string) {
	p.listenersMutex.Lock()
	delete(p.listeners, name)
	delete(p.listenerSubscriptions, name)
	p.listenersMutex.Unlock()
}

func (p *TableStreamProviderMock) AddSubscription(listenerName, table, publisher string) (err *protocol.TableStreamError) {

	p.listenersMutex.RLock()
	for name, listener := range p.listeners {
		if name == listenerName {
			log.Println("Sending tablestream data to", listenerName)
			var data map[string]interface{}
			json.Unmarshal([]byte(ORDER_JSON), &data)

			listener <- subscriber.TableStreamEvent{
				Table: table,
				Type:  subscriber.IMAGE,
				Data:  data,
			}
		}
	}
	p.listenersMutex.RUnlock()
	return
}

func (p *TableStreamProviderMock) RemoveSubscription(listener string, table string) (err *protocol.TableStreamError) {
	return
}

func (p *TableStreamProviderMock) GetImage(tableName string) (update subscriber.TableStreamEvent, ok bool) {
	return
}

func StartLocalTableStream(lt *protocol.LocalTransport, table string) {

	subscriptionReceiveCh := make(chan protocol.SubscriptionRequestMsg)
	lt.RegisterPublisher(table, subscriptionReceiveCh)

	go func() {
		select {
		case <-subscriptionReceiveCh:

			lt.SendSubscriptionResponse(table, "",
				protocol.SubscriptionResponseMsg{
					Image: []byte(ORDER_JSON),
				})
		}
	}()
}
