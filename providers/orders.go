package providers

import (
	"bitbucket.org/onsideanalysis/data_api/schemas/trading"
	"bitbucket.org/onsideanalysis/trading-system-framework/queue"
	"bitbucket.org/onsideanalysis/trading-system-framework/queue/rabbitmq"
	"encoding/json"
	"log"
)

type IOrdersProvider interface {
	UpdateOrdersStrategies(request trading.UpdateOrderStrategyMsg) (err error)
}

type OrdersProvider struct {
	executionProducer *rabbitmq.RMQProducer
	routingKey        string
}

func NewOrdersProvider(rabbitHosts []string, exchange string) *OrdersProvider {

	producer, err := rabbitmq.NewRMQProducer(rabbitHosts, exchange, "topic", nil)
	if err != nil {
		log.Println("Failed to create execution producer", err)
		return nil
	}
	producer.Connect()

	return &OrdersProvider{
		executionProducer: producer,
	}
}

func (p *OrdersProvider) UpdateOrdersStrategies(request trading.UpdateOrderStrategyMsg) (err error) {
	message, err := json.Marshal(request)
	if err != nil {
		log.Printf("Failed to marshal for strategy update %+v\n", request)
		return err
	}

	return p.publish(message, trading.STRATEGY_UPDATES)
}

func (p *OrdersProvider) publish(message []byte, routingKey string) (err error) {

	input := queue.Message{ContentType: "application/json", Key: routingKey, Body: message}
	log.Printf("%+v\n", string(message))
	err = p.executionProducer.Publish(input)
	return
}
