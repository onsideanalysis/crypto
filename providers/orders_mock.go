package providers

import "bitbucket.org/onsideanalysis/data_api/schemas/trading"

type OrdersProviderMock struct {
}

func NewOrdersProviderMock() IOrdersProvider {
	return &OrdersProviderMock{}
}

func (p *OrdersProviderMock) UpdateOrdersStrategies(request trading.UpdateOrderStrategyMsg) (err error) {
	return
}
